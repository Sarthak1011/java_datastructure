/*    CLASS	               
 class name and constructor name are same 30
 object class has no super class beacause it is last parent class if any one having no parennt class then object class is that parent
all the classess byte code are in the method area 
 squence 
 1.static variable             //method area in static block
 2.static block               //static block but comes first in stack
 3.static method             //method area
 4.insatance variable 	     //constructor 
 5.instance block           //constructor
 6.constructor              //object
 7.instance method          //constructor
 */ 

class Demo {

	int x = 10;//instance variable
		   
	static int y = 20;//static instance variable
	
	static {//static block

		System.out.println("In Static block");
	//	System.out.println(x);// error
		System.out.println(y);

	}

	Demo(){//in constructor

		System.out.println("In Demo Constructor");
		System.out.println(x);
		System.out.println(y);

	}

	{
		//instance block 

		System.out.println("In Instance block");
	//	System.out.println(x);//error
		System.out.println(y);

	}

	static void fun(){//static method

		System.out.println("In static method");
	//	System.out.println(x);//error non static variable x cannot be refernece from static context
		System.out.println(y);
	}
	void gun(){//instance method

		System.out.println("Instance method");
		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String args[]){

		Demo obj = new Demo();
		
		obj.fun();//or Demo.fun
		obj.gun();	
//		System.out.println(x);//error
		System.out.println(y);//error
	}
}
/*
 sequnce
 y=20
 in static block
 in static method
 x=10
 in instance block
 in Demo Constructor 
 in onstance method


 output

 in Static block
 in instancce block
 in Demo Constructor
 in static method
 instance method

 */ 



