class Parent{
	int x=10;
	String str="name";

	Parent(){
		System.out.println(this.hashCode());
	}
	void parentMethod(){
		System.out.println(x);
		System.out.println(str);
	}
}
class Child extends Parent{
	int x=20;
	String str="data";

	Child(){
		System.out.println(this.hashCode());
	}
	void childMethod(){
		System.out.println(x);
		System.out.println(str);
	
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Parent();
		Child obj1=new Child();

		obj1.parentMethod();
	}
}
