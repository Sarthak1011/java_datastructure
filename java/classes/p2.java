/*  CONSTRUCTOR
 1.class name and constructor is same
 2.when we create oject then implicitly call constructor  
 3.constructor is used for the initilize the instance variable
 4.constructor has no any return type beacause it is special method or when we give return type to constructor then it is normal instance method
 5.if we not create constructr then compiler by default create constructor and that constructor is default constructor and user give the construtor that is no argument constructor
 6.the first line of the constructor is invoked special (super()) means the call parent class constructor 
 7.we not give any parameter to constructor there is one parameter hidden this reference e.g Demo(Demo this) that  "this" is like object
 8.this keyword is help to constructor to initilize the instance variable 
 9.when we call in one constructor to another constructor then use the this() but this line should be first beacause no any one chnage in another constructor
 

 we are going through the refernce book their are two types of constructor one is no argument constructor and second is parameterized constructor but there is only one constructor i.e parameterized constructor because in no argument constructor this parameter is present

 non static variable cannt be access from the static context beacause the static variable come first and when we not create any object no craete constructor and not memory for the instance variable .instance varible allocate memory only in crating the obj
 construct is bydefault default in java when we apply access specifier to class then it apply to constructor

 constructor is one type of method but it is special method because it has no return type.
 method goes on method table in method table signature of method are unique 
 this through call one constructor to another 

 */ 



class Demo {

	//instance variable
	
	int x = 10;

	//no argument constructor
	 Demo(){//Demo(Demo this)
		//this parameter is here
		System.out.println(this);
		System.out.println("Demo constructor");

	}
	Demo(int x){//Demo(Demo this , int x)

		System.out.println(x);//100
		//help to constructor to initilize the instance variable
		System.out.println(this.x);//10
	}

	//no return type to constructor
	void Demo(){

		System.out.println("it is normal method");
	}


	public static void main(String args[]){

		Demo obj1 = new Demo();//object creation Demo(obj1)
		Demo obj2 = new Demo(100);//object creation   Demo(obj2,100)
		System.out.println(obj1);
		System.out.println(obj1.x);
	}
}
