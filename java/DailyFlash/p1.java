/*

Static method : checkPalindromeString 
Non static method : 
Palindrome string :
String is identical if reversed
Eg. racecar , madam , etc.



Anagram Strings :
2 Strings are anagram if they contain same characters and same number of times.
Non static method :
checkAnagramStrings

 */ 
import java.io.*;

class StringDemo {

	static int checkPalindromeString(String str1 ){

		char arr1[]=str1.toCharArray();

		String str2 = new String(str1);

		int j=arr1.length-1;
		for(int i=0;i<arr1.length;i++){

			if(i<j){
				char temp = arr1[i];
				arr1[i]=arr1[j];
				arr1[j]=temp;
			}
			j--;
		}
		String str3 = new String(arr1);

		if(str2.hashCode() == str3.hashCode()){
			return 0;
		}
		return -1;
	}

	static int CountChar(char arr[],int index){

		int count = 0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==arr[index]){
				count++;
			}
		}
		return count;
	}
			
	int checkAnagramStrings(String str1,String str2){

		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();

		int flag=0;
		if(arr1.length == arr2.length ){
			for(int i=0;i<arr1.length;i++){
				int count=0;
				for(int j=0;j<arr2.length;j++){
					if(arr1[i]==arr2[j]){
						count++;
					}
				}
				if(count == CountChar(arr1,i)){
					flag=1;
				}else{
					return -1;
				}
			}
			if(flag==1){
				return 0;
			}
		}
		return -1;

	}


	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string:");
		String str1 = obj.readLine();

		int ret1 =checkPalindromeString(str1);

		if(ret1 == 0){
			System.out.println("Palindrome String");
		}else{
			System.out.println("Not palindrome string");
		}

		StringDemo obj1 = new StringDemo();
		
		System.out.println("Enter the String ");
		String str2 = obj.readLine();
		
		System.out.println("Enter the String ");
		String str3 = obj.readLine();
	       
		int ret2 =obj1.checkAnagramStrings(str2,str3);	
	       
		if(ret2 == 0){
		       System.out.println("Anagram String");
	       }else{
		       System.out.println("Not anagram String");
	       }

	}
}
