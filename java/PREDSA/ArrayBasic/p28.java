/*
 28] Remove Duplicates from unsorted array
Given an array of integers which may or may not contain duplicate elements. Your
task is to remove duplicate elements, if present.
Example 1:
Input:
N = 6
A[] = {1, 2, 3, 1, 4, 2}
Output:
1 2 3 4
Example 2:
Input:
N = 4
A[] = {1, 2, 3, 4}
Output:
1 2 3 4
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5

*/

import java.io.*;

class RemoveEle{

	int[] bubbleSort(int arr[]){
		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length-1-i;j++){
				if(arr[j]>arr[j+1]){
					int temp = arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		return arr;
	}
	int[] DuplicatesNum(int arr[]){
		int arr1[] = bubbleSort(arr); 
		int temp[] = new int[arr.length];
		int k =0;
		int count =0;	
		for(int i =0;i<arr1.length-1;i++){
			if(arr1[i]!=arr1[i+1]){
				temp[k++]=arr1[i];
			}
		}
		temp[k++]=arr1[arr1.length-1];
		int newarr[] = new int[k-count];
		for(int i = 0;i<k-count;i++){
			newarr[i]=temp[i];
		}
		return newarr;

	}
	public static void main(String []args)throws IOException{	
 		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size1");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){

                        arr1[i]=Integer.parseInt(br.readLine());
                }
		RemoveEle obj = new RemoveEle();
		int arr[] = obj.DuplicatesNum(arr1);
		//int arr[] = obj.bubbleSort(arr1);

		for(int i = 0; i<arr.length ; i++){
			System.out.println(arr[i]);
		}
	}
}

