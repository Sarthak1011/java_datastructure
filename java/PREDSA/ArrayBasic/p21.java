/*
 21] First element to occur k times
Given an array of N integers. Find the first element that occurs at least K number
of times.
Example 1:
Input :
N = 7, K = 2
A[] = {1, 7, 4, 3, 4, 8, 7}
Output :
4
Explanation:
Both 7 and 4 occur 2 times.
But 4 is first that occurs 2 times
As at index = 4, 4 has occurred
at least 2 times whereas at index = 6,
7 has occurred at least 2 times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10^4
1 <= K <= 100
1<= A[i] <= 200

 */

import java.io.*;

class NumberOcc {
	
	int Occurance(int arr[],int k){
		
		for(int i =1;i<arr.length-1;i++){
			int count  = 0;
			for(int j = 0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count == k){
				return arr[i];
			}
		}
		return -1;
	}



	public static void main(String args[])throws IOException{
		      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size1 = Integer.parseInt(br.readLine());

                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){

                        arr1[i]=Integer.parseInt(br.readLine());

                }
                System.out.println("Enter the number that occurs how many times");
                int k = Integer.parseInt(br.readLine());
                NumberOcc obj = new NumberOcc();

		int num = obj.Occurance(arr1,k);
		System.out.println(num);
	}
}


