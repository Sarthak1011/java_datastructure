/*
 2] Find minimum and maximum element in an array
Given an array A of size N of integers. Your task is to find the minimum and
maximum elements in the array.
Example 1:
Input:
N = 6
A[] = {3, 2, 1, 56, 10000, 167}
Output: 1 10000
Explanation: minimum and maximum elements of array are 1 and 10000.
Example 2:
Input:
N = 5
A[] = {1, 345, 234, 21, 56789}
Output: 1 56789
Explanation: minimum and maximum elements of array are 1 and 56789.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10^5
1 <= Ai <=10^12

 */
import java.io.*;

class MinMax {

        int[] MinMaxNum(int arr[]){

                int min = arr[0];
                int max = arr[0];
                for(int i = 0;i<arr.length;i++){

                        if(min > arr[i] ){
                                min = arr[i];
                        }
                        if(max < arr[i]){

                                max = arr[i];
                        }
                }
                int minmax[]= {min , max};
                return minmax;

        }
        public static void main(String []args)throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Array Size");
                int size = Integer.parseInt(obj.readLine());

                System.out.println("Enter the Array Elements");

                int arr[] = new int[size];

                for(int i = 0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(obj.readLine());


                }
                MinMax obj1 = new MinMax();

                int arr1[] =obj1.MinMaxNum(arr);
                System.out.println(arr1[0]+ "is a minimum number"+arr1[1]+ "is a maximum number");
      
	}
}
