
/*
 40] Fibonacci in the array
Given an array arr of size N, the task is to count the number of elements of the
array which are Fibonacci numbers
Example 1:
Input: N = 9, arr[] = {4, 2, 8, 5, 20, 1,
40, 13, 23}
Output: 5
Explanation: Here, Fibonacci series will be 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55.
Numbers that are present in array are 2, 8, 5, 1, 13
Example 2:
Input: N = 4, arr[] = {4, 7, 6, 25}
Output: 0
Explanation: No Fibonacci number in
this array.
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ N ≤ 10^6

 */ 

import java.io.*;

class FibonacciSeries{

	int  findFiboNum(int num){
		int x1 =0;
		int x2 =1;
		for(int i =1;i<=num;i++){
			int x3 = x1 + x2;
			x1 = x2;
			x2 = x3;

		}
		return -1;
	}
	int isPresentFiboNum(int arr[]){

		for(int i =0;i<arr.length;i++){
			if(arr[i]==findFiboNum(arr[i])){
				return arr[i];
			}
		}
		return -1;

	}
	 public static void main(String []args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size1");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }

		FibonacciSeries obj = new FibonacciSeries();
		System.out.println(obj.isPresentFiboNum(arr1));
	 }
}

		

				
