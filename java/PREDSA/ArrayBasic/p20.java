/*
 20] Check if pair with given Sum exists in Array (Two Sum)
Given an array A[] of n numbers and another number x, the task is to check
whether or not there exist two elements in A[] whose sum is exactly x.
Examples:
Input: arr[] = {0, -1, 2, -3, 1}, x= -2
Output: Yes
Explanation: If we calculate the sum of the output,1 + (-3) = -2
Input: arr[] = {1, -2, 1, 0, 5}, x = 0
Output: No
 */ 

import java.io.*;

class SumPair {


	int sumPair(int arr[], int x){

		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length;j++){
				//System.out.println("arr[i] = "+arr[i]+"   "+"arr[j] = "+arr[j]);
				if(arr[i]+arr[j] == x && i !=j ){

					return 0;
				}
			}
		}
		return -1;
	}
		
	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size");
                int size1 = Integer.parseInt(br.readLine());

                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){

                        arr1[i]=Integer.parseInt(br.readLine());

                }
		System.out.println("Enter the checking number");
                int x = Integer.parseInt(br.readLine());
		SumPair obj = new SumPair();
		int ret = obj.sumPair(arr1, x);
		if(ret == 0){
			System.out.println("Yes");
		}else{
			System.out.println("No");
		}

	}
}
