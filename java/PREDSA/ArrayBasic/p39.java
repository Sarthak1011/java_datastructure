/*
 39] Leaders in an array
Write a program to print all the LEADERS in the array. An element is a leader if it
is greater than all the elements to its right side. And the rightmost element is
always a leader.
For example:
Input: arr[] = {16, 17, 4, 3, 5, 2},
Output : 17, 5, 2
Input: arr[] = {1, 2, 3, 4, 5, 2},
Output: 5, 2

 */ 
import java.io.*;

class LeaderNum{

	int[] leader(int arr[]){

		int j =0;
		int max = 0;
		int newarr[] = new int[arr.length-1];
		for(int i =arr.length-1;i>=0;i--){

			if(arr[i]>=max){
				newarr[j]=arr[i];
				max = newarr[j++];
			}
		}
		int temp[] = new int[j];
		int k =j;
		for(int i =0;i<j;i++){

			temp[i]=newarr[--k];	
		}
	
		return temp;
	}
	 public static void main(String []args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size1");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
                LeaderNum obj = new LeaderNum();
                int arr[] = obj.leader(arr1);
                for(int i = 0;i<arr.length;i++){
                        System.out.print(arr[i]+"\t");
                }
                        System.out.print("");
	 }
}
