//import arrayboilerplate.ArrayPackage;
import java.io.*;

//import arrayboilerplate.ArrayPackage;

class MaxOddSum{

        int[] sortArr(int[] arr){

                int[] intArray = arr;

                for (int i = 0; i < intArray.length - 1; i++) {
                        for (int j = 0; j < intArray.length - 1 - i; j++) {

                                if (intArray[j] > intArray[j + 1]) {

                                        int temp = intArray[j];
                                        intArray[j] = intArray[j + 1];
                                        intArray[j + 1] = temp;
                                }
                        }
                }
                return intArray;
        }

        int maxOddSum(int[] arr){
                int countofeven = 0;
                int count = 0;
                int sum = 0;
                int[] sortedarr = sortArr(arr);

                for(int i = 0;i<sortedarr.length;i++){

                        if(sortedarr[i] % 2 == 0){
                                countofeven++;
                        }

                        if(sortedarr[i] < 0){
                                count++;
                        }
                        if(sortedarr[i] > 0){
                                sum = sum + sortedarr[i];
                        }
                }

                if(countofeven == sortedarr.length || count == sortedarr.length){
                        return-1;
                }

                if(count > 0){

                        for(int i = 0; i<count;i++){
                                if(sortedarr[i]<0 && sortedarr[i] % 2 != 0){
                                        sum = sum + sortedarr[i];
                                        break;
                                }
                        }
                        if(sum % 2 != 0){
                                return sum;
                        }
                        for(int i = 0; i<sortedarr.length;i++){
                                if(sortedarr[i] % 2 != 0 ){
                                        sum = sum - sortedarr[i];
                                        break;
                                }
                        }
                        return sum;
                }
                if(sum % 2 == 0 && count == 0){
                        for(int i = 0; i<sortedarr.length;i++){
                                if(sortedarr[i] % 2 != 0 ){
                                        sum = sum - sortedarr[i];
                                        break;
                                }
                        }
                }
                return sum;
                }

        public static void main(String[] args)throws IOException{

              BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Array Size");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array elements");

                int arr[]= new int[size];

                for(int i = 0;i<size;i++){
                        arr[i]= Integer.parseInt(br.readLine());

                } 

                MaxOddSum mos = new MaxOddSum();

                int maxoddsum = mos.maxOddSum(arr);
                System.out.println("Maximum Odd Sum is : " + maxoddsum);
        }
}
