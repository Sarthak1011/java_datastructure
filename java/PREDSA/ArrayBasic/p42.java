/*42] Count the number of elements between two given elements
in an array. Given an unsorted array and two elements num1
and num2.
The task is to count the number of elements occurring between the given elements
(excluding num1 and num2). If there are multiple occurrences of num1 and num2,
we need to consider the leftmost occurrence of num1 and rightmost occurrence of
num2.
Example 1:
Input : Arr[] = {4, 2, 1, 10, 6}
num1 = 4 and num2 = 6
Output : 3
Explanation:
We have an array [4, 2, 1, 10, 6] and num1 = 4 and num2 = 6. So, the
leftmost index of num1 is 0 and the rightmost index of num2 is 4. So, the
total number of elements between them is [2, 1, 10] So, the function will
return 3 as an answer.
Example 2:
Input : Arr[] = {3, 2, 1, 4}
num1 = 2 and num2 = 4
Output : 1
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
2 ≤ N ≤ 10^5
1 ≤ A[i], num1, num2 ≤ 10^5

 */

import java.io.*;

class Count {

	int countEle(int arr[],int num1 , int num2){
		int start = 0;
		int end = 0;
		for(int i = 0;i<arr.length;i++){
			if(num1 == arr[i]){
				start = i;
			}
			if(num2 == arr[i]){
				end = i;
			}
		}
		return end - start - 1;
	}
 	public static void main(String []args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size1");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                
		System.out.println("Enter the Array Elements");
                
		for(int i = 0;i<arr1.length;i++){
                
			arr1[i]=Integer.parseInt(br.readLine());
                
		}
		System.out.println("Enter the starting number");
		int start = Integer.parseInt(br.readLine());
               	
		System.out.println("Enter the ending number");
		int end = Integer.parseInt(br.readLine());

		Count obj = new Count();
		System.out.println(obj.countEle(arr1,start,end));
	}
}
