/*
 8] Even occurring elements
Given an array Arr of N integers that contains an odd number of occurrences for all
numbers except for a few elements which are present even number of times. Find
the elements which have even occurrences in the array.
Example 1:
Input:
N = 11
Arr[] = {9, 12, 23, 10, 12, 12,
15, 23, 14, 12, 15}
Output: 12 15 23
Example 2:
Input:
N = 5
Arr[] = {23, 12, 56, 34, 32}
Output: -1
Explanation:
Every integer is present odd number of times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10^5
0 ≤ Arr[i] ≤ 63

 */
import java.io.*;

class EvenOcc {

	int[] evenOccurance(int arr[]){

		int j =0;
		int count = 0;
		int k =0;
		int arr1[] = new int[5];
//		[1,2,1,3,2]
		for(int i =0;i<arr.length;j++){
			if(j<arr.length){

			       if(arr[i]==arr[j]){
					System.out.println("arr[i] = "+arr[i]+ "i= "+i+"arr[j] ="+arr[j]+"j ="+j);
				
					count++;
				}
			}else{
				System.out.println(arr[i]+" "+count);
				if(count % 2 == 0 && count !=0){
					arr1[k++]=arr[i];
				}
				arr[j]=0;
				i++;
				j=i-1;
				count = 0;
			}
		
		}
		return arr1;
	}		
	public static void main(String arg[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Array elements");

		int arr[]= new int[size];
		
		for(int i = 0;i<size;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}

		EvenOcc obj = new EvenOcc();

		int ret[] =obj.evenOccurance(arr);

		for(int  i= 0;i<ret.length;i++){
			System.out.println(ret[i]);
		}
	}
}


