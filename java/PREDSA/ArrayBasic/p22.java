/*
 22] Exceptionally odd
Given an array of N positive integers where all numbers occur even number of
times except one number which occurs odd number of times. Find the exceptional
number.
Example 1:
Input:
N = 7
Arr[] = {1, 2, 3, 2, 3, 1, 3}
Output: 3
Explanation: 3 occurs three times.
Example 2:
Input:
N = 7
Arr[] = {5, 7, 2, 7, 5, 2, 5}
Output: 5
Explanation: 5 occurs three times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10^5
1 ≤ arr[i] ≤ 10^6

 *
 */
 
import java.io.*;

class ExceptOdd {

        int exceptOdd(int arr[]){

                for(int i =1;i<arr.length-1;i++){
                        int count  = 0;
                        for(int j = 0;j<arr.length;j++){
                                if(arr[i]==arr[j]){
                                        count++;
                                }
                        }
                        if(count% 2 == 1){
                                return arr[i];
                        }
                }
                return -1;
        }



        public static void main(String args[])throws IOException{
         
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

               	System.out.println("Enter the size");
               	int size1 = Integer.parseInt(br.readLine());
               	int arr1[]= new int[size1];
               	System.out.println("Enter the Array Elements");
               	for(int i = 0;i<arr1.length;i++){
      
			arr1[i]=Integer.parseInt(br.readLine());
                }
                ExceptOdd obj = new ExceptOdd();

                int num = obj.exceptOdd(arr1);
                System.out.println("\n\n"+num);
        }
} 
