/*37] Move all negative numbers to beginning and positive to end
with constant extra space
An array contains both positive and negative numbers in random order. Rearrange
the array elements so that all negative numbers appear before all positive numbers.
Examples :
Input: -12, 11, -13, -5, 6, -7, 5, -3, -6
Output: -12 -13 -5 -7 -3 -6 11 6 5

 
 */
import java.io.*;
class MoveEle{

	int[] moveEle(int arr[]){

		int temp[] = new int[arr.length];
		int j =0;
		for(int i =0;i<arr.length-1;i++){
			if(arr[i]<0){
				temp[j++]=arr[i];
			}
		}
		for(int i = 0;i<arr.length;i++){
			if(arr[i]>0){

				temp[j++]=arr[i];

			}
		}
		return temp;
	}
 	public static void main(String []args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size1");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
		MoveEle obj = new MoveEle();
		int arr[] = obj.moveEle(arr1);
		for(int i = 0;i<arr.length-1;i++){
			System.out.print(arr[i]+"\t");
		}
			System.out.print("");
	}
 }
