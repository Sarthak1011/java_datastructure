/*
 5] Replace all 0's with 5
You are given an integer N. You need to convert all zeros of N to 5.
Example 1:
Input:
N = 1004
Output: 1554
Explanation: There are two zeroes in 1004
on replacing all zeroes with "5", the new
number will be "1554".
Example 2:
Input:
N = 121
Output: 121
Explanation: Since there are no zeroes in
"121", the number remains as "121".
Expected Time Complexity: O(K) where K is the number of digits in N
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 10000

 */

import java.io.*;
class ReplaceNum {

	int rev = 0;
	int replaceAll(int a , int k , int m){

		int num = a;
		while(num != 0){
			int rem1 = num %10;
			if(rem1 == k){
				rem1 = m;
			}
			rev = rev * 10 + rem1;
			num = num/10;
		}
		int x = 0;
		while(rev != 0 ){
			int rem2 = rev % 10;
			x = x * 10 + rem2;
			rev = rev / 10;
		}
		return x;
	}
   	public static void main(String []args)throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the num");
                int num = Integer.parseInt(obj.readLine());

		System.out.println("Which single digit number do you want to replace ");
		int replaceNum = Integer.parseInt(obj.readLine());

		System.out.println("Which number do you want to adjust on that number");
		int adjustedNum = Integer.parseInt(obj.readLine());

		ReplaceNum obj1 = new ReplaceNum();
		int num1 = obj1.replaceAll(num, replaceNum , adjustedNum);

		System.out.println(num1);
	}
}

