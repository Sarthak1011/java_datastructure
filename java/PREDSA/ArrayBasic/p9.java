/*9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
Examples 1:
Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
Output: arr[] = { 1, 2, 4, 5 }
Examples 2:
Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
Output: arr[] = { 4, 5, 9, 1 }
 */ 
import java.io.*;
class RemoveEle {

	int[] removeEle(int arr[],int index){
	
		int j =0;
		int arr1[]= new int[arr.length-1];
		for(int i = 0;i<arr.length;i++){
			if(i != index){
				arr1[j++]=arr[i];
			}
		}
		return arr1;

	}
		public static void main(String arg[])throws IOException {
                	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                	System.out.println("Enter the Array Size");
                	int size = Integer.parseInt(br.readLine());

                	System.out.println("Enter the Array elements");

                	int arr[]= new int[size];

                	for(int i = 0;i<size;i++){
                        	arr[i]= Integer.parseInt(br.readLine());
                	}

			System.out.println("Enter the Index number do you want to delete Ele");
			int index = Integer.parseInt(br.readLine());

			RemoveEle obj = new RemoveEle();

			int arr2[]=obj.removeEle(arr,index);
                	for(int i = 0;i<arr2.length;i++){
                        	System.out.println(arr2[i]);
                	}
		}	
}

