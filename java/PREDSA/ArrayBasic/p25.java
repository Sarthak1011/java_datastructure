/*
 25] Maximum product of two numbers
Given an array Arr of size N with all elements greater than or equal to zero. Return
the maximum product of two numbers possible.
Example 1:
Input:
N = 6
Arr[] = {1, 4, 3, 6, 7, 0}
Output: 42
Example 2:
Input:
N = 5
Arr = {1, 100, 42, 4, 23}
Output: 4200
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10^7
0 ≤ Arr[i] ≤ 10^4

 */
import java.io.*;

class Product {

	int maxProduct(int arr[]){
		int maxProduct = 1;
		int product = 1;
		for(int i =0;i<arr.length;i++){
			for(int j =i+1;j<arr.length;j++){
				
				product = arr[i]*arr[j];
				
				if(product > maxProduct){
					maxProduct = product;
				}
			}
		}
		return maxProduct;
	}
	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){

                        arr1[i]=Integer.parseInt(br.readLine());
                }
		Product p = new Product();
		int num = p.maxProduct(arr1);
		System.out.println(num);
	}
}
