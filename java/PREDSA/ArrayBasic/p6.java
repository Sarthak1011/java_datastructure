/*
 6] Elements in the Range
Given an array arr[] containing positive elements. A and B are two numbers
defining a range. The task is to check if the array contains all elements in the given
range.
Example 1:
Input: N = 7, A = 2, B = 5
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: Yes
Explanation: It has elements between range 2-5 i.e 2,3,4,5
Example 2:
Input: N = 7, A = 2, B = 6
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: No
Explanation: Array does not contain 6.
Note: If the array contains all elements in the given range then driver code outputs
Yes otherwise, it outputs No
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ N ≤ 10^7

 */
import java.io.*;

class Range {


	int isContain(int arr[] , int start , int end){
	int val =start;	
	int count = 0;
	int size = end - start +1;
	int[] arr2 = new int[size];

		for(int i = 0;i<size;i++){
			arr2[i]=val;
			val++;
		}
		
		for(int i = 0;i<arr2.length;i++){
		
			for(int j = 0;j<arr.length;j++){
				if(arr2[i]==arr[j]){
					count++;
					break;
				}
			}
		}
		if(count==size){
			return 0;
		}
		return -1;

	}
	public static void main(String []args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter the Array size");
		 int size = Integer.parseInt(br.readLine());

		 System.out.println("Enter the Array Elements ");

		 int arr[] = new int[size];

		 for(int i = 0;i<arr.length;i++){
		
			 arr[i]= Integer.parseInt(br.readLine());
		
		 }
		 System.out.println("Enter the starting number");
		 int start = Integer.parseInt(br.readLine());

		 System.out.println("Enter the ending number");
		 int end = Integer.parseInt(br.readLine());

		 Range obj = new Range();
		 int ret = obj.isContain(arr , start , end);

		 if(ret == 0){
			 System.out.println("Range is present ");
		 }else{
			 System.out.println("Range is not Present");
		 }
	}

}


