/*
 36] Find a peak element which is not smaller than its neighbors
Given an array arr of n elements that is first strictly increasing and then maybe
strictly decreasing, find the maximum element in the array.
Note: If the array is increasing then just print the last element will be the maximum
value.
Examples:
Input: array[]= {5, 10, 20, 15}
Output: 20
Explanation: The element 20 has neighbors 10 and 15, both of them are less
than 20.
Input: array[] = {10, 20, 15, 2, 23, 90, 67}
Output: 20 or 90
Explanation: The element 20 has neighbors 10 and 15, both of them are less
than 20, similarly 90 has neighbors 23 and 67.

 */

import java.io.*;

class PeakEle{

	int[] findPeak(int arr[]){

		int j =0;
		int temp[]=new int[arr.length];
		for(int i =1;i<arr.length-1;i++){
			if(arr[i-1] < arr[i] && arr[i+1] < arr[i]){
				//System.out.println("arr[i-1]= "+arr[i-1]+"arr[i] ="+arr[i]+"arr[i+1 =]"+arr[i+1]+"arr[i]"+arr[i]);
			//	System.out.println(arr[i]);
				temp[j++]=arr[i];
			}
		}
		int newarr[]= new int[j];
		for(int i =0;i<j;i++){
			newarr[i]=temp[i];
		}
		return newarr;
	}
	public static void main(String []args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size1");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
		PeakEle obj = new PeakEle();
		int arr[]= obj.findPeak(arr1);
		for(int i =0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	 }
}




