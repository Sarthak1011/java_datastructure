/*
 23] Find the smallest and second smallest element in an array
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.
Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5

 */

import java.io.*;

class SmallestNum {

	int[] SortArray(int arr[]){
		for(int i = 0;i<arr.length;i++){
			for(int j = i+1;j<arr.length-1;j++){
					//System.out.println("arr["+i+"] = "+arr[i]+" arr["+j+"] = "+arr[j]);
				if(arr[i]>arr[j]){
					//System.out.println("arr["+i+"] = "+arr[i]+"arr["+j+"] = "+arr[j]);

					int temp = arr[i];
					arr[i]=arr[j];
					arr[j]= temp;
				}
			}
		}/*
		for(int i = 0;i<arr.length;i++){
			System.out.println(arr[i]);
		}*/
		return arr;
	}
	int[] smallNum(int arr[]){
		int sortArray[]=SortArray(arr);

		int small[] = new int[2];
		small[0] = sortArray[0];
		for(int i = 0;i<sortArray.length;i++){
			if(small[0]<arr[i] ){
				small[1] = arr[i];
				break;
			}
		}
		return small;
	}

 	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size1 = Integer.parseInt(br.readLine());
                int arr1[]= new int[size1];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr1.length;i++){

                        arr1[i]=Integer.parseInt(br.readLine());
                }
                SmallestNum obj = new SmallestNum();

                int num[] = obj.smallNum(arr1);
                System.out.println("\n\n"+num[0]+" "+num[1]);
	}
}


