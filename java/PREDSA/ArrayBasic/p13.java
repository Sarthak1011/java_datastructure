/*
 13] Find unique element
Given an array of size n which contains all elements occurring in multiples of K,
except one element which doesn't occur in multiple of K. Find that unique element.
Example 1:
Input :
n = 7, k = 3
arr[] = {6, 2, 5, 2, 2, 6, 6}
Output :
5
Explanation:
Every element appears 3 times except 5.
Example 2:
Input :
n = 5, k = 4
arr[] = {2, 2, 2, 10, 2}
Output :
10
Explanation:
Every element appears 4 times except 10.
Expected Time Complexity: O(N. Log(A[i]) )
Expected Auxiliary Space: O( Log(A[i]) )
Constraints:
3<= N<=2*10^5
2<= K<=2*10^5
1<= A[i]<=10^9

 */

import java.io.*;
class UniqueNum{
	
	int uniqueNum(int arr[],int times){

		for(int i =0;i<arr.length;i++){
			int count=0;
			for(int j = 0;j<arr.length;j++){

				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count!=times){
				return arr[i];
			}
		}
		return -1;
	}
	public static void main(String arg[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Array Size");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array elements");

                int arr[]= new int[size];

                for(int i = 0;i<size;i++){
      	              arr[i]= Integer.parseInt(br.readLine());
                }

                System.out.println("Enter the number that number how many time repeat");
                int times = Integer.parseInt(br.readLine());

		UniqueNum obj = new UniqueNum();
		int num  =obj.uniqueNum(arr,times);
		System.out.println(num);

	}
}
