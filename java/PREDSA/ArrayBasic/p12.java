/*
 12] First and last occurrences of X
Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.
Example 1:
Input:
N = 4 , X = 3
arr[] = { 1, 3, 3, 4 }
Output:
1 2
Explanation:
For the above array, first occurance of X = 3 is at index = 1 and last
occurrence is at index = 2.
Example 2:
Input:
N = 4, X = 5
arr[] = { 1, 2, 3, 4 }
Output:
-1
Explanation:
As 5 is not present in the array, so the answer is -1.
Expected Time Complexity: O(log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10^5
0 <= arr[i], X <= 10^9
 */ 
import java.io.*;
class FirstLastOcc {

	int[] firstLastOcc(int arr[],int num){

		int arr1[]=new int[2];
		for(int i = 0;i<arr.length;i++){ 
			if(arr[i]==num){
				arr1[0]=i;
				break;
			}
		}
		for(int i = arr.length-1;i>=0;i--){ 
			if(arr[i]==num){
				arr1[1]=i;
				break;
			}
		}
		return arr1;
	}
	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Array Elements ");

		int arr[] = new int[size];

		for(int i =0;i<arr.length;i++){
		
			arr[i]= Integer.parseInt(br.readLine());
	
		}
		System.out.println("Enter the number that do you want to check Occurance");
		int num = Integer.parseInt(br.readLine());

		FirstLastOcc obj = new FirstLastOcc();

		int arr1[] = obj.firstLastOcc(arr,num);

		if(arr1[0]==0 && arr1[1]==0){
			System.out.println("-1");
		}else{
			for(int i =0;i<arr1.length;i++){
				System.out.println(arr1[i]);
			}
		}
	}
}
	
