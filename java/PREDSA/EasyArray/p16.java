/*
 Que 16 : Rotation
Given an ascending sorted rotated array Arr of distinct integers of size N. The array is
right rotated K times. Find the value of K.
Example 1:
Input:
N = 5
Arr[] = {5, 1, 2, 3, 4}
Output: 1
Explanation: The given array is 5 1 2 3 4. The original sorted array is 1 2 3 4 5.
We can see that the array was rotated 1 time to the right.
Example 2:
Input:
N = 5
Arr[] = {1, 2, 3, 4, 5}
Output: 0
Explanation: The given array is not rotated.
Expected Time Complexity: O(log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <=10
5
1 <= Arri <= 10
7

 */ 

import java.io.*;

class Array {

	int Rotation(int arr[]){
		int count = 1;	
		for(int i = 1;i<arr.length;i++){
			if(arr[i-1]<arr[i]){
				count++;
			}else{
				break;
			}
		}
		return count;
	}
