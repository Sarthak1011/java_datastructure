/*
 Que 10 : First Repeating Element
Given an array arr[] of size n, find the first repeating element. The element should occur
more than once and the index of its first occurrence should be the smallest.
Note:- The position you return should be according to 1-based indexing.
Example 1:
Input:
n = 7
arr[] = {1, 5, 3, 4, 3, 5, 6}
Output: 2
Explanation: 5 is appearing twice and its first appearance is at index 2 which is
less than 3 whose first occurring index is 3.
Example 2:
Input:
n = 4
arr[] = {1, 2, 3, 4}
Output: -1
Explanation: All elements appear only once so the answer is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= n <= 10
6
0 <= Ai<= 10
6

*/

import java.io.*;

class Array {

	int firstRepeatingEle(int arr[]){

		for(int i =0;i<arr.length;i++){
			int count = 0;
			for(int j = i;j<arr.length;j++){

				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count >= 2){
				return i;
			}
		}
		return -1;

	}
	  public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }

                Array obj = new Array();

                System.out.println(obj.firstRepeatingEle(arr));
	  }
}
		
			


