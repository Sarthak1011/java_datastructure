/*
 Que 7 : Check if array is sorted
Given an array arr[] of size N, check if it is sorted in non-decreasing order or not.
Example 1:
Input:
N = 5
arr[] = {10, 20, 30, 40, 50}
Output: 1
Explanation: The given array is sorted.
Example 2:
Input:
N = 6
arr[] = {90, 80, 100, 70, 40, 30}
Output: 0
Explanation: The given array is not sorted.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10
5
1 ≤ Arr[i] ≤ 10
6
 */
import java.io.*;

class Array {

	int checkSorted(int arr[]){

		for(int i = 0; i<arr.length-1;i++){
			if(arr[i]>arr[i+1]){
				return 0;
			}
		}
		return 1;
	}
	  public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }

                Array obj = new Array();
                int arr1 = obj.checkSorted(arr);
                        System.out.println(arr1);
         }
}

