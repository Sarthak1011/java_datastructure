/*
Que 1 : Missing number in array
Given an array of size N-1 such that it only contains distinct integers in the range of 1 to
N. Find the missing element.
Example 1:
Input:
N = 6
A[] = {1,2,4,5,6}
Output: 3
Example 2:
Input:
N = 11
A[] = {1,3,2,5,6,7,8,11,10,4}
Output: 9
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10
6
1 ≤ A[i] ≤ 10
6

 */ 
import java.io.*;

class Array {
/*
	int[] BubbleSort(int arr[]){

		for(int i =0;i<arr.length;i++){
			for(int j = i+1;j<arr.length;j++){
				if(arr[i]>arr[j]){
					int temp = arr[i];
					arr[i]= arr[j];
					arr[j]= temp;
				}
			}
		}
		return arr;
	}
	int missingEle(int arr[]){

		int newarr[] = BubbleSort(arr);

		for(int i = 0 ;i<newarr.length-1;i++){
			if(arr[i+1]-arr[i]!=1){
				return arr[i]+1;
			}
		}
		return -1;
	}*/

	int missingEle(int arr[]){
		int temp[] = new int[arr.length + 1];

		for(int i = 0;i<temp.length;i++){
			temp[i]= 0;
		}
		for(int i = 0;i<arr.length;i++){
			temp[arr[i]-1]=1;
		}
		for(int i = 0;i<temp.length;i++){
			if(temp[i]==0){
				return i + 1;
			}
		}
		return -1;
	}
	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array ");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Array Elements");

		int arr[]= new int[size];
		for(int i =0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		Array obj = new Array();
		System.out.println(obj.missingEle(arr));
	}
}


