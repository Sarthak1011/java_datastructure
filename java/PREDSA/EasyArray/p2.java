/*
Que 2 :Sort an array of 0s, 1s and 2s
Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
Example 1:
Input:
N = 5
arr[]= {0 2 1 2 0}
Output:
0 0 1 2 2
Explanation: 0s 1s and 2s are segregated into ascending order.
Example 2:
Input:
N = 3
arr[] = {0 1 0}
Output:
0 0 1
Explanation: 0s 1s and 2s are segregated into ascending order.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10
6
0 <= A[i] <= 2
 
 */ 
import java.io.*;

class Array {
	int[] selectionSort(int arr[]){
		for(int i =0;i<arr.length;i++){
			int minIndex = i;
			for(int j = i+1;j<arr.length;j++){
				if(arr[minIndex]>arr[j]){
					minIndex = j;
				}
			}
			int temp = arr[i];
			arr[i]= arr[minIndex];
			arr[minIndex]= temp;
		}
		return arr;
	}
 	public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
                Array obj = new Array();
		int arr1[] = obj.selectionSort(arr);
		for(int i = 0;i<arr1.length;i++){

    	        	System.out.println(arr1[i]);
		
		}
	}
 }
