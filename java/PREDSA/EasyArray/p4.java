/*
Que 4 : Count pairs with given sum
Given an array of N integers, and an integer K, find the number of pairs of elements in
the array whose sum is equal to K.
Example 1:
Input:
N = 4, K = 6
arr[] = {1, 5, 7, 1}
Output: 2
Explanation:
arr[0] + arr[1] = 1 + 5 = 6
and arr[1] + arr[3] = 5 + 1 = 6.
Example 2:
Input:
N = 4, K = 2
arr[] = {1, 1, 1, 1}
Output: 6
Explanation:
Each 1 will produce sum 2 with any 1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10
5
1 <= K <= 10
8
1 <= Arr[i] <= 10
6

 */

import java.io.*;

class Array {

	int countSumPair(int arr[] , int sum){
		int count = 0;

		for(int i =0;i<arr.length;i++){
			for(int j  = i+1;j<arr.length;j++){

				if(arr[i]+arr[j] == sum){

					count++;

				}
			}
		}
		return count;
	}
	 public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.println("Enter the Sum");
		int sum = Integer.parseInt(br.readLine());
                Array obj = new Array();
                int arr1 = obj.countSumPair(arr,sum);
                        System.out.println(arr1);
        }
}	
