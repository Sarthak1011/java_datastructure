/*
 Que 8 : Rotate Array
Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise
direction) by D steps, where D is a positive integer.
Example 1:
Input:
N = 5, D = 2
arr[] = {1,2,3,4,5}
Output: 3 4 5 1 2
Explanation: 1 2 3 4 5 when rotated
by 2 elements, it becomes 3 4 5 1 2.
Example 2:
Input:
N = 10, D = 3
arr[] = {2,4,6,8,10,12,14,16,18,20}
Output: 8 10 12 14 16 18 20 2 4 6
Explanation: 2 4 6 8 10 12 14 16 18 20
when rotated by 3 elements, it becomes
8 10 12 14 16 18 20 2 4 6.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10
6
1 <= D <= 10
6
0 <= arr[i] <= 10
5

 */ 

import java.io.*;

class Array {

/*	int[] rotedArray(int arr[] , int D){

		for(int i =0;i<arr.length;i++){
			for(int j = )
				int temp = arr[i];
				arr[i] = arr[i+1];
				arr[arr.length-i]= temp;
		}
		return arr;
	}*/

	 int[] rotedArray(int arr[] , int D){

		 for(int j  = 0;j<D;j++){
                	for(int i =0;i<arr.length-1;i++){
                                int temp = arr[i];
                                arr[i] = arr[i+1];
                                arr[i + 1]= temp;
			}
		
        	}
		return arr;
	 }
	 public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.println("Enter the number");
		int D = Integer.parseInt(br.readLine());

                Array obj = new Array();

		int arr1[] = obj.rotedArray(arr,D);

		for(int i = 0;i<arr1.length;i++){
			System.out.println(arr1[i]);
		}
	 }
}

