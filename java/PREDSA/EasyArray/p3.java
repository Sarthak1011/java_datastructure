/*
 Que 3 : Find Duplicates in an Array
Given an array of size N which contains elements from 0 to N-1, you need to find all the
elements occurring more than once in the given array. Return the answer in ascending
order. If no such element is found, return list containing [-1].
Note: The extra space is only for the array to be returned. Try and perform all operations
within the provided array.
Example 1:
Input:
N = 4
a[] = {0,3,1,2}
Output:
-1
Explanation:
There is no repeating element in the array. Therefore output is -1.
Example 2:
Input:
N = 5
a[] = {2,3,1,2,3}
Output:
2 3
Explanation:
2 and 3 occur more than once in the given array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10
5
0 <= A[i] <= N-1, for each valid i

 */ 

import java.io.*;

class Array {
	 int[] selectionSort(int arr[]){
                for(int i =0;i<arr.length;i++){
                        int minIndex = i;
                        for(int j = i+1;j<arr.length;j++){
                                if(arr[minIndex]>arr[j]){
                                        minIndex = j;
                                }
                        }
                        int temp = arr[i];
                        arr[i]= arr[minIndex];
                        arr[minIndex]= temp;
                }
                return arr;
        }
	int[] duplicateNum(int arr[]){
		int sortArr[] = selectionSort(arr);

		int j = 0;
		for(int i =0;i<sortArr.length-1;i++){
			if(arr[i]==arr[i+1]){
				arr[j++]=arr[i];
			}
		}
		int temp[] = new int[j];
		for(int i = 0;i<j;i++){
			temp[i]= arr[i];
		}
		return temp;
	}
	  public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
                Array obj = new Array();
                int arr1[] = obj.duplicateNum(arr);
                for(int i = 0;i<arr1.length;i++){

                        System.out.println(arr1[i]);

                }
        }
}
			

