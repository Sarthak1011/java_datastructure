/*
 Que 9 : Find transition Point
Given a sorted array containing only 0s and 1s, find the transition point.
Example 1:
Input:
N = 5
arr[] = {0,0,0,1,1}
Output: 3
Explanation: index 3 is the transition point where 1 begins.
Example 2:
Input:
N = 4
arr[] = {0,0,0,0}
Output: -1
Explanation: Since, there is no "1", the answer is -1.
Expected Time Complexity: O(LogN)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 500000
0 ≤ arr[i] ≤ 1

 */

import java.io.*;

class Array {

/*	int transPoint(int arr[]){


		for(int i = 0;i<arr.length-1;i++){
			if(arr[i]!=arr[i+1]){
				return i+1;
			}
		}
		return -1;
	}*/

	int transPoint(int arr[]){

		int start = 0;
		int end = arr.length-1;

		while(start <= end){

			int mid = (start + end)/2;

			if(arr[mid] == 0){

				start = mid + 1;
			
			}else if(arr[mid]== 1){
				//mid == 0 beacause the array contain only one element and that is 1 then arr[mid - 1] is array index out of bound that handle the condition  mid > 0 is mid ! = -1 

				if(mid == 0 ||mid > 0 && arr[mid-1] == 0){
					
					return mid;
				}
				end = mid - 1;
			}
		}
		return -1;
	}
	 public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }

                Array obj = new Array();

		System.out.println(obj.transPoint(arr));
	 }
}

