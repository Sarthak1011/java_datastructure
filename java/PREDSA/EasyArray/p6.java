/*Que 6 : Second Largest
Given an array Arr of size N, print the second largest distinct element from an array.
Example 1:
Input:
N = 6
Arr[] = {12, 35, 1, 10, 34, 1}
Output: 34
Explanation: The largest element of the array is 35 and the second largest element
is 34.
Example 2:
Input:
N = 3
Arr[] = {10, 5, 10}
Output: 5
Explanation: The largest element of the array is 10 and the second largest element
is 5.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10
5
1 ≤ Arri ≤ 10
5

 */ 

import java.io.*;

class Array {

	int[] selectionSort(int arr[]){

		for(int i = 0;i<arr.length;i++){
			int maxIndex = i ;
			for(int j = i+1 ; j<arr.length;j++){
				if(arr[maxIndex]<arr[j]){
					maxIndex = j;
				}
			}
			int temp = arr[maxIndex];
			arr[maxIndex]= arr[i];
			arr[i]= temp;
		}
		return arr;
	}
					
	int secondLargeEle(int arr[]){

		int sortarr[] = selectionSort(arr);
		for(int i = 0;i<sortarr.length-1;i++){
			if(sortarr[i]!= sortarr[i+1]){
				return arr[i+1];
			}
		}
		return -1;
	}
	 public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array ");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter the Array Elements");

                int arr[]= new int[size];
                for(int i =0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
      
                Array obj = new Array();
                int arr1 = obj.secondLargeEle(arr);
                        System.out.println(arr1);
	 }
}

		

