import java.io.*;

class LengthDemo {

	int MyLength(String str1){

		char arr[]=str1.toCharArray();

		int len=0;
		for(int i=0;i<arr.length;i++){
			len++;
		}
		return len;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string:");
		String str = obj.readLine();

		System.out.println(str.length());

		LengthDemo obj1 = new LengthDemo();

		int len =obj1.MyLength(str);

		System.out.println(len);

	}
}
