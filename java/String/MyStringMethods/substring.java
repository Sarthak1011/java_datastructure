import java.io.*;

class SubStringDemo  {

	static String mySubString_1(String str1,int index){


		char arr1[]=str1.toCharArray();
		char arr2[]= new char[arr1.length];

		int j=0;
		for(int i=index;i<arr1.length;i++){
			arr2[j]=arr1[i];
			j++;
		}
		String str2 = new String(arr2);
		return str2;
	}
	static String mySubString_2(String str1,int start,int end){

		
		char arr1[]=str1.toCharArray();
		char arr2[]= new char[arr1.length];

		int j=0;
		for(int i=start;i<end;i++){
			arr2[j]=arr1[i];
			j++;
		}
		String str2 = new String(arr2);
		return str2;
	}
			
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string :");
		String str1 = obj.readLine();

		System.out.println("Enter the index");
		int index = Integer.parseInt(obj.readLine());

		String str2=mySubString_1(str1,index);
		System.out.println(str2);

		System.out.println("Enter the start and end");
		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		String str3=mySubString_2(str1,start,end);
		System.out.println(str3);
	}
}
