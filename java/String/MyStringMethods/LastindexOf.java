import java.io.*;

class LastIndexOfDemo {

	static int lastIndexOf(String str1,char ch,int index){

		char arr[]=str1.toCharArray();

		for(int i=index;i>=0;i--){
			if(arr[i]==ch){
				return i;
			}
			
		}
		return -1;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String:");
		String str1 = obj.readLine();

		//obj.skip(1);
		System.out.println("Enter the character");
		char ch = (char)obj.read();

		obj.skip(1);
		System.out.println("Enter the index ");
		int index = Integer.parseInt(obj.readLine());

		int ret = lastIndexOf(str1,ch,index);

		System.out.println(ret);
	}
}
