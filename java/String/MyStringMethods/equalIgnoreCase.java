import java.io.*;

class EqualIgnoreCaseDemo {


	static boolean myEqualsIgnoreCase(String str1 ,String str2){

		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();

		if(arr1.length == arr2.length){

			for(int i=0;i<arr1.length;i++){
				if(arr1[i]==arr2[i]||arr1[i]==arr2[i]+32 ||arr1[i]==arr2[i]-32){
					continue;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
		return true;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String:");
		String str1 = obj.readLine();

		System.out.println("Enter the String:");
		String str2 = obj.readLine();

		boolean ret = myEqualsIgnoreCase(str1,str2);
		System.out.println(ret);
	}
}


