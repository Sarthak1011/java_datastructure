import java.io.*;
import java.util.*;

class ConcatDemo {

	static String myStrConcat(String str1 , String str2){

		char carr1[]=str1.toCharArray();
		char carr2[]=str2.toCharArray();

		char carr3[]=new char[carr1.length+carr2.length];

		for(int i=0;i<carr1.length;i++){
			carr3[i]=carr1[i];
		}
		for(int i=0;i<carr2.length;i++){
			carr3[carr1.length+i]=carr2[i];
		}
		String str3 = new String(carr3);
		return str3;
	}
			
	public static void main(String args[])throws IOException{

		BufferedReader obj1 = new BufferedReader(new InputStreamReader(System.in));

		Scanner obj2= new Scanner(System.in);

		System.out.println("Enter the String: ");
		String str1 = obj1.readLine();
		
		System.out.println("Enter the String: ");
		String str2 = obj2.nextLine();

		String str3 = myStrConcat(str1,str2);

		System.out.println("concated String ");

		System.out.println(str3);
	}
}



		
