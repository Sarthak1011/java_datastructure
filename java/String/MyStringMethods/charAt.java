import java.io.*;

class CharAtDemo {

	static char myCharAt(String str , int index){

		char carr[]=str.toCharArray();

		for(int i=0;i<carr.length;i++){
			if(i==index){
				return carr[i];
			}
		}
		return 0;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string: ");
		String str = obj.readLine();

		System.out.println("Enter the index number ");
		int index = Integer.parseInt(obj.readLine());

		char ch = myCharAt(str,index);

		System.out.println("the character at "+index+" index is "+ch);
	}
}
