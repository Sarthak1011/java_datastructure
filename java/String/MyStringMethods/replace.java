import java.io.*;

class ReplaceDemo {

	static String myReplace(String str,char oldChar,char newChar){

		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){
			if(arr[i]==oldChar){
				arr[i]=newChar;
			}
		}

		String str1 = new String(arr);
		return str1;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String:");
		String str1 = obj.readLine();

		System.out.println("Enter the old character: ");
		char oldChar = (char)obj.read();

		obj.skip(1);
		System.out.println("Enter the new character: ");
		char newChar = (char)obj.read();

		String str2 = myReplace(str1,oldChar,newChar);

		System.out.println(str2);
	}
}
