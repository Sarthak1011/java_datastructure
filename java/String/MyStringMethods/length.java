import java.io.*;

class LengthDemo {

	static int myStrLen(String str1){

		char arr[]=str1.toCharArray();

		int count=0;

		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}

	public static void main(String args[])throws IOException{
	
		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str1 = obj.readLine();

		int len = myStrLen(str1);
		System.out.println(" Length of String " + str1+ " is "+len);
	}
}



