/*
 myThread obj = new MyThread thread is created when obj is created the parent class constructor is created then in MyThread class super line is their in Thread constructor thier is vm.create() method is thier and thread is created  
 when obj.start() method is call then thread is start for run  in start() method run method is called 
 */ 
class Demo extends Thread {

	public void run(){

		System.out.println("In Demo Run: "+Thread.currentThread().getName());//thread-1

	}
}
class MyThread extends Thread {

	public void run(){
	
		System.out.println("In myThread run: "+Thread.currentThread().getName());//thread-0
		Demo obj = new Demo();
		obj.start();

	}
}

class ThreadDemo {
	 
	public static void main(String args[]){

		System.out.println("ThreadDemo: "+Thread.currentThread().getName());//main
		MyThread obj = new MyThread();
		obj.start();
	}
}
	
