//create the thread then 4 things are complusory
//1.extend thread class 
//2.start the thread class 
//3.override run method
//4.do not override start method
//Vm.Create() create the thread

class Mythread extends Thread {
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
	public void start(){//do not override the start method 
		System.out.println("In start");
		run();
	}
}
class ThreadDemo {

	public static void main(String args[]){

		Mythread obj = new Mythread();
		obj.start();
		System.out.println(Thread.currentThread().getName());
		
	}
}

