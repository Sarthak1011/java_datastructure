class Country {
	Country(){
		System.out.println("Country:India");
	}
	void PrimeMinister(){
		System.out.println("Prime Minister:Narendra Modi");
	}
}
class State extends Country {
	State(){
		System.out.println("State:Maharastra");
	}
}
class Dist extends State {
	Dist(){
		System.out.println("Dist:Pune");
	}
}
class Tal extends Dist {
	Tal(){
		System.out.println("Tal:Bhor");
	}
}
class Village extends Tal {
	Village(){
		System.out.println("Village:Kikvi");
	}
}
class Home {
	public static void main(String args[]){
		Country obj1 = new Country();
		Village obj2 = new Village();
		obj2.PrimeMinister();
		Country obj3 = new State();
		
	}
}
