class Parent {

	Parent(){//parent(Parent this)

		System.out.println("bike");

	}

	void ParentProperty(){//ParentProperty(Parent this)

		System.out.println("flat ,gold");
	}
}
class Child extends Parent{
	
          	//Child(Child this)
	Child(){
		System.out.println("Carrer");
	}
}

class Client {

	public static void main(String args[]){

		Parent obj1 = new Parent();//Parent(obj1)
		obj1.ParentProperty();
	
		Child obj2 = new Child();//Child(obj2)
		obj2.ParentProperty();

		Parent obj3 = new Child();//Child(obj3)
		obj3.ParentProperty();
	}

}

