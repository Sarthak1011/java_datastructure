//handle the abnormal termination thats why exception handling are use
//after throws keyword compile time exception are write
//convert the abnormal code  into the normal code is called exception handling
//in try block their are max 2-3 lines code are there and in try block only risky code code is there 
//in catch block exception are handle thats why handling code is present in the catch block
//
//java.lang.Object --> 
//               java.lang.Throwable -->
//                                   java.lang.Exception --> 
//                                                       java.lang.RuntimeException -->
//                                                                            java.lang.ArithmeticException -->

import java.io.*;
class Demo {
	public static void main(String args[]){

		System.out.println("Start main");
		try{
			System.out.println(10/0);

		}catch(ArithmeticException obj ){

			System.out.println("Exception Occured");
			System.out.println(obj);
		}
		System.out.println("End Main");

	}
}
