//when Exception occured then it return the object accross the stack frame

//throws keyword written in method chya pud
//throw keyword written in the method and it is last line of the method

import java.io.*;

class Demo {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader obj1 = new BufferedReader(new InputStreamReader(System.in));
		obj1.close();
		System.out.println("Enter the String");
		String str1 = obj.readLine();

		System.out.println(str1);

		obj.close();
	
		System.out.println("Enter the String");
		String str2 = obj1.readLine();

		System.out.println(str2);

	}
}
