/*
 java.lang.Object-->
                  java.lang.Throwable-->
		                   java.lang.Exception-->
				                      java.lang.RuntimeException-->
						                               java.lang.NullPointerException-->

 */ 

class Demo {

	void m1(){

		System.out.println("In m1");

	}
	public static void main(String args[]){

		Demo obj = new Demo();
		obj.m1();

		obj = null;

		try {
			obj.m1();

		}catch(NullPointerException obj1){

			System.out.println(obj1);
			Demo obj2  = new Demo();
			obj2.m1();
		}finally{

			System.out.println("Entered In Finally block");

		}
	}
}


