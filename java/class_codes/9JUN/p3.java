abstract class Defence {

	void duty() {

		System.out.println("Our Duty Is Protect The India");
	
	}
	abstract void dress();
}

class Navy extends Defence {

	void dress(){

		System.out.println("Navy Dress is White");

	}
}
class Client {

	public static void main(String args[]){

		Navy soldier = new Navy();
		soldier.duty();
		soldier.dress();
	}
}
