class Singleton {

	static Singleton obj = new Singleton();//static variable 1

	private Singleton(){//2

		System.out.println("Constructor");

	}
	static {//static block 3

		System.out.println("Sarthak");
	}
	static Singleton getObject(){//static method 4
		return obj;
	}
}
class Client {

	public static void main(String args[]){

		Singleton obj1 = Singleton.getObject();
		System.out.println(obj1);		
		Singleton obj2 = Singleton.getObject();
		System.out.println(obj2);
		
	}
}
