class Parent {

	int x = 10;
	static int y = 20;

	static {

		System.out.println("In static parent");

	}
	Parent() {

		System.out.println("IN parent constructor");

	}

	void MethodOne(){

		System.out.println(x);
		System.out.println(y);

	}
	void MethodTwo(){

	//	System.out.println(x);
		System.out.println(y);

	}
}
class Child extends Parent {

	static {

		System.out.println("In child");

	}
	Child(){

		System.out.println("Child Constructor");

	}
}

class Client {

	public static void main(String args[]){

		Child obj = new Child();

		obj.MethodOne();
		obj.MethodTwo();
	}
}
