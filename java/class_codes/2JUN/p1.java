class Object extends Child{

	Object(){//parent(parent this)

		System.out.println("In Object Constructor");

	}
}
class Child extends Client{

	Child(){//child(child this)

		System.out.println("In child constructor");

	}
}

class Client {//cyclic inheritance not allowed 

	public static void main(String args[]){

		Object obj1 = new Object();
		Child obj = new Child();//Child(obj);

	}
}
