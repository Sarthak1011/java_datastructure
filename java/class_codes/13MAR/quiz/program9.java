
class C2W {
        public static void main(String args[]){

		int var=5;
		System.out.println("Inside main");
		while(var-- >3);{
			System.out.println("inside while");
		}
        }
}

/*
 1.inside main
 2.inside main
   inside while
 3.inside main
 (code goes into infinte loop with no output)
 4.compile time error
 */
