/*
 1 A 2 B
 3 C 4 D 
 5 E 6 F 
 */ 


class c2w {
	public static void main(String args[]){

		int row=4;
		int num=1;
		char ch='A';

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j%2==1){
					System.out.print(num++ +" ");
				}else{

					System.out.print(ch++ +" ");
				}
			}
			System.out.println();
		
			}
	}

}

