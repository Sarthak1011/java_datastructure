class Demo {

	Demo(){
		System.out.println("Demo");
	}
	void fun(){
		System.out.println("In Demo fun");
	}
}
class Client {
	public static void main(String args[]){

		Demo obj = new Demo() {

			void fun(){

				System.out.println("In client fun");

			}
			Demo gun(){

				System.out.println(new Demo());
				return new Demo();
			}
		}.gun();
		obj.fun();
	
	}
}
