class c2w {

	public static void main(String args[]){

		int arr[];//no error in java but in C its error array size missing
			  //reason java says that the not fix size in array if we use new then array size define
	//        int arr1[4];//error
			 
		int arr2[] = new int[4];//this is the proper way-->|0|0|0|0|

		int arr3[] = new int[] {10,20,30,40};//this is the another way diagram -->|10|20|30|40|
						     //in the int[2]{10,20} this is the wrong 

		int arr4[]= new int[]{10,20};  //this also way

		int arr5[] = {10,20,30,40,50};//arr[5]this is also wrong
					     


		System.out.println(arr4[3]);//runtime error array index out of bound
		for(int i=0;i<=3;i++){
			System.out.println(arr5[i]);
		}

	        

	}
}
