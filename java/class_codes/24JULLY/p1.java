
/*
 Concurrency method in thread class 
 1.sleep()
 2.join()
 3.yield()
1.sleep()-->

	public static void sleep(long) throws java.lang.InterruptedException;
	public static void sleep(long, int) throws java.lang.InterruptedException;
	public static void sleep(java.time.Duration) throws java.lang.InterruptedException;


 */ 

class MyThread extends Thread {

	public void run(){
		try{
			Thread.sleep(1000,2000);
		}catch(InterruptedException Ie){
		}
		System.out.println(Thread.currentThread());//Thread[thread-0,5,main]

	}

}

class ThreadDemo {

	public static void main(String args[]){

		System.out.println(Thread.currentThread());//Thread[main,5,main]
		MyThread obj = new MyThread();//create thread
		obj.start();//start the thread
	         try{
		Thread.sleep(1000);//sleep()
		}catch(InterruptedException Ie){
		}
		Thread.currentThread().setName("Core2Web");//main thread name replace with Core2Web
		System.out.println(Thread.currentThread());//Thread[Core2Web,5,main]
	}
}
