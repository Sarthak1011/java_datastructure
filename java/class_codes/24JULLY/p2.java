/*
[Thread obj = Thread.currentThread();]
 join says that first u  complete your work then i do
 public final void join(long) throws java.lang.InterruptedException;
  public final void join(long, int) throws java.lang.InterruptedException;
  public final void join() throws java.lang.InterruptedException;
  public final boolean join(java.time.Duration) throws java.lang.InterruptedException;
 */ 

class MyThread extends Thread {

	public void run(){

		for(int i = 0;i<10;i++){
		
			System.out.println(Thread.currentThread());
		
		}
	}
}
class ThreadDemo {

	public static void main(String args[]){

		MyThread obj = new MyThread();
		obj.start();
	
		try{

			obj.join();//main thread says that Mythread class(thread-0) u first complete your work then i do my work
		}catch(InterruptedException Ie){
		}

		for(int i = 0;i<10;i++){
		
			System.out.println(Thread.currentThread());
		
		}
	}
}
		
