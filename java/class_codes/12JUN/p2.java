interface Demo {

	void fun();

	void gun();

}
abstract class Child1 implements Demo {

	public void fun(){

		System.out.println("In fun");

	}
}
class Child2 extends Child1 {

	public void fun(){

		System.out.println("In child2 fun");

	}
	public void gun(){

		System.out.println("In  child2 gun");

	}
}
class Client {

	public static void main(String args[]){
		Demo obj1 = new Child2();
		Child1 obj2 = new Child2();
		Child2 obj3= new Child2();

		obj1.fun();
		obj1.gun();

		obj2.fun();
		obj2.gun();

		obj3.fun();
		obj3.gun();

	}
}
