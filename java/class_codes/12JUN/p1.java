/*
 in interface 100% abtraction is there
 in interface method have no body only declaration is there
 it is complusory is that the interface method body should be in child class otherwise is error 
 interface is class 
 interface have no constructor
 we cannot create object ofinterface but we call method using refernce
 in interface method are public abstract thats why we give body in child class then that method is also public otherwise scope in weaker 
  
 */ 
interface Demo {
	void fun();//public abstract void fun();
	void gun();//public abstract void gun();

}
class ChildDemo implements Demo {

	public void fun(){

		System.out.println("in fun");
	
	}
	public void gun(){

		System.out.println("In gun");

	}
}
class Client {

	public static void main(String args[]){

		Demo obj = new ChildDemo();
		obj.fun();
		obj.gun();
	}
}
