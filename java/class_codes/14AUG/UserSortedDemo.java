import java.util.*;
class SortedSetDemo implements Comparable {

	String str = null;
	SortedSetDemo(String str){
		this.str = str;
	}
	public int compareTo(Object obj){
	
		return (str.compareTo(((SortedSetDemo)obj).str));

	}
	public String toString(){
		return str;
	}
}
class Demo {

	public static void main(String args[]){

		TreeSet ts  =  new TreeSet();
		ts.add(new SortedSetDemo("Sarthak"));
		ts.add(new SortedSetDemo("Nikita"));
		ts.add(new SortedSetDemo("Sonya"));

		System.out.println(ts.first());//
		System.out.println(ts.last());//
                System.out.println(ts.headSet(new SortedSetDemo("Sonya")));//
                System.out.println(ts.tailSet(new SortedSetDemo("Nikita")));// 
               	System.out.println(ts.subSet(new SortedSetDemo("Nikita"),new SortedSetDemo("Sonya")));//
                System.out.println(ts.comparator());

		System.out.println(ts);

	}
}
