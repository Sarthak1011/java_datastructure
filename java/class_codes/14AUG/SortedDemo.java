/*
 Compiled from "SortedSet.java"
public interface java.util.SortedSet<E> extends java.util.Set<E> {
  public abstract java.util.Comparator<? super E> comparator();
  public abstract java.util.SortedSet<E> subSet(E, E);
  public abstract java.util.SortedSet<E> headSet(E);
  public abstract java.util.SortedSet<E> tailSet(E);
  public abstract E first();
  public abstract E last();
  public default java.util.Spliterator<E> spliterator();
}
 */
import java.util.*;
class Demo {

	public static void main(String args[]){

		TreeSet ts = new TreeSet();
		ts.add(20);
		ts.add(30);
		ts.add(10);
		ts.add(40);

		System.out.println(ts);//[10,20,30,40]
		System.out.println(ts.first());//10
		System.out.println(ts.last());//40
		System.out.println(ts.headSet(20));//[10]
		System.out.println(ts.tailSet(20));// [20 , 30 , 40]
		System.out.println(ts.subSet(20,40));//[20,30]
		System.out.println(ts.comparator());//null

	}
}

		

		




