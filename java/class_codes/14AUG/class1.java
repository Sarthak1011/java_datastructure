//TreeSet 
//the type of tree set is always comparable becase in tree set all the data is sorted thats why compare the data 
import java.util.*;

class Movies implements Comparable {

	String mName = null;
	float coll = 0.0f;

	Movies(String mName , float coll){

		this.mName = mName;
		this.coll = coll;

	}

	public int compareTo(Object obj){
		System.out.println("1");
	
		return (mName.compareTo(((Movies)obj).mName));

	}
	public String toString(){
		System.out.println("2");
		return mName + " : "+coll;
	}
}

class TreeSetDemo {

	public static void main(String args[]){

		TreeSet ts = new TreeSet();

		ts.add(new Movies("OMG2",150.4f));//first call goes to comapareTo method and then toString
		ts.add(new Movies("Gadar2",203.3f));
		ts.add(new Movies("SourceCode",222.4f));

		System.out.println(ts);
	
	}
}
