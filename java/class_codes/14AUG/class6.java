import java.util.*;
class ClassRoom {
        String studentName = null;
        float sgpa = 0.0f;
        ClassRoom (String studentName , float sgpa){
                this.studentName = studentName;
                this.sgpa = sgpa;
        }
        public String toString(){
                return "[ " + studentName + " - " + sgpa + " ] ";
        }
}
abstract class SortByName implements Comparator {
}
abstract class SortBySGPA implements Comparator {
}
class StudentRank{
        public static void main (String [] args){
                ArrayList al = new ArrayList();
                al.add(new ClassRoom("Sarthak",9.3f));
                al.add(new ClassRoom("Amar",8.3f));
                al.add(new ClassRoom("Vishal",5.2f));
                al.add(new ClassRoom("Harshal",7.0f));
                al.add(new ClassRoom("Manmohan",4.0f));
                System.out.println(al + "\n");

                Collections.sort(al,new SortByName(){
                         public int compare(Object obj1,Object obj2){
                                return ((((ClassRoom)obj1).studentName.compareTo(((ClassRoom)obj2).studentName)));
                        }
                });
                System.out.println(al + "\n");
                Collections.sort(al,new SortBySGPA(){
                         public int compare(Object obj1,Object obj2){
                                return (int) (((ClassRoom)obj1).sgpa - ((ClassRoom)obj2).sgpa);
                        }
                });
                System.out.println(al + "\n");
        }
}
