import java.util.*;
class TreeSetDemo implements Comparable {
        String str = null;
        int num = 0;

        TreeSetDemo(String str , int  num){
                this.str = str ;
                this.num = num;
                System.out.println("Str in constructor = " + str);
        }
        public String toString(){
                return str + " "+ num;
        }
        public int compareTo(Object obj){
                System.out.println("Str in compareTo = " + str);
                System.out.println("This = " + this);
                System.out.println("Obj = " + obj);
                System.out.println("Comparing to = " + (((TreeSetDemo)obj).str));

                return (str.compareTo(((TreeSetDemo)obj).str));
        }
}
class Demo {
        public static void main(String args[]){
                TreeSet ts = new TreeSet();

                ts.add(new TreeSetDemo("Sarthak",10));
                System.out.println("Sarthak Added\n");

                ts.add(new TreeSetDemo("Nikita",11));
                System.out.println("Nikita Added\n");

                ts.add(new TreeSetDemo("Shweta",3));
                System.out.println("Shweta Added\n");

                ts.add(new TreeSetDemo("Akash",2));
                System.out.println("Akash Added\n");

                System.out.println(ts);
       	}
}
