import java.util.*;

class PlayerRank implements Comparable {

	String pName = null;
	int runs  = 0;

	PlayerRank(String pName , int runs){

		this.pName = pName;
		this.runs = runs;

	}

	public int compareTo(Object obj){
	
		return (runs -((PlayerRank) obj).runs);

	}

	public String toString(){

		return pName + ":"+runs;

	}
}

class Cricket {

	public static void main(String args[]){

		TreeSet ts = new TreeSet();

		ts.add(new PlayerRank("SuryaKumar Yadav" , 99877));
		ts.add(new PlayerRank("Rohit Sharma" , 12345));
		ts.add(new PlayerRank("Ishan Kishan" , 6534));
		ts.add(new PlayerRank("Tilak Varma" , 12344));

		System.out.println(ts);
	}
}

