import java.util.*;
class Apps {
        String aName = null;
        float size = 0.0f;

        Apps(String aName , float size){
                this.aName = aName;
                this.size = size;
        }
        public String toString (){

                return aName + " " + size ;
        }
}
class SortByAppName implements Comparator {

        public int compare(Object obj1 , Object obj2){

                return (((Apps)obj1).aName.compareTo(((Apps)obj2).aName));

        }
}
class SortByAppSize implements Comparator {

        public int compare(Object obj1 , Object obj2){

                return (int)(((Apps)obj1).size-((Apps)obj2).size);

        }
}

class Mobile {

        public static void main(String args[]){

                LinkedList ll = new LinkedList();
                ll.add(new Apps("Whatsapp",50.4f));
                ll.add(new Apps("Instagram",70.4f));
                ll.add(new Apps("Facebook",30.4f));

                System.out.println(ll);

               	Collections.sort(ll,new SortByAppName());
                System.out.println(ll);
                
		Collections.sort(ll,new SortByAppSize());
		System.out.println(ll);
        }
}


