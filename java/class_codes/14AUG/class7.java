import java.util.*;

class TreeSetDemo implements Comparable {

	String str = null;
	int num = 0;

	TreeSetDemo(String str , int  num){
		this.str = str ;
		this.num = num;
	}
	public String toString(){
		return str + " "+ num;
	}



	public int compareTo(Object obj){
		System.out.println("This  "+this+"   "+System.identityHashCode(this));
		System.out.println("Obj   "+obj+"    "+System.identityHashCode(obj));
	
		return (str.compareTo(((TreeSetDemo)obj).str));
	}
}
class Demo {

	public static void main(String args[]){

		TreeSet ts = new TreeSet();
		ts.add(new TreeSetDemo("Sarthak",10));
		ts.add(new TreeSetDemo("Nikita",11));
		ts.add(new TreeSetDemo("Shweta",2));
		ts.add(new TreeSetDemo("Akash",3));

		System.out.println(ts);
	}
}


