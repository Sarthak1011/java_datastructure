//Sort the list using the Collections class 
//if sort list using the user defined class then use the comparator interface and compare is comparator method 
//

import java.util.*;

class ArrayListSort {

		int num = 0;

		ArrayListSort(int num){

			this.num = num;

		}
		public String toString(){
			return ""+num;
		}
}

class SortByNum implements Comparator {
	//compare method is Comparator interface method he is override from 
	public int compare(Object obj1 ,Object obj2){
		return ((ArrayListSort)obj1).num - ((ArrayListSort)obj2).num;
	}
}
class ArrayDemo {

	public static void main(String args[]){

		ArrayList al = new ArrayList();
		al.add("Kanha");
		al.add("Ashish");
		al.add("Rahul");
		al.add("Badhe");
		al.add("Vishal");
		al.add("Manya");

		System.out.println(al);
		//predefined Sorting 
		Collections.sort(al);
		System.out.println(al);

		LinkedList ll = new LinkedList();
		ll.add(new ArrayListSort(10));//add(ll,new ArrayListSort(10));
		ll.add(new ArrayListSort(50));
		ll.add(new ArrayListSort(20));
		ll.add(new ArrayListSort(30));
		
		System.out.println(ll);

		//UserDefined Sorting 
		Collections.sort(ll,new SortByNum());
		System.out.println(ll);

	}
}
