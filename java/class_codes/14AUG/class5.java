import java.util.*;

class Demo implements Comparable {

	int num  = 0;

	Demo(int num){
		this. num = num;

	}
	public int compareTo(Object obj){
		return num - ((Demo)obj).num;
	}

	public String toString(){
		return ""+num;
	}
}

class CursorDemo{

	public static void main(String args[]){
		TreeSet ts = new TreeSet();
		ts.add(new Demo(20));
		ts.add(new Demo(10));
		ts.add(new Demo(30));
		ts.add(new Demo(40));

		Iterator itr = ts.iterator();

		while(itr.hasNext()){

			System.out.println(itr.next());

		}
	}
}
