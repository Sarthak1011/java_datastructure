import java.util.*;
class ListDemo {
	String str = null;
	int num = 0;
	ListDemo(String str , int num){
		this.str = str;
		this.num = num;
	}
	public String toString(){
		return str + " "+num;
	}
}
class SortByName implements Comparator {
	public int compare(Object obj1 , Object obj2){							
		return (((ListDemo)obj1).str.compareTo(((ListDemo)obj2).str));
	}
}
class SortByNum implements Comparator {
	public int compare(Object obj1 , Object obj2){		
		return ((((ListDemo)obj1).num)-(((ListDemo)obj2).num));
	}
}
class ListSort {
	public static void main(String args[]){
		ArrayList al = new ArrayList();
		al.add(new ListDemo("Surya",4));
		al.add(new ListDemo("Rohit",3));
		al.add(new ListDemo("Yuraj",2));
		al.add(new ListDemo("Tilak",1));

		Collections.sort(al,new SortByName() {
			public int compare(Object obj1 , Object obj2){
				System.out.println("Anonymous Name");
				return (((ListDemo)obj1).str.compareTo(((ListDemo)obj2).str));
			}
		});
		System.out.println(al);
		Collections.sort(al,new SortByNum(){
			public int compare(Object obj1 , Object obj2){

				System.out.println("Anonymous Num");
				return ((((ListDemo)obj1).num)-(((ListDemo)obj2).num));
			}
		});
		System.out.println(al);
	}
}
