class StringBufferDemo {

	public static void main(String args[]){

		StringBuffer str = new StringBuffer();

		System.out.println(str);//
		System.out.println(str.capacity());//16

		str.append("shashi");
		
		System.out.println(str);//shashi
		System.out.println(str.capacity());//16

		str.append("Bagal");

		System.out.println(str);//shashiBagal
		System.out.println(str.capacity());//16

		str.append("Core2web");

		System.out.println(str);//shashiBagalCore2web
		System.out.println(str.capacity());//34____________________formula [(current capacity + 1)*2]

		str.append("Binescap12345");

		System.out.println(str);
		System.out.println(str.capacity());

	}
}


