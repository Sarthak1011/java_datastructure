//create threadgroup using runnable


class MyThread extends Thread implements Runnable {

	public void run(){

		System.out.println(Thread.currentThread());

	}

}
class ThreadDemo {

	public static void main(String args[]){

		ThreadGroup pThreadGP = new ThreadGroup("INDIA");
		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadGP , obj1 , "Maha");
		Thread t2 = new Thread(pThreadGP , obj2 , "Goa");

		t1.start();
		t2.start();
	}
}


