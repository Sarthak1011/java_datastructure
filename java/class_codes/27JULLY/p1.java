class MyThread extends Thread {
	MyThread(ThreadGroup tg , String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());

		try {
			Thread.sleep(10000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}



class ThreadDemo {
	public static void main(String args[])throws InterruptedException{
	
		ThreadGroup pThreadGP = new ThreadGroup("INDIA");

		MyThread obj1 = new MyThread(pThreadGP,"maha");
		MyThread obj2 = new MyThread(pThreadGP,"goa");
		obj1.start();
		obj2.start();

		System.out.println(pThreadGP.activeCount());//how many thread in this class show this method
		System.out.println(pThreadGP.activeGroupCount());
		
		Thread.sleep(5000);

		ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP , "Pakistan");
		
		MyThread obj3 = new MyThread(cThreadGP1,"Karaachi");
		MyThread obj4 = new MyThread(cThreadGP1,"lohore");

		obj3.start();
		obj4.start();
		
		System.out.println(pThreadGP.activeCount());//how many thread in this class show this method
		System.out.println(pThreadGP.activeGroupCount());
	}
}
