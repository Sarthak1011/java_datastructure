class StringLenDemo {

	int myStrLen(String str){

		char carr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<carr.length;i++){
			count++;
		}
		return count;
	}

	public static void main(String args[]){

		String str = "sarthak";


		StringLenDemo obj = new StringLenDemo();
		int len = obj.myStrLen(str);

		System.out.println(len);
	}
}
