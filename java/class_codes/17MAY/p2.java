/*             ___________________________________________Access Specifiers_______________________________________
 *             |                         |                                    |                                  |
 1. default access specifier   2.private access specifier           3.public access specifier          4.protected access specifier

 (access only that folder)    (accesss only that class variables)    (access all but with package)     (....)

 */

class Core2web{

	int noOfCourses = 8;
	private String favCourse = "C";

	void display(){

		System.out.println(noOfCourses);
		System.out.println(favCourse);
	}
}

class User {

	public static void main(String args[]){

		Core2web obj = new Core2web();

		obj.display();
		
		System.out.println(obj.noOfCourses);
		System.out.println(obj.favCourse);//favCourse has private access in Core2web
	}
}

	
