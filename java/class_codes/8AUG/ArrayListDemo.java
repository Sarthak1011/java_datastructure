import java.util.*;

class ArrayListDemo {

	public static void main(String args[]){

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(11.f);
		al.add("Sarthak");
		al.add('S');

		System.out.println(al);
		System.out.println(al.getClass());
		//instead of Object we use the var 
		//getClass().getName();
		for(Object i : al){
	//	System.out.println(i.getClass());
	
		System.out.println(i.getClass().getName());
	}
	System.out.println(al.remove(al));
	//System.out.println(al);

		/*
		  1.Iterator Cursor 
			Iterator itr = ArrayList/LinkedList/Vector.iterator();
			iterator is method  Iterator is Interface
		  		public abstract boolean hasNext();
  				public abstract E next();
 		 		public default void remove();
	 			public default void forEachRemaining(java.util.function.Consumer<? super E>);
		*/
		Iterator itr = al.iterator();
/*
		while(itr.hasNext()){
			itr.next();
			itr.remove();
		}*/


		System.out.println(al);

		/*
		 2.ListIterator
		 	public abstract boolean hasNext();
  			public abstract E next();
  			public abstract boolean hasPrevious();
  			public abstract E previous();
  			public abstract int nextIndex();
  			public abstract int previousIndex();
  			public abstract void remove();
  			public abstract void set(E);
  			public abstract void add(E);
		*/
		ListIterator litr = al.listIterator();

		while(litr.hasNext()){

			System.out.println(litr.next());

		}
		while(litr.hasPrevious()){

			System.out.println(litr.previous());

		}

		System.out.println(litr.nextIndex());
		System.out.println(litr.previousIndex());
		System.out.println(al.indexOf(litr.nextIndex()));
		litr.set("Nikita");
		System.out.println(al);
		litr.add("Sonya");
		System.out.println(al);

	}
}
