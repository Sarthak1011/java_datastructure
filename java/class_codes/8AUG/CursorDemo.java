import java.util.*;

class CursorDemo {

	public static void main(String args[]){

		System.out.println("IteratorCursor");

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);
	//	al.remove(1);  index

		Iterator itr = al.iterator();
		while(itr.hasNext()){

			//itr.remove();
			System.out.println(itr.next());
		}

		System.out.println("ListIteratorCursor");

		LinkedList ll = new LinkedList();
		ll.add(10);
		ll.add(20);
		ll.add(30);
		ListIterator itr1 = ll.listIterator();

		while(itr1.hasNext()){

			System.out.println(itr1.next());
		}
		while(itr1.hasPrevious()){

			System.out.println(itr1.previous());
		}

		System.out.println("EnumerationCursor");

		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		Enumeration e1 = v.elements();
		
		while(e1.hasMoreElements()){
		
			System.out.println(e1.nextElement());
		
		}

	}
}



