import java.util.*;
class LinkedListIterator {


	public static void main(String args[]){
			//ARRAYLIST
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		
		System.out.println("\tArrayList");
		Iterator itr = al.iterator();
		System.out.println(itr);
		System.out.println(itr.getClass().getName());

		ListIterator litr = al.listIterator();
		System.out.println(litr);
		System.out.println(litr.getClass().getName());

			//LINKEDLIST
		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.add(20);
		ll.add(30);

		System.out.println("\n \t LinkedList\n");
		itr = ll.iterator();
		System.out.println(itr);
		System.out.println(itr.getClass().getName());

		litr = ll.listIterator();
		System.out.println(litr);
		System.out.println(litr.getClass().getName());

		System.out.println("\n\t Vector \n");
			//VECTOR
		Vector v = new Vector();
		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		
		itr = v.iterator();
		System.out.println(itr);
		System.out.println(itr.getClass().getName());

		litr = v.listIterator();
		System.out.println(litr);
		System.out.println(litr.getClass().getName());


		       //STACK
		Stack s = new Stack();
		s.push(10);
		s.push(20);
		s.push(30);

		System.out.println("\n\t Stack\n");
		itr = s.iterator();
		System.out.println(itr);
		System.out.println(itr.getClass().getName());

		litr = s.listIterator();
		System.out.println(litr);
		System.out.println(litr.getClass().getName());

	}
}



