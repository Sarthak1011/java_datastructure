//REAL TIME EXAMPLE OF OVERLOADING


class IPL {

	void MatchInfo(String team1 , String team2){

		System.out.println(team1+" vs "+team2);

	}
	void MatchInfo(String team1 , String team2,String venue){

		System.out.println(team1+" vs "+team2);
		System.out.println("Venue = "+venue);

	}
}

class Client {

	public static void main(String args[]){

	IPL ipl2023 = new IPL();
	ipl2023.MatchInfo("Mumbai","Csk");
	ipl2023.MatchInfo("Mumbai","Csk","Wankhede Stadium");
	}
}
