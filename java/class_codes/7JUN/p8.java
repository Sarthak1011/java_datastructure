/*
 In two different classess (i.e parent child reltion)parent method has not acceptable to child then that method override by child is called the overriding
 in overriding return type is important if the primitive datatype is same for both methods and return type is class then that class have parent child relation

 ji method apun overide karto tila overriden method mantat
 ji method overide karte tila override method mantat
 varient means different 
 covarient means parent child  
 */ 
class Parent {

        Object fun(){

                System.out.println("In Parent Fun");

                return new Object();

        }
}
class Child extends Parent {

        Integer fun(){

                System.out.println("In child Fun");

                return 11;
        }
}
class Client {

        public static void main(String args[]){

                Parent p = new Child();
                p.fun();
        }
}
