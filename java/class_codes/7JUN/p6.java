class Parent {

	char fun(){

		System.out.println("In Parent Fun");

		return 'N';

	}
}
class Child extends Parent {

	int fun(){//error return type nit same

		System.out.println("In child Fun");

		return 11;
	}
}
class Client {

	public static void main(String args[]){

		Parent p = new Child();
	}
}


