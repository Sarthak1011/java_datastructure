import java.util.*;

class Demo implements Comparable{

	int data  = 0;

	Demo(int data){
		this.data = data;
	}
	public String toString(){

		return ""+data;
	}
	public int compareTo(Object obj){
		System.out.println("CompareTo");
		return (data- (((Demo)obj).data));
	}
}

class SortByData implements Comparator {

	public int compare(Object obj1 , Object obj2){

		System.out.println("Compare");
		return (((Demo)obj1).data-(((Demo)obj2).data));
	}
}

class SortDemo {

	public static void main(String args[]){

		PriorityQueue pq = new PriorityQueue();
		pq.offer(new Demo(10));
		pq.offer(new Demo(40));
		pq.offer(new Demo(30));
		pq.offer(new Demo(20));

		System.out.println(pq);
		ArrayList al = new ArrayList(pq);
		Collections.sort(al,new SortByData());
		System.out.println(al);
	}
}


