import java.util.*;

class HashMapDemo {

	String str = null;
	HashMapDemo(String str ){
		this.str = str;	
	}
	public String toString(){
		return "{"+str+ "}";
	}
	public static void main(String []args){

		HashMap hm = new HashMap();
		hm.put((new HashMapDemo("kanha")),"Infosys");
		hm.put((new HashMapDemo("rahul")),"BMC");
		hm.put((new HashMapDemo("Ashish")),"Barcleys");
		hm.put((new HashMapDemo("Badhe")),"Carpro");
		hm.put((new HashMapDemo("Shahsi")),"Bienscap");

		System.out.println(hm);
	}
}
