import java.util.*;

class Demo implements Comparable {

	int data = 0;

	Demo(int data){
		this.data = data;
	}
	public int compareTo(Object obj){

		return (data - (((Demo)obj).data));
	}
	public String toString(){
		return ""+data;
	}
}
class SortDemo {

	public static void main(String [] args){

		PriorityQueue pq = new PriorityQueue();
		pq.offer(new Demo(10));
		pq.offer(new Demo(40));
		pq.offer(new Demo(20));
		pq.offer(new Demo(50));

		System.out.println(pq);

		TreeSet ts = new TreeSet(pq);
		System.out.println(ts);
	}
}

