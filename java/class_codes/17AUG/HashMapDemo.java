import java.util.*;

class HashMapDemo {

	int num1= 0;
	HashMapDemo(int num1){
		this.num1 = num1 ;
	}
	public String toString(){
		return num1 + " ";
	}
}
class Demo{
     public static void main(String args[]){

		HashMap hm = new HashMap();
		hm.put(new HashMapDemo(10) ,100);
		hm.put(new HashMapDemo(20) ,200);
		hm.put(new HashMapDemo(30) ,300);
		hm.put(new HashMapDemo(20) ,300);
		hm.put(new HashMapDemo(20) ,200);
		
		hm.put("Sarthak",10);
		hm.put("Nikita",11);

		System.out.println(hm);
	}
}
