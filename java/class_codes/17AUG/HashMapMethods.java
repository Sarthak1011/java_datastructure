import java.util.*;

class HashMapDemo {

	public static void main(String args[]){

		HashMap hm = new HashMap();
		hm.put("Sarthak","Nikita");
		hm.put("Akash","Shweta");
		hm.put("Shashi","Pranali");

		System.out.println(hm);
/*public int size();
public boolean isEmpty();
public V get(java.lang.Object);*/
		System.out.println(hm.size());
		System.out.println(hm.isEmpty());//fakse
		System.out.println(hm.get("Sarthak"));//Nikita
		// public boolean containsKey(java.lang.Object);
		System.out.println(hm.containsKey("Sarthak"));//true


  // public java.util.Collection<V> values();
   		System.out.println(hm.values());
//  public java.util.Set<java.util.Map$Entry<K, V>> entrySet();
		System.out.println(hm.entrySet());
//  public V getOrDefault(java.lang.Object, V);
 		System.out.println(hm.getOrDefault("Sarthak","Akash"));
//  public V putIfAbsent(K, V);
 		System.out.println(hm.putIfAbsent("Sarthak","Niki"));
// public boolean remove(java.lang.Object, java.lang.Object);
 		System.out.println(hm.remove("Sarthak","Nikita"));
  //public boolean replace(K, V, V);
		System.out.println(hm.replace("Akash","Shweta","Shwetu"));
		System.out.println(hm);
//  public V replace(K, V);
		System.out.println(hm.replace("Akash","Shweta"));
		hm.put("Sarthak","Nikita");
		System.out.println(hm);
/*  public V computeIfAbsent(K, java.util.function.Function<? super K, ? extends V>);
 public V computeIfPresent(K, java.util.function.BiFunction<? super K, ? super V, ? extends V>);
  public V compute(K, java.util.function.BiFunction<? super K, ? super V, ? extends V>);
  public V merge(K, V, java.util.function.BiFunction<? super V, ? super V, ? extends V>);
  public void forEach(java.util.function.BiConsumer<? super K, ? super V>);
  public void replaceAll(java.util.function.BiFunction<? super K, ? super V, ? extends V>);
  public java.lang.Object clone();
 */ 


	}
}

