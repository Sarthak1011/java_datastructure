import java.util.*;

class LinkedHashDemo {

	String str= null;
	LinkedHashDemo(String str){
		this.str = str;
	}
	public String toString(){
		return ""+str;
	}
	public void finalize(){
		System.out.println("Notify");
	}
	public static void main(String args[]){

		LinkedHashMap lhm = new LinkedHashMap();
		lhm.put("Sarthak","Equitech");
		lhm.put("Nikita","NVDIA");
		lhm.put("Shweta","Veritax");
		lhm.put("Aakash","Infosys");
		lhm.put("Nikita","Equitech");

	
		System.out.println(lhm);
		lhm = null;
		System.gc();
	/*	lhm.put(new LinkedHashDemo("Sarthak"),"Equitech");
		lhm.put(new LinkedHashDemo("Nikita"),"NVDIA");
		lhm.put(new LinkedHashDemo("Shweta"),"Veritax");
		lhm.put(new LinkedHashDemo("Aakash"),"Infosys");
		lhm.put(new LinkedHashDemo("Shweta"),"capgeminia");
	
		System.out.println(lhm);
		lhm=null;
		System.gc();
*/

	}
}

