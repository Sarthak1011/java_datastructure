import java.util.*;

class Demo {

	int data = 0;

	Demo (int data){
		this.data= data;
	}

	public String toString(){
		return ""+data;
	}
}
class SortByData implements Comparator{

	public int compare(Object obj1 , Object obj2){
		return (((Demo)obj1).data - (((Demo)obj2).data));
	}
}

class SortDemo {

	public static void main(String [] args){

		HashSet hs = new HashSet();
		hs.add(new Demo(10));
		hs.add(new Demo(40));
		hs.add(new Demo(30));
		hs.add(new Demo(20));

		System.out.println(hs);

		ArrayList al = new ArrayList(hs);

		Collections.sort(al,new SortByData());

		System.out.println(al);
	}
}
