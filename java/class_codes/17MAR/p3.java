class c2w {
	public static void main(String args[]){

		int x=10;
		{
			int y=20;
			System.out.println(x+" "+y);
		}
		{
			System.out.println(x+" "+y);//out of scope
		}
		System.out.println(x+" "+y);//out of scope
	}
}
