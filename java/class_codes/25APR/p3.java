class ArrayDemo {

	static void fun(int xarr[]){
		for(int x:xarr){
			System.out.println(System.identityHashCode(x));
		}
	}

	public static void main(String args[]){

		int arr[]={10,20,30};
		for(int x:arr){
			System.out.println(System.identityHashCode(x));
		}
		fun(arr);
	}
}
