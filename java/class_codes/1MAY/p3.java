class StringDemo {
	public static void main(String args[]){

	
		String str1 = "Sarthak";
		String str2 = new String("Kachare");

		System.out.println(str1 +"=" +System.identityHashCode(str1)+"This is string constat pool string");
		System.out.println(str2 +"=" +System.identityHashCode(str2)+"This is Heap section string ");
		
		String str3="Sarthak";
		String str4=new String("Kachare");
		
		System.out.println(str3 +"=" +System.identityHashCode(str3)+"This is String constat pool  string");
		System.out.println(str4 +"=" +System.identityHashCode(str4)+"This is an heap section string");

	}
}


