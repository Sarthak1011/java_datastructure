class Bitwise {
	public static void main(String []args){
		int x=5;
		int y=7;

		System.out.println(x & y); //5
		System.out.println(x | y);//7
		System.out.println(x ^ y);//2
		System.out.println(x << 2);//20
		System.out.println(x >>2);//1
		System.out.println(x >>>2);//1
	}
}
