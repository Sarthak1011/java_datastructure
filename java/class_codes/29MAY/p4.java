class Demo {

	int x = 1;

	static int y= 2;

	Demo(){
		this(10,20);
		System.out.println(x);//internally this.x
		System.out.println(y);//this.y -->y is in static block so here this has 1000 reference and go to ptr to special structure and then access static variable
	}
	 public  Demo(int x , int y){

		System.out.println(x);
		System.out.println(y);
		System.out.println(this.x);
		System.out.println(this.y);
	}

	public static void main(String args[]){

		Demo obj = new Demo();
	}
}
