//Method Signature



class Demo {

	Demo(){
	
		System.out.println("Constructor 1");

	}

	Demo(){

		System.out.println("Constructor 2");
	}
}

/*
 here 2 same demo constructor thats why there is error here

 method table ver sgly method astat tyaver sglya method hya unique astat tya vr method ch nav an tyanchya parameter cha datatype asto method chya name varun and parameter varun tarvl jat same method konti an vegli konti method table ver duplicate method nastat 


 - in c language there is no concept of the method overloading thats why c not accepts the same name of the method \
 - in cpp there is concept of the name mangling thats whys cpp accepts the same method name 
 - in java there is concept of the method signature thats why here accepts the same method name
 */ 
