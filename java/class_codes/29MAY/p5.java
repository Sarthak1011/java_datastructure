class Demo{

	int x = 10;

	Demo(){
		this(10);
	}
	Demo(int x){

		this();
	}

	public static void main(String args[]){
		Demo obj = new Demo();
	}

}
//recursive constructor invocation(method call)
//error recursive call not allowed here
