import java.io.*;
import sp_num_codes.imp;

class Client {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		char ch;

		do {
			System.out.println("1.palindrome Number");
			System.out.println("2.AutoMorphic Number");
			System.out.println("3.Strong Number");
			System.out.println("4.perfect Number");
			System.out.println("5.Armstrong Number");
			System.out.println("6.perfectSquare Number");
			System.out.println("7.Prime Number");
			System.out.println("8.Reverse Number");
			System.out.println("9.factorial of Number");
			System.out.println("10.fabonocci series");
			System.out.println("Enter your choice");
			int choice = Integer.parseInt(obj.readLine());

			System.out.println("Enter the num ");
			int num = Integer.parseInt(obj.readLine());

			SpCodes sp = new SpCodes();
			switch(choice){

				case 1:
					{
						int pal = sp.palindromeNum( num);
						if(pal == 0 ){
							System.out.println(num + " is a Palindrome Number");
						}else{
							System.out.println(num + " is Not a Palindrome Number");
						}
						break;
					}
				case 2:
					{

						int auto =sp.autoMorphicNum(num);
		
						if(auto == 0 ){
							System.out.println(num + " is a AutoMorphic Number");
						}else{
							System.out.println(num + " Not a AutoMorphic Number");
						}
						break;
					}
				case 3:
					{
						int str =sp.strongNum(num);
						if(num == 0)
							System.out.println("0 is not Strong number");
		
						if(str == 0 ){
							System.out.println(num + " is a Strong Number");
						}else{
							System.out.println(num + " Not a Strong Number");
						}
						break;
					}
				case 4:
					{
						int per =sp.perfectNum(num);
		
						if(per == 0 ){
							System.out.println(num + " is a Perfect Number");
						}else{
							System.out.println(num + " Not a Perfect Number");
						}
						break;
					}
				case 5:
					{
						int arm =sp.armStrongNum(num);
		
						if(arm == 0 ){
							System.out.println(num + " is a ArmStrong Number");
						}else{
							System.out.println(num + " Not a ArmStrong Number");
						}
						break;
					}
				case 6:
					{
						int persq =sp.perfectsqNum(num);
		
						if(persq == 0 ){
							System.out.println(num + " is a PerfectSquare Number");
						}else{
							System.out.println(num + " Not a PerfectSquare Number");
						}
						break;
					}
				case 7:
					{
						int pri =sp.primeNum(num);
						if(num <=0)
							System.out.println("not prime");
		
						if(pri == 0 ){
							System.out.println(num + " is a Prime Number");
						}else{
							System.out.println(num + " Not a Prime Number");
						}
						break;
					}
				case 8:
					{
						int rev =sp.reverseNum(num);
		
					
							System.out.println("Reverse Number is "+rev);
						
						break;
					}

				case 9:
					{
						int fact =sp.factorialNum(num);
		
						if(fact == 0 ){
							System.out.println("Factorial of "+num +" is "+fact);
						}else{
							System.out.println("Factorial of "+num +" is "+fact);
						}

						break;
					}
				case 10:
					{
						int fabo =sp.fabonacciNum(num);
						System.out.println("Fabonacci number of "+num + " is "+fabo);
						break;
					}
			}

			System.out.println("Do you want to find another number then enter y or n");
			ch =(char)obj.read();
			obj.skip(1);
		}while(ch == 'y'||ch =='Y');
	}
}
