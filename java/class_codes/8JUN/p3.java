class Parent {

	private void fun(){//private access 

		System.out.println("In private parent fun");

	}
}
class Child extends Parent {

	void fun(){

		System.out.println("In default fun");

	}
}

class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.fun();
	}
}
