class Parent {

	protected void fun(){

		System.out.println("IN protected parent fun");

	}
}
class Child extends Parent {

	void fun(){

		System.out.println("in default child fun");
	
	}
}
class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.fun();
	}
}
