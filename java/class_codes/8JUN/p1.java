/*
 technical example of overriding
  In object class equal method check the address and in string class method equal check the content that string class method is override from object class  method 

  we cannot extend string class because it is final 
  in java one class have not two parent beacuse java do not support the multiple inheritance

  Integer class cha parent class Number class ahe 

  overloading -->compile time polymorphism/early binding
  overriding --> run time polymorphism/late binding
--------------|------------------------------------------------|--------------------------------------------------------------------------------------------
  parameter   |              overloading                       |              overriding
  ------------|------------------------------------------------|--------------------------------------------------------------------------------------------
  other name  | compile time polymorphism/early binding        |     run time polymorphism/late binding
--------------|------------------------------------------------|--------------------------------------------------------------------------------------------
  paramter    | not same paramter different paramter           |     parameter should be different
              | requied and paramter length shold be different |
--------------|------------------------------------------------|-------------------------------------------------------------------------------------------
  return type | no matter return type                          |    if primittive datatype the complusory same 
  	      |						       |    if classes is their then that have parent child relation 
  ------------|------------------------------------------------|-------------------------------------------------------------------------------------------    access     | no matter access specifier                     |     impact on overriding because the if scope of access specifier of child class  method is  specifier   |                                                |        larger than the parent class method access specifier  if small then error(attempting              |                                                |         to assign weaker access privilage was ______)
   -----------|------------------------------------------------|--------------------------------------------------------------------------------------------
   static     | not matter static modifier (in compile time in |  static modifier not allowed in overriding beacause static method cannot override static  
   modifier   |method table only method name and parameter go) |  is bing class but in overriding parent child relation two classes required if two method 
              |                                                | method are static then  which object  refernce is their that method called
  ------------|------------------------------------------------|-------------------------------------------------------------------------------------------
   final      |not matter final modifier (in compile time in   | final class we cannot extend and final method we cannot override 
 modifier     |method table only method name and parameter go) |
 -----------------------------------------------------------------------------------------------------------------------------------------------------------      
   */ 


class Parent {

	public void fun(){//public scope

		System.out.println("In public parent fun");

	}
}
class Child extends Parent {

	void fun(){//default scope     error because weaker scope as compair to public

		System.out.println("Default child fun");

	}
}
class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.fun();
	}
}

	
