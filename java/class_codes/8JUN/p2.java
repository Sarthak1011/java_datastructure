class Parent {

      void fun(){//default scope

	     System.out.println("IN fun");

     }
}
class Child extends Parent {

     public   void fun(){//public scope    

	     System.out.println("In para");

     }
}
class Client {

	public static void main(String args[]){

		Demo obj = new Demo();
		obj.fun();
	}
}
