class Parent {

	  void fun(){

		System.out.println("In parent fun");

	}

}

class Child extends Parent {

	final void fun(){

		System.out.println("IN child fun");
	
	}
}

class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.fun();
	}
}
