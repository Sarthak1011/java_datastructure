class Parent {

	public void fun(){

		System.out.println("In fun");

	}
}
class Child extends Parent {

	private void fun(){

		System.out.println("In child fun");

	}
}
class Client {

	public static void main(String args[]){

		Parent obj = new Child();

		obj.fun();
	}
}
