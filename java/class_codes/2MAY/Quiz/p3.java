class c2w {

	public static void main(String args[]){

		
		String svar1 = "This is a string";//SCP  100
		String svar2 = "a string ";//SCP  200

		String svar3 = "This is"+svar2;//HEAP  300    // +(concat) internally call the append method and append method return new String append mathod is string builder method

		if(System.identityHashCode(svar1) == System.identityHashCode(svar3)){
			System.out.println("Equal");
		}else{
			System.out.println("Not equal");
		}
	}
}

