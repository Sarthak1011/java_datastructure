class c2w {

	public static void main(String args[]){

		String str1 = new String("java");
		String str2 = new String("java");

		System.out.println(str1 == str2);//fasle because the == internally check the identity hash code and this strings are memeory allocate in heap and in heap section object created seprated
		System.out.println(str1.equals(str2));//equals method check the content of the string

		System.out.println(System.identityHashCode(str1));//100
		System.out.println(System.identityHashCode(str2));//200
		
		System.out.println(str1.hashCode());//300 if content of two string is same that hash code is same
		System.out.println(str2.hashCode());//300
	}
}
