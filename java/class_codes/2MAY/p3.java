class StringDemo {

	public static void main(String args[]){

		String str1 ="Shashi";//SCP 100
		String str2 = "Bagal";//SCP 200

		System.out.println(str1 + str2);//ShashiBagal

		String str3 = "ShashiBagal";//SCP 300
		String str4 = str1 + str2;//HEAP 400

	
		System.out.println(System.identityHashCode(str3));//300
		System.out.println(System.identityHashCode(str4));//400
	}
}
