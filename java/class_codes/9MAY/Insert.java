import java.io.*;

class InsertDemo {

	static String MyInsert(StringBuffer str1 ,int index,String str2){
		
		String str3 = str1.toString();
		char arr1[]=str3.toCharArray();

		char arr2[]=str2.toCharArray();

		char arr3[]=new char[arr1.length+arr2.length];

		for(int i=0;i<index;i++){
			arr3[i]=arr1[i];
		}
		for(int i=0;i<arr2.length;i++){
			arr3[i+index]=arr2[i];
		}

		for(int i=0;i<arr1.length-index;i++){
			arr3[index+arr2.length+i]=arr1[index+i];
		}
		String str4 = new String(arr3);
		return str4;
	}



	public static void main(String args[])throws IOException {

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String:");
		StringBuffer str1 = new StringBuffer(obj.readLine());

		System.out.println("Enter the index number for adding string:");
		int index = Integer.parseInt(obj.readLine());

		System.out.println("Enter the string:");
		String str2 = obj.readLine();


//		System.out.println(str1.insert(index,str2));

		String str3 = MyInsert(str1,index,str2);
		System.out.println(str3);

	}
}
