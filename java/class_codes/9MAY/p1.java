class SBDemo {

	public static void main(String args[]){

		StringBuffer str = new StringBuffer(100);//Initial Capacity

		str.append("Binecaps");
		str.append("Core2web");

		System.out.println(str);//BinecapsCore2web
		System.out.println(str.capacity());//100

		str.append("Incubator");

		System.out.println(str);//BinecapsCore2webIncubator
		System.out.println(str.capacity());//100
	}
}
