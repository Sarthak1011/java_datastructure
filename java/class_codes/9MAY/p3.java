class SBDemo {

	public static void main(String args[]){
		
                String str1 ="Shashi";//Scp Immutable  when we change in the String create new Object
                String str2 = new String("Bagal");//Heap  Immutable same here
                StringBuffer str3 = new StringBuffer("Core2web");//Heap  Muttable   when we change in the String change in string but not create another object that change extend in this object

		//String str4 = str1.concat(str3);//it is wrong because the concat method parameter applicable for the string not stringBuffer 
		StringBuffer str5 = str3.append(str2);//it is true append method parameter applicable for the both string and stringBuffer

		System.out.println(str1);//Shashi
		System.out.println(str2);//Bagal
		System.out.println(str3);//Core2WebBagal
		//System.out.println(str4);//error
		System.out.println(str5);//Core2webBagal
	}
}
