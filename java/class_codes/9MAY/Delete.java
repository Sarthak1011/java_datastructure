import java.util.*;
class DeleteDemo {

	static StringBuffer myDelete(StringBuffer str1, int start,int end){


		String str2 = str1.toString();
		char arr1[]=str2.toCharArray();
		char arr2[]=new char[arr1.length];
		int j=0;
		for(int i=0;i<arr1.length;i++){
			if( i<start || i>=end){
				
				arr2[j]=arr1[i];
				j++;
			}
		}
		String str3 = new String(arr2);
		StringBuffer str4 = new StringBuffer(str3);
		return str4;
	}
	public static void main(String args[]){
		Scanner obj = new Scanner(System.in);

		System.out.println("Enter the string:");
		StringBuffer str1 = new StringBuffer(obj.nextLine());

		System.out.println("Enter the starting and ending number for delete String:");
		int start = obj.nextInt();
		int end = obj.nextInt();

		//System.out.println(str1.delete(start,end));

		StringBuffer str2 = myDelete(str1,start,end);

		System.out.println(str2);
	 
	}
}

