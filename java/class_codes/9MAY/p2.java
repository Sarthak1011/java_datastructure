class SBDemo {

	public static void main(String args[]){

		String str1 ="Shashi";//Scp Immutable  when we change in the String create new Object
		String str2 = new String("Bagal");//Heap  Immutable same here
		StringBuffer str3 = new StringBuffer("Core2web");//Heap  Muttable   when we change in the String change in string but not create another object that change extend in this object 

		//String str4 = str1.append(str3);//here str1 is string class string but str3 is StringBuffer String and append method is StringBuffer class that  method method we apply on string class string thats why here is error 

		StringBuffer str5 = str3.append(str1);//here str3 is StringBuffer string and append also stringBuffer method we operate on str3 thats true and append method applicable for both string and stringBuffer string thats why there is no error and that store in stringBuffer String beacause their return type is new StringBuffer

		System.out.println(str1);//Shashi
		System.out.println(str2);//Bagal
		System.out.println(str3);//Core2webSashi -->> beause stringBuffer create only one object when we operates on it thats why this output get
		//System.out.println(str4);
		System.out.println(str5);//Core2webShashi
	}
}
//append method parameters applicable for string and string buffer -------->stringBuffer method
//concat method parameters only applicable for the String ----------->String class method
//StringBuffer return the new StringBuffer thats why we operate on the stringBuffer string then that new string store in StringBuffer
