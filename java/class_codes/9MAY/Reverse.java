import java.io.*;

class ReverseDemo {

	static StringBuffer myReverse(StringBuffer str1){

		String str2 = str1.toString();
		char arr1[]=str2.toCharArray();
		char arr2[]=new char[arr1.length];

		int j=arr1.length-1;
		for(int i=0;i<arr1.length;i++){
			if(i<j){
		        	char temp = arr1[i];
		        	arr1[i]=arr1[j];
		        	arr1[j]=temp;
		        	j--;
			}
		}
		String str3 = new String(arr1);
		StringBuffer str4 = new StringBuffer(str3);
		return str4;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj  = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String:");
		StringBuffer str1 = new StringBuffer(obj.readLine());

		//System.out.println(str1.reverse());
		StringBuffer str2 = myReverse(str1);
		System.out.println(str2);

	}
}
