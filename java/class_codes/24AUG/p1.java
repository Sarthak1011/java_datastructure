import java.util.concurrent.*;

class BlockingQueueDemo {

	public static void main(String [] args){

		BlockingQueue bQueue = new ArrayBlockingQueue();
		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);
		bQueue.offer(40);

		System.out.println(bQueue);
	}
}

