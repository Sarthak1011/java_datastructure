/*
 A  1  B  2
 C  3  D  
 E  4  
 F
*/

class c2w {
	public static void main(String args[]){

		int row=4;
		char ch='A';
		int num=1;

		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){

				if(j % 2 == 0){
					System.out.print(ch++ +" ");
				}else{
					System.out.print(num++ +" ");
				}

			}
			System.out.println();
		}
	}
}


