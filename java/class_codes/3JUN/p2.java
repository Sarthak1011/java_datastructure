/*
 Polymorphism has two types  1)overloading(Same class) 2)overrinding(different class parent child reltion)
 example overloading 
 person behaviour -family,friends,office
 println print - string , integer, float , object
 same class same method error

in overloading  no matter return type of method
 */ 

class Demo {

	//same method name and same parameter error
	void fun(int x ){

		System.out.println(x);

	}
	void fun(int x){//error fun already define 

		System.out.println(x);

	}
	//no matter return type
	int fun(int x){

		System.out.println(x);
	}
}


