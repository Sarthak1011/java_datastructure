/*
 init means call the constructor constructor 
 this()--> call the class constructor
 super()--> call the parent constrctor
 why call first parent constructor ?
 beacuse parent variable initilize first if not initilize then child not access the parent variables

 if in parent child relation the same variables in both then child access parent varibles using the super.parentvariable
super is non static
 */

class Parent {

	int x = 10;
	static int y = 20;

	Parent(){

		System.out.println();
		System.out.println("Parent");

	}
}
class Child extends Parent {

	int x = 100;
	static int y = 200;

	Child(){
		System.out.println("Child");
		//System.out.println(super);
	 	System.out.println(this);

	}
       	void access(){

		System.out.println(super.x);
		System.out.println(super.y);
		//System.out.println(this.x);
		System.out.println(x);
		System.out.println(y);
	}
}

class Client {

	public static void main(String args[]){

		Child obj = new Child();
		obj.access();
		
	}
}
