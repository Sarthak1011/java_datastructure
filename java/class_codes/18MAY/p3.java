class Employee {

	int empId = 1;
	String empName = "Kanha";

	static int y = 50;

	void empInfo(){

		System.out.println(empId);
		System.out.println(empName);
		System.out.println(y);
	}
}
class MainDemo{

	public static void main(String args[]){

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.empInfo();
		emp2.empInfo();

		emp2.empId = 2;
		emp2.empName = "Rahul";
		emp2.y=5000;

		emp1.empInfo();
		emp2.empInfo();
	}
}




