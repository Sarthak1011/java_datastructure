class Demo {

	int x=10;
	private int y=20;

	void Fun(){

		System.out.println(x);
		System.out.println(y);
	}
}
class MainDemo {

	public static void main(String args[]){

		Demo obj = new Demo();

		obj.Fun();

		System.out.println(obj.x);
		System.out.println(obj.y);//private variable cannot access here private variable scope only that class

		System.out.println(x);//error cannot access
		System.out.println(y);//cannot access
	}
}

