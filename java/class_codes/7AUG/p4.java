//Iterator (Universal Cursor)
//
import java.util.*;
class IteratorDemo {

	public static void main(String args[]){

		ArrayList al = new ArrayList();
		al.add("Sarthak");
		al.add("Nikita");
		al.add("Shweta");
		al.add("Aakash");

		Iterator itr = al.iterator();

		while(itr.hasNext()){
			if("Shweta".equals(itr.next())){
				itr.remove();
			}
			//System.out.println(itr.next());
		}
		System.out.println(al);
	}
}
