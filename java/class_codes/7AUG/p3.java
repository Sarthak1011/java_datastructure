import java.util.*;

class LinkedListDemo {
		
	public static void main(String args[]){
		LinkedList ll = new LinkedList();
		ll.add(20);
		ll.addFirst(10);
		ll.addLast(30);
		System.out.println(ll.getFirst());
		System.out.println(ll.getLast());
		System.out.println(ll.removeFirst());
		System.out.println(ll.removeLast());
		System.out.println(ll);
		System.out.println(ll.node(0));
	}
}

