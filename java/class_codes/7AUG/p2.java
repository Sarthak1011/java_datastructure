import java.util.*;

class CricPlayer {

	int jerNo = 0;
	String name = null;

	CricPlayer(int jerNo , String name ){
		this.jerNo = jerNo;
		this.name = name;
	}
	public String toString(){
		return name + " " + jerNo;
	}
}

class ArrayDemo {


	public static void main(String args[]){

		ArrayList al = new ArrayList();
		al.add(new CricPlayer(68,"Surya"));
		al.add(new CricPlayer(45,"Rohit"));
		al.add(new CricPlayer(93,"Bumrah"));

		System.out.println(al);

		al.add(2, new CricPlayer(33,"Hardik"));
		System.out.println(al);

		al.set(3,new CricPlayer(32,"Ishan"));
		System.out.println(al);
	}
}
