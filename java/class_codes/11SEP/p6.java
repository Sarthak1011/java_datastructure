import java.io.*;

class DeSerialization {

	public static void main(String []args)throws IOException,ClassNotFoundException{

		FileInputStream fis = new FileInputStream("PlayerData.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);

		Player sky =(Player)ois.readObject();
		Player hitman =(Player)ois.readObject();

		System.out.println(sky.jerNo);
		System.out.println(sky.pName);
		System.out.println(hitman.jerNo);
		System.out.println(hitman.pName);

	}
}
