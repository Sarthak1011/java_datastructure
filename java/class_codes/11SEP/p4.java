import java.io.*;

class FileReaderDemo {

	public static void main(String args[])throws IOException{

		FileInputStream fis = new FileInputStream("Incubator.txt");
		FileDescriptor fd = fis.getFD();
		FileReader fr = new FileReader(fd);

		int data = fr.read();
		System.out.println("In :"+fd.in);
		System.out.println("Out :"+fd.out);
		System.out.println("Error :"+fd.err);

		while(data != -1){

			System.out.print((char)data);
			data = fr.read();
		}
		fr.close();
	}
}
