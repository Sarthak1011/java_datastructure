//Serialization
import java.io.*;

class Player implements Serializable{

	int jerNo;
	String pName;

	Player(int jerNo , String pName){

		this.jerNo = jerNo;
		this.pName = pName;
	}
}
class SerializableDemo {

	public static void main(String []args)throws IOException{

		Player sky = new Player(63,"Surya");
		Player hitman = new Player(45,"Rohit");

		FileOutputStream fos = new FileOutputStream("PlayerData.pdf");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(sky);
		oos.writeObject(hitman);
		oos.close();
		fos.close();

	}
}
