class ArrayDemo {

	void fun(int arr[]){

		arr[1]=70;
		arr[2]=80;
	}
	public static void main(String args[]){

		int arr[]={10,20,30,40};

		System.out.println(arr[0]+"--> "+System.identityHashCode(arr[0]));//100
		System.out.println(arr[1]+"--> "+System.identityHashCode(arr[1]));//200
		System.out.println(arr[2]+"--> "+System.identityHashCode(arr[2]));//300
		System.out.println(arr[3]+"--> "+System.identityHashCode(arr[3]));//400

			ArrayDemo obj = new ArrayDemo();

			obj.fun(arr);
			System.out.println("After calling method");
			for(int x:arr){
				System.out.println(x+"--> "+System.identityHashCode(x)); 
			}
			int x=70;
			int y=80;

			System.out.println("variables");
			System.out.println(x+"--> "+System.identityHashCode(x));//500
			System.out.println(y+"--> "+System.identityHashCode(y));//600

	}
}

	
