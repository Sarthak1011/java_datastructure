class Parent {

	void fun(){

		System.out.println("In fun");

	}
}
class Child extends Parent {

	void gum(){

		System.out.println("In gun");

	}
}
class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.gun();//error parent have not this method
	        obj.fun();//parent have that method 
	}
}
