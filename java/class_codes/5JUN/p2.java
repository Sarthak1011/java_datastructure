class Parent {

	Parent(){

		System.out.println("IN parent Constructor");

	}
	void fun(){

		System.out.println("In Parent Fun");

	}
}
class Child extends Parent {

	Child(){

		System.out.println("In child constructor");

	}
	void gun(){

		System.out.println("In child gun");

	}
}
class Client {

	public static void main(String args[]){

		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();//error cannot find symbol

	}
}
