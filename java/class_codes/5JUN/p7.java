class Parent {

	void fun(int x){

		System.out.println("IN parent para fun");

	}
}

class Child extends Parent {

	void fun(double x){

		System.out.println("In child double fun");

	}
	void fun(float x){

		System.out.println("In child float fun");
	}
}
class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.fun(10);
	}
}

