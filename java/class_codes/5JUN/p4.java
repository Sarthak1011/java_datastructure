class Parent {

	int x = 10;
	void p(){
		System.out.println(this.x);
	}


}
class Child1 extends Parent {

	int x = 20;
	void s(){
		System.out.println(x);
	}

}
class Child2 extends Child1 {
	int x = 30;

	void c(){
		System.out.println(super.x);
	}

}
class User {

	public static void main(String args[]){

		Child2 obj = new Child2();
		System.out.println(obj.x);
		obj.c();
		obj.s();
		obj.p();

	}
}


