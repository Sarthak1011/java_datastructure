class Parent {

	Parent(){

		System.out.println("In Parent Constructor");

	}
	void fun(){

		System.out.println("In parent fun");

	}

}
class Child extends Parent {

	Child(){

		System.out.println("In child Constructor");

	}
	void gun(){

		System.out.println("In Child gun");

	}
}
class Client {

	public static void main(String args[]){

		Parent obj = new Child();
		obj.fun();
		obj.gun();
		/*
		 here is the parent refernce and the child object first in compile see parent object and in compile time method in picture in parent method table only constructor and fun thier is no gun that why thier is error for the gun conclusion is that we call that method in this senario the parent have complusory that method we are calling  
		 */ 
	}
}
