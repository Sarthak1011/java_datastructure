/*
 method have no body that method is abstract
 method have body that method is concrite
 in overriding complusory parent child relation
 in overriding parent method is not accepeted to child then that methood child override 

 */ 

class Parent {

	Parent(){

		System.out.println("In parent Constructor ");

	}
	void Property(){

		System.out.println("Home , Gold , Car");

	}
	void Marry(){

		System.out.println("Deepika padokone");

	}
}

class Child extends Parent {

	Child(){

		System.out.println("In Child Constructor");

	}
	void Marry(){

		System.out.println("Alia Batt");

	}
}
class Client {

	public static void main(String args[]){

		Child obj = new Child();//Child obj is refernce of Child object child object see at compile time new Child see at run time
		obj.Property();
		obj.Marry();
	}
}

/*
 in this code  child obj see at the compile time and new child see at run time in method table of parent the constructor,property,Marry three method are there  and in child method table constructor , Marry , Property , Parent marry method is there but child override parent method and go through our method  
 */ 


