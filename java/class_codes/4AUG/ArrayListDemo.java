/*     ArrayList                                     Array
 ArrayList is Dynamic array                 1. array has sequential memory
 ArrayList has not fixed size               2.array has fixed size  
 In ArrayList we can put any type of object 3.In Array we can put same type of object 
 ArrayList has its own methods              4.array has no its own methods
 ArrayList is Class                         5.array is datatype
 ArrayList store duplicate data             6.array also store the same data 
 */ 

import java.util.*;

class ArrayListDemo extends ArrayList {

	public static void main(String args[]){

		ArrayListDemo al = new ArrayListDemo();
		//1.add(Element)
		al.add(10);
		al.add(10.5f);
		al.add(100);
		al.add("Sarthak");
		al.add('S');
		
		ArrayList al1 = new ArrayList();
		//1.add(Element)
		al1.add("Surya");
		al1.add("Bumrah");


		//2.int size()
		System.out.println(al.size());// 5

		//3.boolean contains=(java.lang.Object)
		System.out.println(al.contains("Sarthak")); //true

		//4.int indexOf(java.lang.Object);
		System.out.println(al.indexOf('S'));//4

		//5.E get(int)
		System.out.println(al.get(2));//100

		//6.E set(int,E)
		System.out.println(al.set(4,"Surya"));//S

		//7.void add(int ,E)
		al.add(4,"Sarthak");
		System.out.println(al);//[10,10.5,100,Sarthak,Sarthak,Surya]

		//8. boolean remove(java.lang.Object)
		System.out.println(al.remove("Sarthak"));
		System.out.println(al);//[10,10.5,100,Sarthak,Surya]

		//9.E remove(int)
		System.out.println(al.remove(4));//Surya
		System.out.println(al);//[10,10.5,100,Sarthak]

		//10.boolean addAll(collection)
		System.out.println(al.addAll(al1));
		System.out.println(al);

		//11.boolean addAll(int , collection)
		System.out.println(al.addAll(2,al1));
		System.out.println(al);

		//12.protected void removeRange(int , int)
		al.removeRange(2,5);
		System.out.println(al);

		//13.java.lang.Object[]toArray();
		Object arr[] = al.toArray();
		for(Object data : arr ){
			System.out.println(data);
		}
		
		//14. int lastIndexOf(java.lang.Object)

		System.out.println(al.lastIndexOf(10));

		//15. void clear();
		al.clear();
		System.out.println(al);

	}
}

