//                       Exception Handling 
//1.compile time Exception(Checked Exception)            2.Run time Exception(Unchecked Exception)
//
// Object-->throwable-->Error-->OutOfMemory Error , Virtual Machine Error
//                  |-->Exception-->compile time exception
//                              |-->Run time Exception
//
//                     object 
//                      |
//                   throwable
//                      |
//                   Exception 
//         _____________|_______________
//        |                            |
//compile time Exception        Run time Exception
//(Checked Exception)            (Unchecked Exception)
//-IOException                   -Arithmatic Exception  
//-File Not Found                -null pointer    
//Interrupted                    -NumberFormat Exception
//                               -indexOutOfBound Exception
//                                  -->arrayIndexOutOfBound
//                                  -->StringIndexOutOfBound

//Default Exception Handler--> runtime Exception handle the deafult Exception handler

//1.Arithmatic Exception


class Demo {

	void m1(){

		System.out.println("Start m1");
		System.out.println(10/0);
	}
	void m2(){
		System.out.println("Start m2");
		m1();
	}
	public static void main(String args[]){

		System.out.println("Start main");
		Demo obj = new Demo();
	       	obj.m2();
	   	System.out.println("End Main");
	}
}

