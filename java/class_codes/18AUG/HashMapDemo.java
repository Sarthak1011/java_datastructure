import java.util.*;

class HashMapDemo{

	int num = 0;
	HashMapDemo(int num ){
		this. num = num;
	}
	public String toString(){
		return ""+num;
	}
}
class Demo{
	public static void main(String []args){

		HashMap hm = new HashMap();
		hm.put(10,3);//10 % 16 = 10 
		hm.put(23,1);//23 % 16 = 7
		hm.put(45,4);//45 % 16 = 13
		hm.put(24,2);//24 % 16 = 8

		System.out.println(hm);
		  
		System.out.println("USER DEFINED CLASS");

                HashMap hs = new HashMap();

                hs.put(new HashMapDemo(10),3);
                hs.put(new HashMapDemo(23),1);
                hs.put(new HashMapDemo(45),4);
                hs.put(new HashMapDemo(24),2);
                System.out.println(hs);
	}
}

