import java.util.*;

class HashSetDemo{

	int num =0;
	HashSetDemo(int num){
		this.num = num;
	}
	public String toString (){
		return ""+num;
	}

	public static void main(String []args){

		HashSet hm = new HashSet();
		hm.add(10);//10 % 16 = 10 
		hm.add(23);//23 % 16 = 7
		hm.add(45);//45 % 16 = 13
		hm.add(24);//24 % 16 = 8
		hm.add(44);//44 % 16 = 12
		hm.add(4);//35 % 16 = 3

		System.out.println(hm);

		System.out.println("USER DEFINED CLASS");

		HashSet hs = new HashSet();

		hs.add(new HashSetDemo(10));
		hs.add(new HashSetDemo(23));
		hs.add(new HashSetDemo(45));
		hs.add(new HashSetDemo(24));
		hs.add(new HashSetDemo(44));
		hs.add(new HashSetDemo(4));
		System.out.println(hs);
	}
}

