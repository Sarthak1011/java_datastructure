/*
 INNER CLASS -> 1.NORMAL INNER CLASS
                2.METHOD LOCAL INNER CLASS
		3.STATIC INNER CLASS(STATIC NESTED CLASS)
		4.ANONYMOUS INNER CLASS (NORMAL ,PARAMETERIZED ANONYMOUS INNER CLASS )

INNER CLASS--> INNER CLASS DEPEND ON THE OUTER CLASS IS CALLED INNER CLASS
		1.NORMAL INNER CLASS
 */ 

class Outer {

	class Inner extends Outer {

		void m1(){

			System.out.println("In Inner-m1");
		}
	}
	void m2(){

		System.out.println("In Outer m2");

	}
}
class Client {

	public static void main(String args[]){

		//Outer obj1 = new Outer();
		Outer.Inner obj2 = new Outer().new Inner();
		obj2.m1();
		obj2.m2();
	}
}


