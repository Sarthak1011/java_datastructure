//NORMAL INNER CLASS

class Outer {

	class Inner {
	
		void m1(){

			System.out.println("In Inner m1");

		}
	}
	void m2(){

		System.out.println("In Outer m2");

	}
}
class Client {

	public static void main(String args[]){

		Outer obj1 = new Outer();

		Outer.Inner obj2 = obj1.new Inner();

		obj2.m1();
		obj1.m2();


	}
}
