import java.io.*;

class JaggedArray {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size");
		int s = Integer.parseInt(obj.readLine());
		int arr[][]=new int[s][];

		for(int i=0;i<arr.length;i++){
			System.out.println("Enter the size of "+i+" row");
			int size = Integer.parseInt(obj.readLine());

		           arr[i]= new int[size];
		           //arr[1]= new int[2];
		           //arr[2]= new int[1];
		}

		 System.out.println("Enter the Jagged Array elements");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(obj.readLine());
			}
		}
		System.out.println("JaggedArray elements are");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();

		}

		System.out.println("Using the for each loop");
		for(int i[]:arr){
			for(int j:i){
				System.out.print(j+"\t");
			}
			System.out.println();
		}
	}
}


