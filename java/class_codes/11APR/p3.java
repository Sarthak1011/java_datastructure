class MethodDemo {


	int x=10;
	static int y=20;

	void fun(){
	       System.out.println("In fun");//In fun
		System.out.println(x);//10
		System.out.println(y);//20
	}
         
        static void gun(){
		System.out.println("In gun");//In gun
		System.out.println(x);
		System.out.println(y);//20
	}
     	
	public static void main(String args[]){

		MethodDemo obj = new MethodDemo();

		obj.fun();
		gun();//call
		System.out.println(obj.x);
		System.out.println(y);//20
	}
}

