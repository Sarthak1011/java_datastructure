//in catch block we give mutiple exception using |( or ) symbol in that block not give parent child relation beacuase compiler confuse which excption we seet
//
import java.util.*;

class Demo {

	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();

		try{
			if(x==0){
				throw new ArithmeticException("Divide  by zero");
			}
			System.out.println("message");
		}catch(ArithmeticException Ae){

		//	System.out.print(" Exception` in thread "+Thread.currentThread().getName()+" ");

			Ae.printStackTrace();
		}
	}
}
