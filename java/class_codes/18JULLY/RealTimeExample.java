import java.io.*;

class RainDropException extends RuntimeException {
	RainDropException(String msg){
		System.out.print("Match Delay Due to Rain");
	}
}
class Match {

	 public static void main(String args[]){

		 BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		 System.out.println("In match rain is drop or not \n Enter 'Y' or 'N'");
                char ch='n';
		 try{
		 	 ch = (char)(obj.read());
		 }catch(IOException Ie){
		 }

		 if(ch == 'Y'||ch == 'y'){
			 throw new RainDropException("Rain is comming");
		 }
	 }
 }
