//throw keyword is used for the throw the user defined exception
import java.util.*;

	class DataUnderflowException extends RuntimeException {
		DataUnderflowException(String msg){
			super(msg);
		}
	}
	class DataOverflowException extends RuntimeException{
		DataOverflowException(String msg){
			super(msg);
		}
	}

class ArryDemo{
	public static void main(String args[]){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the array elements between 0 to 100");
		int arr[] = new int[5];
		 for(int i = 0;i<arr.length;i++){
			 int data = sc.nextInt();

			 if(data<0)
				 throw new DataUnderflowException("Data is less than zero");
			
			 if(data >100)
				 throw new DataOverflowException("Data is greater than 100");
		
			 arr[i]=data;
		 }
		 for(int i = 0;i<arr.length;i++){
	
			 System.out.println(arr[i]);
		 }
	}
}

