import java.io.*;
class InputArray {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<size;i++){

			arr[i]=Integer.parseInt(obj.readLine());
		}
		System.out.println("Array elements are");

		for(int i=0;i<size;i++){
			System.out.print("|"+arr[i]+"|");
		}
		System.out.println();
		System.out.println("The length of array is:"+arr.length);


	}
}

