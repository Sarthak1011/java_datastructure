class ArrayDemo {


public static void main(String args[]){

	int arr[] = {10,20,30,40,50};

	char carr[]={'a','b','c'};

	float farr[]={10.4f,12,23.2f};


	boolean barr[]={true , false , true};

	System.out.println("Integer array elements are");
	
	System.out.println(arr[0]);
	System.out.println(arr[1]);
	System.out.println(arr[2]);
	System.out.println(arr[3]);
	System.out.println(arr[4]);
//	System.out.println(arr[5]);//array index out of bound runtime error

	System.out.println("charcter array elements are");
	
	System.out.println(carr[0]);
	System.out.println(carr[1]);
	System.out.println(carr[2]);
	
	System.out.println("float array elements are");

	System.out.println(farr[0]);
	System.out.println(farr[1]);
	System.out.println(farr[2]);
	
	System.out.println("boolean array elements are");
	
	System.out.println(barr[0]);
	System.out.println(barr[1]);
	System.out.println(barr[2]);
}
}




