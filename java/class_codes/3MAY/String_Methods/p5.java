import java.io.*;
import java.util.*;

class CompareToIgnoreDemo {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));


		System.out.println("Enter the string 1");
		String str1 = obj.readLine();
		
		System.out.println("Enter the string 2");
		String str2 = obj.readLine();

		System.out.println(str1.compareToIgnoreCase(str2));
	}
}

