import java.io.*;
import java.util.*;

class CharAtDemo {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string");
		String str1 = obj.readLine();

		System.out.println(str1.charAt(1));
		System.out.println(str1.charAt(0));
		System.out.println(str1.charAt(5));
	}
}
