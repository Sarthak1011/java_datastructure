import java.io.*;
import java.util.*;

class LengthDemo {

	public static void main(String args[])throws IOException{

		BufferedReader obj1 = new BufferedReader(new InputStreamReader(System.in));
		Scanner obj2 = new Scanner(System.in);


		System.out.println("Enter the string ");
		String str1 = obj1.readLine();

		System.out.println("Enter the second String");
		String str2 = obj2.nextLine();

		int s1 = str1.length();
		int s2 = str2.length();

		System.out.println(s1);
		System.out.println(s2);
	}
}
