import java.io.*;
import java.util.*;
class concatDemo {

	public static void main(String args[])throws IOException{
		
		BufferedReader obj1 = new BufferedReader(new InputStreamReader(System.in));
		
		Scanner obj2 = new Scanner(System.in);

		System.out.println("Enter the string1 ");
		String str1 = obj1.readLine();

		System.out.println("Enter the second String");
		String str2 = obj2.nextLine();

		String str3 = str1.concat(str2);

		System.out.println(str3);
	}
}
