class StringDemo {

	public static void main(String args[]){

		String str1 = "Shashi";//SCP----100
		String str2 = "Bagal";//SCP-----200

		String str3 = str1 + str2;//HEAP----300
		String str4 = str1.concat(str2);//HEAP ----400

		System.out.println(str3);
		System.out.println(str4);
	}
}

//'+' internall call the append() method and that method is in string Builder class. append() method return the new String.
//concat method call the concat() method and that method is in String class.
//concat and + are join the two string
