class HashCode{

	public static void main(String args[]){

		String str1 = "nikita";
		String str2 = new String("sarthak");
		String str3 = "nikita";
		String str4 = new String("sarthak");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}

//identity hash code is depend on the memory allocation if string is same and memory allocate at the Scp then their identity hash code is same otherwise different
//hash code is depend on the content if thier content is same then their hash code is same
