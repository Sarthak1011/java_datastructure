//local method Inner class
class Outer {

	static void m1(){

		class Inner {

			static void m1(){

				System.out.println("In Inner m1");

			}
		}
		Inner obj = new Inner();
		obj.m1();
	}
}
class Client {

	public static void main(String args[]){

		Outer obj = new Outer();
		obj.m1();
	}
}
