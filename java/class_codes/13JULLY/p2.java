class Outer {

	object m1(){

		System.out.println("m1 Outer ");

		class Inner {

			void m1(){

				System.out.println("IN m1 Inner");
			}
		}
		return new Inner();
	}
}
class Client {
	public static void main(String args[]){
		Outer obj = new Outer();
		obj.m1().m1();
	}
}
