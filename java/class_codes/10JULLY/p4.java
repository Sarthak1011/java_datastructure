class Outer {

	int x = 10;
	int y = 20;
	
	class Inner {

		void fun1(){//Inner(Inner this)

			System.out.println("Inner Fun1");
			System.out.println(x);
			System.out.println(y);
			
		}
	}
	void fun2(){//fun2(Outer this)

		System.out.println("In Outer fun2");

	}
}
class Client {

	public static void main(String args[]){

		Outer obj = new Outer();//Outer(obj)
		obj.fun2();//fun2(obj)

		Outer.Inner obj1 =obj.new Inner();//Inner(obj1 , obj)
	        obj1.fun1();//fun(obj1)

	}
}	
