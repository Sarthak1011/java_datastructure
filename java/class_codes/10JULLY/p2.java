//Inner class has 2 parameters one is hidden this refernece and second one is outer class this$0 
//this$0 is final 
//we create two objects thats why here two this are present

class Outer {

	class Inner {

		Inner(){
		 
			System.out.println("Inner Constructor");
			System.out.println(this);
		}

		void fun(){
		
			System.out.println("In Inner fun");

		}
	}
	void gun(){

		System.out.println("In Outer gun");

	}
}
class Client {

	public static void main(String[] args){

		Outer obj1 = new Outer();
		obj1.gun();

		Outer.Inner obj2 = obj1. new Inner();
		obj2.fun();
	}
}



