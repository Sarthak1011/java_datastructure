class Outer {

	class Inner {

		void fun1(){

			System.out.println("In Inner fun1");

		}
	}
	
	void fun2(){

		System.out.println("In Outer fun2");

	}
}

class Client {

	public static void main(String args[]){

		Outer obj = new Outer();
		obj.fun2();

		Outer.Inner obj2 = obj.new Inner();
		obj2.fun1();

		Outer.Inner obj1 = new Outer().new Inner();
		obj1.fun1();
	}
}
