import java.util.*;

class PriorityQueueDemo {

	public static void main(String args[]){

		PriorityQueue pqueue = new PriorityQueue();
/*		pqueue.offer(10);
		pqueue.offer(20);
		pqueue.offer(60);
		pqueue.offer(30);
		pqueue.offer(40);
*/
		System.out.println(pqueue);

		pqueue.offer("Ashish");
		pqueue.offer("Zadhe");
		pqueue.offer("Kanha");
		pqueue.offer("Rahul");
		pqueue.offer("Shashi");
		
		System.out.println(pqueue);

	}
}
