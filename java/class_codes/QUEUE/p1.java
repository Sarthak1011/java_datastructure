
              /*
	                 	           		 ------------- Queue(I)--------------------------- 
                                		    	|	            |                            |
						PriorityQueue(C)          Deque(I)             BlockingQueue(I)
			                                                     |-LinkedList              |
									     |-ArrayDeque              |
									                               |
							                _________________________________________________________________________
									|					|                               |
							PriorityBlockingQueue(C)			ArrayBlockingQueue(C)         LinkedBlockingQueue(C)


	       */ 


import java.util.*;

class QueueDemo {

	public static void main(String args[]){

		Queue que = new LinkedList();
		que.offer(10);
		que.offer(20);
		que.offer(50);
		que.offer(30);

		System.out.println(que);
		que.poll();
		System.out.println(que);
		que.remove();
		System.out.println(que);

		System.out.println(que.peek());
		System.out.println(que.element());

		que.poll();
		System.out.println(que);
		que.remove();
		System.out.println(que);
		
		System.out.println(que.peek());
		//System.out.println(que.element());//NoSuchEmentException
		//System.out.println(que);
		que.poll();
		System.out.println(que);
		//que.remove();
		System.out.println(que);
	}
}
