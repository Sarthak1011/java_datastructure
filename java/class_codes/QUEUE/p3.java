import java.util.*;

class Project {
	int teamMem = 0;
	String pName = null;
	int days = 0;

	Project(int teamMem,String pName , int days){
		this.teamMem = teamMem;
		this.pName= pName;
		this.days = days;
	}
	public String toString(){
		return pName + " "+ teamMem + "" + days;
	}
}

class PriorityDemo {

	public static void main(String [] args){

		PriorityQueue pqueue = new PriorityQueue();

		pqueue.offer(new Project(3,"Collage Admission App"));
		pqueue.offer(new Project(5,"Animal Diseas Detetction"));
		pqueue.offer(new Project(4,("CHatAPP"));
		pqueue.offer(new Project(3,"Browserp"));
