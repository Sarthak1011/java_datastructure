import java.util.*;

class ArrayDemo {

	public static void main(String args[]){

		Scanner sc = new Scanner(System.in);

	System.out.println("Enter the array size");
	int size = sc.nextInt();

	int arr[]= new int[size];
	System.out.println("Enter the array elements");

	int count =0;
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
		if(arr[i]%2==0){
			count++;
		}
	}
	System.out.println("Count of evene elements in array is"+count);
}
}
