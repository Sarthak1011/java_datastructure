class Demo {


	int x = 10;
	static int y = 20;


	Demo(){
		System.out.println("In Constructor");
	}

	static {
		System.out.println("In static Block 1");
	}

	{
		System.out.println("In Instance Block 1");
	}

	public static void main(String args[]){

		Demo obj = new Demo();

	}
	static {

		System.out.println("In static Block 2");
	}
	
	{
		System.out.println("In Instance Block 2");
	}
}
