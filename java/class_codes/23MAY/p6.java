class Demo {

	static int x = 10;//static global variable
	int y = 20;//global instance variable


	//constructor
	Demo (){

		int a = 100;
		//static int b = 200;//error  static varible cannot initilize int any block or method
		System.out.println(x+ " constructor "+y);
	}
	//static block

	static {

		int p = 500;
		//static int q = 600;//error
		System.out.println(x+" static block "+p);
	}

	//instance block
	{

		int m = 1;
		//static int n = 2;//error
		System.out.println(x+" instance block "+m);
	}
	//instance method /non static mathod
	void fun (){

		int w=20;
		//static int v = 30;//error
		System.out.println(x+" instance method "+w);
	}
	//static method
	static void gun(){

		int e=150;
		//static int f = 160;//error
		System.out.println(x+" static method "+e);
	}
	//main method
	public static void main(String args[]){

		int h = 5;
		//static int i = 6;//error

		System.out.println(x+" main method "+h);

		Demo obj = new Demo();
		obj.fun();
		gun();
		//Demo.gun();
	}
}
//output
//10 static block 500
//10 main method 5
//10 instance block 1
//10 constructor 100
//10 instance method 20
//10 static method 150


