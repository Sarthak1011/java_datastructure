class Demo {

	static {

		System.out.println("In static block 1");
	}

	public static void main(String args[]){

		System.out.println("In demo main");
		Client obj2 = new Client();
	}
}
class Client {

	static {

		System.out.println("In static block 2");

	}

	public static void main(String args[]){

		System.out.println("In client Main");

		Demo obj = new Demo();
	
	}

	static {
		System.out.println("In static block 3");
	}
}

