class MyThread extends Thread {

	MyThread (ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){

		System.out.println(Thread.currentThread());

	}
}
class ThreadGroupDemo {

	public static void main(String sarthak[]){

		ThreadGroup TGP = new ThreadGroup("ICC");

		MyThread T1 = new MyThread(TGP,"IND");
		MyThread T2 = new MyThread(TGP,"AUS");
		MyThread T3 = new MyThread(TGP,"ENG");

		T1.start();
		T2.start();
		T3.start();


		ThreadGroup TGC = new ThreadGroup(TGP,"BCCI");
		
		MyThread T4 = new MyThread(TGC,"MI");
		MyThread T5 = new MyThread(TGC,"CSK");
		MyThread T6 = new MyThread(TGC,"RCB");
		
		T4.start();
		T5.start();
		T6.start();
	}
}


