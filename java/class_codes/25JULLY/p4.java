/*
 Compiled from "ThreadGroup.java"
public class java.lang.ThreadGroup implements java.lang.Thread$UncaughtExceptionHandler {
  static final boolean $assertionsDisabled;
  java.lang.ThreadGroup(java.lang.ThreadGroup, java.lang.String, int, boolean);
  public java.lang.ThreadGroup(java.lang.String);
  public java.lang.ThreadGroup(java.lang.ThreadGroup, java.lang.String);
  public final java.lang.String getName();
  public final java.lang.ThreadGroup getParent();
  public final int getMaxPriority();
  public final boolean isDaemon();
  public boolean isDestroyed();
  public final void setDaemon(boolean);
  public final void setMaxPriority(int);
  public final boolean parentOf(java.lang.ThreadGroup);
  public final void checkAccess();
  public int activeCount();
  public int enumerate(java.lang.Thread[]);
  public int enumerate(java.lang.Thread[], boolean);
  public int activeGroupCount();
  public int enumerate(java.lang.ThreadGroup[]);
  public int enumerate(java.lang.ThreadGroup[], boolean);
  public final void stop();
  public final void interrupt();
  public final void suspend();
  public final void resume();
  public final void destroy();
  public void list();
  public void uncaughtException(java.lang.Thread, java.lang.Throwable);
  public boolean allowThreadSuspension(boolean);
  public java.lang.String toString();
  static {};
}
 *
 */

class MyThread extends Thread {

	public void run(){

		System.out.println(getParent());
	}
}
class ThreadDemo {

	public static void main(String []sarthak){
	
		ThreadGroup TG = new ThreadGroup();
		Tg.start();
	}
}

