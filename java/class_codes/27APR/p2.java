class TwoDArray {

	public static void main(String args[]){

		int arr1[][] = new int [2][3];
		int arr2[][] = new int [2][];
		//int arr3[][] = new int [][3];  //error compusulory row size initilize
		//int arr4[][]= {10,20,30,40,50,60};//error 
		int arr5[][] = {{10,20,30},{40,50,60},{}};
		int arr6[][] = new int [][]{{10},{20,30},{40,50,60}};


		System.out.println("1 array");
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr1[i].length;j++){
				System.out.print(arr1[i][j]+"\t");
			}
			System.out.println();
		}
	/*	System.out.println("2 array");
		for(int i=0;i<arr2.length;i++){
			for(int j=0;j<arr2[i].length;j++){
				System.out.print(arr2[i][j]+"\t");//error compiler become compuse int taking col size exception column length is null 
			}
			System.out.println();
		}
		*/
		System.out.println("3 array");
		for(int i=0;i<arr5.length;i++){
			for(int j=0;j<arr5[i].length;j++){
				System.out.print(arr5[i][j]+"\t");
			}
			System.out.println();
		}
		System.out.println("4 array");
		for(int i=0;i<arr6.length;i++){
			for(int j=0;j<arr6[i].length;j++){
				System.out.print(arr6[i][j]+"\t");
			}
			System.out.println();
		}
	}
}
