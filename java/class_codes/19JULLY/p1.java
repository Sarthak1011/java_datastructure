/*
 Multithreading is use for the improve the performance of code
 we do thread Synchronized
 if 1 thread then 1 stack 2 thread then 2 stack
 two ways to implement multithreading 1.using Thread class 2. using the Runnable interface
 Object---------------------extends--------->|
  |__Runnable interface----implements--Thread class
   OS------>CPU Schedular
   Thread-->Thread Schedular
   sleep();throws  InterruptedException
 */ 
/*
 THREAD LIFE CYCLE 
 1.[new/ born]---start()-->2.[Ready to Run]----thread Schedular--->3.[Running]--->4.[Dead/Zombie]
                                  |                               |      
 				   <---sleep()<-----------------<--     
 */ 

// CREATE THREAD USING THE THREAD CLASS

class MyThread extends Thread {

	public void run(){//override parent class run method (i.e Thread class run method (i.e public void run();))
//Here not allowed the throws InterrupedIoexception beacase in parent run method not thrown the InterruptedException
		for(int i = 0; i < 10; i++){
			System.out.println("In Run");
			try{
				Thread.sleep(1000);
			}catch(InterruptedException obj){
	
			}
		}
	}
}
class ThreadDemo {

	public static void main(String args[]){

		MyThread obj = new MyThread();//create thread 
		obj.start();//start thread 

		for(int i = 0; i < 10; i++){
			System.out.println("In Main");
			try{
				Thread.sleep(1000);
			}catch(InterruptedException obj1){
				
			}
		}
	}
}
