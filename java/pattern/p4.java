/*
 1  2  3  4
 5  6  7  8
 9  10 11 12
 
 */ 


import java.io.*;

class c2w {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		int col = Integer.parseInt(obj.readLine());
		int num=1;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				System.out.print(num++ +"\t");
			}
			System.out.println();
		}
	}
}

