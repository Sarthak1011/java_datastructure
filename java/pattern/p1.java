//print odd number between the range


import java.io.*;

class c2w {

	public static void main(String args[])throws IOException{


		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the starting number and ending number");
		
		int start = Integer.parseInt(obj.readLine());

		int end = Integer.parseInt(obj.readLine());


		for(int i=start;i<=end;i++){
			if(i%2==1){
				System.out.print(i+"\t");
			}
		}
		System.out.println();
	}
}



