/*
 Q1
Write a program in which students should enter marks of 5 different subjects. If all subject
having above passing marks add them and provide to switch case to print grades(first class
second class), if student get fail in any subject program should print “You failed in exam”

 */


import java.io.*;
class c2w {

	public static void main(String args[]) throws IOException {

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.println("enter the five subject marks");

		int m1=Integer.parseInt(obj.readLine());
		int m2=Integer.parseInt(obj.readLine());
		int m3=Integer.parseInt(obj.readLine());
		int m4=Integer.parseInt(obj.readLine());
		int m5=Integer.parseInt(obj.readLine());

		int sum=0;
		if(m1>=35 && m2>=35 && m3>=35 && m4>=35 && m5>=35){

			sum = m1 + m2 + m3 + m4 + m5;

			int flag=-1;
			System.out.println("Total marks :"+sum);
			
			if(sum > 400 && sum <= 500){
				flag = 1;
			}else if(sum > 300 && sum <=400){
				flag=2;
			}else if(sum>=175 && sum <=300){
			        flag=3;
			}

			switch(flag){

				case 1:
					System.out.println("First Class");
					break;
				case 2:
				       System.out.println("Second Class "); 
				       break;
				case 3:
				       System.out.println("Third Class");
				       break;
				default:
				       System.out.println("Invalid marks please enter valid marks");
				       break;
			}
			              	       
		}else{
			System.out.println("You are fail in exam");
		}
	}
}

			
