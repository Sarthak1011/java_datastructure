/*
 Program 9
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array

 */

import java.io.*;

class ArrayDemo {

	void MergeArray(int arr1[],int arr2[]){
		int size1 = arr1.length;
		int size2 = arr2.length;
		int arr3[]=new int[size1+size2];

		for(int i=0;i<arr1.length;i++){
			arr3[i] = arr1[i];
		}

		for(int i=0;i<arr2.length;i++){
			arr3[size1+i]=arr2[i];
		}

		for(int i=0;i<arr3.length;i++){
			System.out.println(arr3[i]);
		}
	}


	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array1 size");
		int size1 = Integer.parseInt(obj.readLine());

		int arr1[]= new int[size1];

		System.out.println("Enter the array 1 Elements");

		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(obj.readLine());
		}
		
		System.out.println("Enter the array2 size");
		int size2 = Integer.parseInt(obj.readLine());
		System.out.println("Enter the array2 elements");

		int arr2[]=new int[size2];
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		obj2.MergeArray(arr1,arr2);
	}
}
