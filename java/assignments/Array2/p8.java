/*
Program 8
WAP to find the uncommon elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Uncommon elements :
3
5
9
8
 
 */

import java.io.*;
class ArrayDemo{

	void UnCommonEle(int arr1[],int arr2[]){
		System.out.println("Uncommon Elements are");
		int flag=0;
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					flag=1;
					break;
				}else{
					flag=0;
				}
			}
			if(flag==0){
				System.out.println(arr1[i]);
			}
		}
		for(int i=0;i<arr2.length;i++){
			for(int j=0;j<arr1.length;j++){
				if(arr2[i]==arr1[j]){
					flag=1;
					break;
				}else{
					flag=0;
				}
			}
			if(flag==0){
				System.out.println(arr2[i]);
			}
		}


          }

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                ArrayDemo obj1 = new ArrayDemo();
                System.out.println("Enter the array1 size");
                int size1 = Integer.parseInt(obj.readLine());

                int arr1[]= new int[size1];

                System.out.println("Enter the array1 elements");
                for(int i=0;i<arr1.length;i++){

                        arr1[i]=Integer.parseInt(obj.readLine());
                }
           
                System.out.println("Enter the array2 size");
                int size2 = Integer.parseInt(obj.readLine());
	   	int arr2[]= new int[size2];
		System.out.println("Enter the array2 elements");
                for(int i=0;i<arr2.length;i++){

                        arr2[i]=Integer.parseInt(obj.readLine());
                }

		ArrayDemo obj2 = new ArrayDemo();

		obj2.UnCommonEle(arr1,arr2);

	}
	}

