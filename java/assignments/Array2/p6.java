/*
 Program 6
WAP to take size of array from user and also take integer elements from user
find the maximum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: max element = 5
 */

import java.io.*;

class ArrayDemo{

	int MaxEle(int arr[]){

		int max=arr[0];
		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max = arr[i];
			}
		}
		return max;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj1 = new ArrayDemo();

		System.out.println("maximum array element is:"+obj1.MaxEle(arr));

	}
}
