/*
 Program 3
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26
 */ 

import java.io.*;


class ArrayDemo{
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];
		
		System.out.println("Enter the array elements");
		int sum1=0,sum2=0;
		for (int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
			if(arr[i]%2==0){
				sum1=sum1+arr[i];
			}else{
				sum2=sum2+arr[i];
			}
		}
		System.out.println("Odd number sum: "+sum1);
		System.out.println("Even number sum: "+sum2);
	}
}


