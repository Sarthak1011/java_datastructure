/*
 Program 10
WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 123
 */ 

import java.io.*;
class ArrayDemo {

	void AddDigit(int arr[]){

		//int sum=0;
		System.out.println("Sum of digits that are even");
		for(int i=0;i<arr.length;i++){
			int sum=0;
			int num=arr[i];
			while(num!=0){
			int rem = num % 10;

			sum = sum + rem;
			num= num/10;
			}
			if(sum % 2 == 0){
				System.out.println(arr[i]);
			}
		}
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the array size");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		obj2.AddDigit(arr);
	}
}
