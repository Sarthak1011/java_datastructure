/*
 Program 7
WAP to find the common elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Common elements :
1
2
 */


import java.io.*;

class ArrayDemo {

	void CommonEle(int arr1[],int arr2[]){
		System.out.println("Common elemsnts are");

		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					System.out.println(arr1[i]);
				}
			}
		}
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));


		System.out.println("Enter the size of array");
		int size = Integer.parseInt(obj.readLine());

		int arr1[]= new int[size];

		System.out.println("Enter the array1 elements");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(obj.readLine());
		}
		int arr2[]= new int[size];
		
		System.out.println("Enter the array2 elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		obj2.CommonEle(arr1,arr2);
	
	}
}


