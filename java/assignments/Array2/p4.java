/*
 Program 4
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2
 */ 

import java.io.*;

class ArrayDemo {


	int SearchEle(int arr[],int search){

		for(int i=0;i<arr.length;i++){
			if(search==arr[i]){
				return i;
			}
		}
	   return -1;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		ArrayDemo obj1 = new ArrayDemo(); 
		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];

		System.out.println("Enter the array elements");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(obj.readLine());
		}

		System.out.println("Enter the searching element");
		int search=Integer.parseInt(obj.readLine());

		int ret = obj1.SearchEle(arr,search);
	
		if(ret!=-1){
			System.out.println("Element found at index: "+ret);
		}else{
			System.out.println("Element not found");
		}
	}
}




