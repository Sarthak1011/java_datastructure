/*
 Program 2
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3
 */ 

import java.util.*;

class ArrayDemo {

	public static void main(String args[]){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the array size");
		int size = sc.nextInt();

		int arr[]= new int[size];

		System.out.println("Enter the array elements");

		int count=0;

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				count++;
			}
		}
		System.out.println("Number of Even Elements: "+count);
		System.out.println("Number of Odd Elements: "+ (size-count));
	}
}




