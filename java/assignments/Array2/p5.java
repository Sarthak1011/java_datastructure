/*
 Program 5
WAP to take size of array from user and also take integer elements from user
find the minimum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: min element = 0

 */

import java.io.*;

class ArrayDemo{

	int MinEle(int arr[]){

		int min=arr[0];
		for(int i=0;i<arr.length;i++){
			if(min>arr[i]){
				min = arr[i];
			}
		}
		return min;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj1 = new ArrayDemo();

		System.out.println("minimum array element is:"+obj1.MinEle(arr));

	}
}
