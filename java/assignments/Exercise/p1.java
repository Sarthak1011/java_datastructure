/*
 1.Write a Program to Print following Pattern.
note: take rows from user.
A C E G
B D F
C E
D
 */ 


import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the rows :");
		int row = Integer.parseInt(obj.readLine());

		for(int i=1;i<=row;i++){
			char ch=(char)(64+i);

			for(int j=row;j>=i;j--){
				System.out.print(ch+"\t");
				ch=(char)(ch+2);
			}
			System.out.println();
		}
	}
}




