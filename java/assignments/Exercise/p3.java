/*
 3.Write a Program that prints a series of Prime numbers ranging
between 1 to 100.
Output: 1 2 3 5 . . .
 */ 

import java.io.*;

class PrimeNum {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the range:");
		int start= Integer.parseInt(obj.readLine());
		int end= Integer.parseInt(obj.readLine());


		//int count=0;
		System.out.println("Prime numbers are:");
		for(int i=start;i<=end;i++){
		int count=0;
			for(int j=2;j<=i/2;j++){
	
				if(i % j == 0){
					count++;
					break;
				}
			}
			if(count == 0){
				System.out.println(i);
			}
		}
	}
}


