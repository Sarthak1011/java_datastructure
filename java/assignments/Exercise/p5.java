/*
 5.Write a program to remove the occurrences of a specified letter Character.
Input :
Java Program
character to remove : a
Output :
Jv Progrm
 */ 

import java.io.*;

class OccurenceChar{

	static String OccChar(String str,char ch){

		char arr1[]= str.toCharArray();
		char arr2[]= new char[arr1.length];

		for(int i=0;i<arr1.length;i++){
			if(arr1[i] == ch){
				continue;

			}
			arr2[i]=arr1[i];
		}
		String str1 = new String(arr2);
		return str1;
	}

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String:");
		String str = obj.readLine();

		System.out.println("Enter the Character:");
		char ch = (char)obj.read();

		String str1 = OccChar(str,ch);

		System.out.println(str1);
	}
}


