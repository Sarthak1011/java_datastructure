/*
 2.Write a Program to Print following Pattern.
note: take rows from user.
E a D b
  c C d
    B e
      f
 */

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows:");
		int row = Integer.parseInt(obj.readLine());
		char ch1=(char)(65+row);
		char ch2=(char)(97);

		for(int i=1;i<=row;i++){

			for(int j=1;j<i;j++){
				System.out.print("\t");
			}

			for(int k=i;k<=row;k++){
				if(k%2==1){
					System.out.print(ch1-- +"\t");
				}else{
					System.out.print(ch2++ +"\t");
				}
			}
			System.out.println();

		}
	}
}
					

