/*
 6.Write a Program to reverse a given String.
Input String:
Ajay Bhosle
Output String:
elsohB yajA
 */

import java.io.*;

class StringRev {

	static String ReverseStr(String str1){

		char arr[]=str1.toCharArray();

		String rev ="";
		for(int i=arr.length-1;i>=0;i--){
			rev= rev + str1.charAt(i);
		}
		return rev;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String :");
		String str = obj.readLine();

		String str1 = ReverseStr(str);
		System.out.println(str1);
	}
}

