/*
 4.Write a program to find maximum and minimum numbers from an array
note: take array from user.
Input:
N = 6
Arr[] = {3,,34, 1, 156,200, 100}
Output:
min = 1, max = 200
 */

import java.io.*;

class MaxMinEle{

	static int MaxEle(int arr[]){
		int max=arr[0];

		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max=arr[i];
			}
		}
		return max;
	}

	static int MinEle(int arr[]){
		int min=arr[0];

		for(int i=0;i<arr.length;i++){
			if(min>arr[i]){
				min=arr[i];
			}
		}
		return min;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]=new int[size];
		System.out.println("Enter the array elements:");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		int max = MaxEle(arr);
		int min = MinEle(arr);

		System.out.println("Max = "+max);
		System.out.println("Min = "+min);
	}
}


