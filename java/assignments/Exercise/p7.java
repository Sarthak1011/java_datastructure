/*
 7.Write a Program that takes a number as input from the user
and prints only those digits from that number, which are perfect
divisors of the actual number.
Input: 124
Output: The Perfect Divisor Digits from the Number 124 are 1 2 4
 */ 


import java.io.*;

class PerfectDiv {

	static void PerfectDivDigit(int num){

		int temp =num;
		while(num!=0){
			int rem = num % 10;
			if(temp % rem==0){
				System.out.println(rem+"\t");
			}
			num=num/10;
		}
	}


	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number:");
		int num = Integer.parseInt(obj.readLine());

		PerfectDivDigit(num);

	}
}
