/*
 1. Program 1:
Write a program to create an array of 5 integer elements.
And print all 5 elements from an array (take hardcoded values in the array)
Output :
1
2
3
4
5
 */

class Array {

	public static void main(String args[]){

		int arr1[]={10,20,30,40,50};
		int arr2[]=new int[]{10,20,30,40,50};

		System.out.println("The array elements are");
		for(int i=0;i<arr1.length;i++){

			System.out.println(arr1[i]);
		}

		System.out.println("The array elements are");
		for(int i: arr2){
			System.out.println(i);
		}
	}
}
