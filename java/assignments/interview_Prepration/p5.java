/*
 5. Program 5:
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and find the max number from the array
Input:
n=5
Enter elements in the array:
2
3
6
9
5
Output:
9
 */

import java.io.*;
class Array {

	int MaxEle(int arr[]){

		int max = arr[0];
		for(int i =0;i<arr.length;i++){

			if(arr[i]>max){
				max=arr[i];

			}
		}
		return max;
	}
        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the array size");
                int size =Integer.parseInt(obj.readLine());

                int arr[]= new int[size];

                System.out.println("Enter the array elements");

                for(int i = 0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(obj.readLine());

                }

		Array obj1 = new Array();
		int max = obj1.MaxEle(arr);

		System.out.println("Maximum elemet from the array is :"+max);
	}
}

