/*
 6. Program 6:
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and find the min number from the array
Input:
n=6
Enter elements in the array:
2
3
6
9
5
1
Output:
1
 */
import java.io.*;
class Array{

	int MinEle(int arr[]){

		int min = arr[0];

		for(int i=0;i<arr.length;i++){

			if(min>arr[i]){
				min=arr[i];
			}
		}
		return min;
	}

public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the array size");
                int size =Integer.parseInt(obj.readLine());

                int arr[]= new int[size];

                System.out.println("Enter the array elements");

                for(int i = 0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(obj.readLine());

                }

                Array obj1 = new Array();
                int min = obj1.MinEle(arr);

                System.out.println("Minimum elemet from the array is :"+min);
        }
}

