/*
 2. Program 2:
Write a program to create an array of 5 integer elements.
Insert from the user and print 5 elements from an array
Input:
1
2
3
4
5
Output :
1
2
3
4
5

 */ 

import java.io.*;
class Array {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		int arr1[]=new int[5];

		System.out.println("Enter Array element");
		for(int i = 0;i<arr1.length;i++){

			arr1[i]=Integer.parseInt(obj.readLine());
	
		}
		System.out.println("Array elements are");
		for(int i = 0;i<arr1.length;i++){

			System.out.println(arr1[i]);
	
		}
	}
}


