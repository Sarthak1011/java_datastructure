/*
 7. Program 7:
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and find the sum of all elements in the array.
Input:
n=6
Enter elements in the array:
2
3
6
9
5
1
Output:
26
	
 */	
import java.io.*;
class Array{

	int SumEle(int arr[]){

		int sum = 0;

		for(int i = 0;i<arr.length;i++){
			sum = sum+arr[i];
		}
		return sum;
	}
	
public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the array size");
                int size =Integer.parseInt(obj.readLine());

                int arr[]= new int[size];

                System.out.println("Enter the array elements");

                for(int i = 0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(obj.readLine());

                }

                Array obj1 = new Array();
                int sum = obj1.SumEle(arr);

                System.out.println("Sum of elements in the array is :"+sum);
        }
}

