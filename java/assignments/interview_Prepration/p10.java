/*
 
10. Program 10:
Write a program to sort the array in ascending order
Input:
Enter the length of the array
n=3
Enter elements in the array
6
8
3
Output:
3
6
8
 */
import java.io.*;
class SortArray {

	int[] bubbleSort(int arr[]){

		for(int i = 0;i<arr.length;i++){

			for(int j=0;j<arr.length-i-1;j++){

				if(arr[j]>arr[j+1]){
					int temp = arr[j+1];
					arr[j+1]=arr[j];
					arr[j]=temp;
					}
				}
			}
			
		return arr;
	}


	public static void main(String args[])throws IOException{


		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size = Integer.parseInt(obj.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(obj.readLine());

		}

		SortArray obj1 = new SortArray();

		int sortArray[]=obj1.bubbleSort(arr);

		System.out.println("Sorting array is:");

		for(int i=0;i<sortArray.length;i++){

			System.out.println(sortArray[i]);

		}
	}
}


