/*
 Q5
write a program to print the following pattern
26 Z 25 Y
24 X 23 W
22 V 21 U
20 T 19 S 
 */ 

class c2w {
	public static void main(String args[]){

		int row=4;

		int num=26;
		char ch='z';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j%2==0){
					System.out.print(ch-- +" ");
				}else{
					System.out.print(num-- +" ");
			}
			}
			System.out.println();
		
	}
}
}
