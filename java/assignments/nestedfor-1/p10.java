/*
 Q10
write a program to print the following pattern
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
 */ 


class c2w {
	public static void main(String args[]){

		int row=6;
		
		for(int i=1;i<=row;i++){
			int num=row;
			char ch='F';
			for(int j=1;j<=row;j++){
				if(j%2 == 1){
					System.out.print(ch+" ");
				}else{
					System.out.print(num +" ");
				}
				num--;
				ch--;
			}
			System.out.println();
		
		}

	}
}
