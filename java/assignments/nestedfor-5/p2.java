/*
 Q2
Write a program to take range as input from the user and print composite numbers.
Input: Enter start: 1
Enter end: 20
Output: composite numbers between 1 and 20
4 6 8 9 10 12 14 15 16 18 20
 */ 
import java.io.*;
class CompositeNum {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the range ");

		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		System.out.println("Composite numbers Between the range "+start +"-"+end+" are:");
		for(int i=start;i<=end;i++){
			for(int j=2;j<=i/2;j++){
				if(i % j ==0){
					System.out.println(i);
					break;
				}
			}
		}
	}
}

