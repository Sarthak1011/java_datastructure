/*
 Q10
Write a program to take range as input from the user and print Armstrong numbers. ( Take a start and
end number from a user )
Input: Enter start: 1
Enter end: 1650
Output: Armstrong numbers between 1 and 1650
1 2 3 4 5 6 7 8 9 153 370 371 407 1634
 */ 

import java.io.*;

class ArmstrongNum {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter the range ");
		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		System.out.println("Armstrong number between "+start+" and "+end);

		for(int i=start;i<=end;i++){
			int num=i;

			int count=0;
			while(num !=0){
				count++;
				num=num/10;
			}
			int temp =i;
			int sum =0;
	

			while(temp != 0){
				int rem = temp%10;
				int fact =1;
				for(int j=1;j<=count;j++){
					fact = fact * rem;
				}
				sum =sum +fact;
				temp =temp / 10;
			}
			if(sum == i){
				System.out.println(i);
			}
		}
	}
}

			
