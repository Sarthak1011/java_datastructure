/*
 Q9
write a program to print a series of strong numbers from entered range. ( Take a start and end number
from a user )
Input:-
Enter starting number: 1
Enter ending number: 150
Output:-
Output: strong numbers between 1 and 150
1 2 145

 */

import java.io.*;

class StrongNum {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the range ");
		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		System.out.println("Stong numbers Between the "+start+" and "+end);

		for(int i=start;i<=end;i++){

			int num =i;
			int sum =0;
			while(num !=0){
				int rem =num % 10;

			        int fact = 1;
		        	 for(int j=1;j<=rem;j++){
				 fact = fact * j;
			       }
			num =num/10;
			sum = sum +fact;
			}
			if(sum == i ){
				System.out.println(i);
			}
		}
	}
}



