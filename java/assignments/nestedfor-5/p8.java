/*
 Q8
Write a program to take range as input from the user and print Palindrome numbers. ( Take a start and
end number from a user )
Input: Enter start: 100
Enter end: 250
Output: Palindrome numbers between 100 and 250
101 111 121 131 141 151 161 171 181 191 202 212 222
 */ 

import java.io.*;

class PalindromeNum {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the range ");
		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		System.out.println("Palindrome numbers between "+start+" and "+end);

		for(int i=start;i<=end;i++){

			int num=i;

			int rev = 0;
			while(num != 0){

				rev = rev*10 + num%10;
				num = num/10;
			}
			if(i == rev){
				System.out.println(i);
			}
		}
	}
}

				

