/*
 Q7
Write a program to take range as input from the user and print the reverse of all numbers. ( Take a
start and end number from a user )
Input: Enter start: 100
Enter end: 200
Output: reverse numbers between 100 and 200
 */ 

import java.io.*;

class ReverseNum {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the range ");
		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		System.out.println("Reverse number between the Range of "+start+" - "+end);
		for(int i=start;i<=end;i++){
			int rev=0;
			int num =i;
			while(num!=0){
				 rev= rev*10 + num % 10;
				 num=num/10;
			}
			System.out.println(i+" = "+rev);
		}
	}
}


	

