/*
 Q1
Write a program to print the numbers divisible by 5 from 1 to 50 & the number is even also print the
count of even numbers.
Input: Enter a lower limit: 1
Enter upper limit: 50
Output: 10, 20, 30, 40, 50
Count = 5
 */ 

import java.io.*;
class NestedFor{

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the range");
		int start = Integer.parseInt(obj.readLine());
		int end = Integer.parseInt(obj.readLine());

		int count =0;
		System.out.println("the number is divisible by 5 and even ");
		for(int i=start;i<=end;i++){
			if(i % 5 ==0 && i % 2 == 0){
				System.out.println(i);
				count++;
			}
		}
		System.out.println("count = "+count);
	}
}
