// count the odd digits

class c2w {

	public static void main(String args[]){

		int count=0;
		long num=986345567;

		while(num!=0){

			long x=num%10;
			num=num/10;

			if(x%2==0){
				count++;
			}
		}
		System.out.println(count);
	}
}
