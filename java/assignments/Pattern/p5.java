/*
 Take no of rows from the user
ROWS = 4
4 4 4 4
3 3 3 3
2 2 2 2
1 1 1 1
 */ 

import java.io.*;
class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

		int num = row;
                for(int i = 1;i <= row;i++){
                        for(int j = 1 ;j<=row;j++){
                                System.out.print(num+"\t");

                        }
			num--;
                        System.out.println();
                }
        }
} 
   
