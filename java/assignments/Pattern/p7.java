/*
 Take no of rows from the user
ROWS = 3
A A A
B B B
C C C
 */

import java.io.*;
class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

                char ch = 'A';
                for(int i = 1;i <= row;i++){
                        for(int j = 1 ;j<=row;j++){
                                System.out.print(ch+"\t");

                        }
			ch++;
                        System.out.println();
                }
        }
}
