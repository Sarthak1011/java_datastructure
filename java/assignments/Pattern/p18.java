/*
 Take no of rows & col from the user ROWS = 6 Col = 4
1 2 3 4
a b c d
# # # #
5 6 7 8
e f g h
# # # #
 */ 

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		System.out.println("Enter the colums");
		int col= Integer.parseInt(obj.readLine());
		int num =1;
		char ch= 97;

		for(int i = 1;i<=row;i++){
			for(int j= 1;j<=col;j++){

				if(i%3==1){
					System.out.print(num++ +"\t");
				}else if(i%3==2){
					System.out.print(ch++ +"\t");
				}else{
					System.out.print("#\t");
				}
			}
			System.out.println();
		}
	}
}


