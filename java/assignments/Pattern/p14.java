/*
 Take no of rows from the user ROWS = 4
D D D D
C C C C
B B B B
A A A A
 */ 

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		char ch = (char)(64+row);

		for(int i =1;i<=row;i++){
			for(int j = 1;j<=row;j++){

				System.out.print(ch+"\t");

			}
			System.out.println();
			ch--;
		}
	}
}

