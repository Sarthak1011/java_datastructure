/*
 ake no of rows from the user ROWS = 4
d d d d
c c c c
b b b b
a a a a

*/
import java.io.*;
class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

                char ch = (char)(96+row);
                for(int i = 1;i <= row;i++){
                        for(int j = 1 ;j<=row;j++){
                                System.out.print(ch +"\t");
                                
                        }
			ch--;
                        System.out.println();
                }
        }
}
