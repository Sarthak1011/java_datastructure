/*
 Take no of rows from the user ROWS = 3
*
* *
* * *

 */

import java.io.*;

class Pattern1{

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		for(int i = 1;i<=row;i++){
			for(int j=1;j<=i;j++){

				System.out.print("*\t");
			}
			System.out.println();
		}
	}
}
