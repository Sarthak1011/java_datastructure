/*
 Take no of rows from the user
ROWS = 3
1 1 1
2 2 2
3 3 3

 */

import java.io.*;
class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

                for(int i = 1;i <= row;i++){
                        for(int j = 1 ;j<=row;j++){
                                System.out.print(i+"\t");

                        }
                        System.out.println();
                }
        }
}

