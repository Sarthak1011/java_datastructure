/*
 Take no of rows from the user ROWS = 3
1 3 5
5 7 9
9 11 13
 */ 


import java.io.*;
class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter the rows");
		int row = Integer.parseInt(obj.readLine());

		int num = 1;

		for(int i = 1;i<=row;i++){
			for(int j=1;j<=row;j++){

				System.out.print(num+"\t");
				num=num+2;
			}
			System.out.println();
		}
	}
}
