/*
 Take no of rows from the user ROWS = 3
A b C
d E f
G h I

 */ 

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter the rows");
		int row = Integer.parseInt(obj.readLine());

	   	char ch1 = 65;
		char ch2 = 97;
		for(int i = 1;i<=row;i++){
			for(int j = 1;j<=row;j++){

				if((i+j)%2==0){
					System.out.print(ch1+"\t");
				}else{
					System.out.print(ch2+"\t");
				}
				ch1++;
				ch2++;
			}
			System.out.println();	
		}
	}
}

