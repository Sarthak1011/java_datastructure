/*
  Take no of rows from the user
ROWS = 3
a b c
d e f
g h i
 */ 

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int rows = Integer.parseInt(obj.readLine());

		char ch ='a';

		for(int i =1;i<=rows;i++){
			for(int j =1;j<=rows;j++){

				System.out.print(ch++ +"\t");

			}
			System.out.println();
		}
	}
}

