/*
 Take no of rows from the user ROWS = 3
9 64 7
36 5 16
3 4 1
*/

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		int num = row * row;

		for(int i = 1;i<=row;i++){
			for(int j = 1;j<=row;j++){

				if((i+j)%2==0){

					System.out.print(num+"\t");
				
				}else{
					System.out.print(num*num+"\t");
				}
				num--;
			}
			System.out.println();
		}
	}
}
