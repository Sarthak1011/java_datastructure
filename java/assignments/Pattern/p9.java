/*
 9. Take no of rows from the user
ROWS = 4
1 2 3 4
a b c d
5 6 7 8
e f g h

 */ 

import java.io.*;
class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

                char ch = 'a';
		int num = 1;
                for(int i = 1;i <= row;i++){
                        for(int j = 1 ;j<=row;j++){
				if(i%2 == 0){
                                	System.out.print(ch+"\t");
					ch++;
				}else{
					System.out.print(num+"\t");
					num++;
				}
			}
                       
                        System.out.println();
                }
        }
} 

