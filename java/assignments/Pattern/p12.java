/*
 Take no of rows from the user ROWS = 3
a B c
d E f
g H i

 */

import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

                char ch1 = 97;
                char ch2 = 65;

                for(int i =1;i<=row;i++){
                        for(int j = 1;j<=row;j++){

                                if(j%2==1){

                                        System.out.print(ch1 +"\t");

                                }else{
                                        System.out.print(ch2 +"\t");
                                }
                                ch1++;
                                ch2++;
                        }

                        System.out.println();
                }
        }
}
