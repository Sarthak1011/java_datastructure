/*
 Take no of rows from the user ROWS = 3
1 2 3
2 3 4
3 4 5

 */

import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(obj.readLine());

	//	int num = 1;
                for(int i =1;i<=row;i++){
			int num =i;
                        for(int j = 1;j<=row;j++){

				System.out.print(num++ +"\t");
			
			}
		//	num=num-2;
                        System.out.println();
                }
        }
}
