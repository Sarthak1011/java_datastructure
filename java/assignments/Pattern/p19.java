/*
 Take no of rows from the user ROWS = 4
D c B a
a B c D
F e D c
b C d E

 */ 

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row=Integer.parseInt(obj.readLine());

		char ch1;
		char ch2;
		
			for(int i=1;i<=row;i++){
				for(int j=1;j<=row;j++){

					if(i%2==1){
						ch1=(char)(ch1+row);
						ch2=(char)(ch2+row);

						if((i+j)%2==0){
							System.out.print(ch1+"\t");
						}else{
							System.out.print(ch2+"\t");
						}
						ch1--;
						ch2--;
						
						
					}else{
					
						if((i+j)%2==1){
							System.out.print(ch2+"\t");
						}else{
							System.out.print(ch1 +"\t");
						}
						ch1++;
						ch2++;
						
					}
				}
				System.out.println();
			}
	}
}

					
