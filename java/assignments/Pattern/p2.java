/*
 Take no of rows & cols from the user
ROWS = 3
Col = 4
1 2 3 4
5 6 7 8
9 10 11 12
 */

import java.io.*;
class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int rows= Integer.parseInt(obj.readLine());

		System.out.println("Enter the colums");
		int cols = Integer.parseInt(obj.readLine());

		int num =1;
		for(int i = 1;i<=rows;i++){
			for(int j =1;j<=cols;j++){

				System.out.print(num++ +"\t");

			}
			System.out.println();
		}
	}
}
