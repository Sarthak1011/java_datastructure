/*
 Program 2
WAP to take size of array from user and also take integer elements from user Print
product of even elements only
input : Enter size : 9
Enter array elements : 1 2 3 2 5 10 55 77 99
output : 40
// 2 * 2 * 10
 */ 

import java.util.*;

class ArrayDemo{
	public static void main(String args[]){

		Scanner sc = new Scanner(System.in);

		System.out.println("enter the array size");
		int size = sc.nextInt();

		int arr[] = new int[size];

		int product = 1;
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				product = product*arr[i];
			}
		}
		System.out.println("product of even elements is :"+product);
	}
}
