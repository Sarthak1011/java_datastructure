/*
 Program 3
WAP to take size of array from user and also take integer elements from user Print
product of odd index only
input : Enter size : 6
Enter array elements : 1 2 3 4 5 6
output : 48
//2*4*6
 */ 

import java.io.*;

class ArrayDemo {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size =Integer.parseInt(obj.readLine());

		int arr[] = new int[size];

		int product =1;

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
			if(i%2==1){
				product = product * arr[i];
			}
		}

		System.out.println("product of odd index "+product);
	}
}

