/*
 1  2  3  4 
 2  4  6  8
 3  6  9  12
 4  8  12 16
 */ 

class c2w {
	public static void main(String args[]){

		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print(i*j+" ");
			}
			System.out.println();
		}
	}
}
