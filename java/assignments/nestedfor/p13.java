/*
 9  64  7
 36  5  16
 3   4   1
 */ 


class c2w {
	public static void main(String args[]){

		int row=3;

		int  num=row*row;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){

				if(num%2==0){
					System.out.print(num * num+" ");
				}else{
					System.out.print(num+" ");
				}
				num--;
			}
			System.out.println();
		}
	}
}
