/*
 Q9
Write a program to take a number as input and print the Addition of Factorials of each
digit from that number.
Input: 1234
Output: Addition of factorials of each digit from 1234 = 33

 */ 

import java.io.*;

class c2w{

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");
		int num=Integer.parseInt(obj.readLine());
		int n=num;

		int sum=0;
		for(int i=1;num!=0;i++){

			int rem=num%10;
			int fact=1;

			for(int j=1;j<=rem;j++){
				fact = fact * j;
			}
			sum=sum+fact;
			num=num/10;
		}       
		System.out.println( "Addition of factorials of each digit from "+n+" = "+sum );
	}
}
