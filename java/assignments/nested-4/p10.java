/*
 Q10
write a program to print a series of prime numbers from entered range. ( Take a start and end number
from a user )
Perform dry run at least from 10 to 20 …
Input:-
Enter starting number: 10
Enter ending number: 100
Output:-
Series = 11 13 17 19 ….. 89 99
 */  

import java.io.*;

class c2w {

	public static void main(String args[])throws IOException{

	BufferedReader obj=new BufferedReader(new InputStreamReader(System.in));

	System.out.println("enter the range ");
	int start = Integer.parseInt(obj.readLine());
	int end = Integer.parseInt(obj.readLine());

	for(int i=start;i<=end;i++){
		int count=0;
		for(int j=2;j<=i/2;j++){
			if(i%j==0){
				count++;
				break;
			}
		}
		if(count==0){
			System.out.print(i+"\t");
		}
	}
	System.out.println();
	}
}




