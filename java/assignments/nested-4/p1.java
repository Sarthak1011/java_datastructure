/*D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
USE THIS FOR LOOP STRICTLY for the outer loop
Int row;
Take the number of rows from user
for(int i =1;i<=row;i++){
}
*/



import java.io.*;

class c2w {

	public static void main(String args[]) throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		char ch = (char)(64+row);

		for(int i=1;i<=row;i++){
			int num=row;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print(ch-- +""+num-- +" ");
				}else{
					System.out.print(++ch +"" +j+" ");
				}
			}
			System.out.println();
		}
	}
}

