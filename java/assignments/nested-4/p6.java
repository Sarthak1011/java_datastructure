/*
 Q6
Write a program, and take two characters if these characters are equal then print them as it is but if
they are unequal then print their difference.
{Note: Consider Positional Difference Not ASCIIs}
Input: a p
Output: The difference between a and p is 15
 */ 

import java.io.*;
import java.util.*;
class c2w {
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the two charcter");

		char ch1=(char)obj.read();
		obj.skip(1);
		char ch2=(char)obj.read();
		obj.skip(1);

		if(ch1==ch2){
			System.out.println("both are equal");
		}else if(ch1>ch2){
			System.out.println("The difference between " + ch1+ " and " +ch2+ " is "+(ch1-ch2));
		}else{
			System.out.println("The difference between " + ch1+ " and " +ch2+ " is "+(ch2-ch1));
		}
	}
}


	
