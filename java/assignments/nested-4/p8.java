/*
Q8
write a program to print the following pattern
Row =8
$
@ @
& & &
# # # #
$ $ $ $ $
@ @ @ @ @ @
& & & & & & &
# # # # # # # #
USE THIS FOR LOOP STRICTLY for the outer loop
Int row;
Take row from user
for(int i =1;i<=row;i++){
}
 */

import java.io.*;

class c2w {
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(obj.readLine());

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if( i%(row/2) == 1){
					System.out.print("$\t");
				}else if(i%(row/2)== 2){
					System.out.print("@\t");
				}else if(i%(row/2) == 3){
					System.out.print("&\t");
				}else {
					System.out.print("#\t");
				}
			}
			System.out.println();
		}
	}
}


