/*
 Q5
write a program to print the following pattern
Row =4
0
1 1
2 3 5
8 13 21 34
 */


import java.io.*;

class c2w {

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row=Integer.parseInt(obj.readLine());

		int x1=0,x2=1;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				System.out.print(x1+"\t");
				int temp=x1+x2;
				x1=x2;
				x2=temp;
			}
			System.out.println();
		}
	}
}


