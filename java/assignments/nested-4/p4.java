/*
 Q4
WAP to print all even numbers in reverse order and odd numbers in the standard way. Both separately.
Within a range. Take the start and end from user
Input: Enter start number - 2
Enter End number - 9
Output:
8 6 4 2
3 5 7 9

 */ 


import java.io.*;
class c2w {
public static void main(String args[])throws IOException{


	BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

	System.out.println("enter the Start and last number");
	int first = Integer.parseInt(obj.readLine());
	int last = Integer.parseInt(obj.readLine());

	for(int i=last;i>=first;i--){
		if(i%2==0){
			System.out.print(i+"\t");
		}
	}
	System.out.println();
	for(int i=first;i<=last;i++){
		if(i%2==1){
	
			System.out.print(i+"\t");
		}
	}
	System.out.println();
}

}
