/*
 Q3
write a program to print the following pattern
5 4 3 2 1
8 6 4 2
9 6 3
8 4
5

 */ 

import java.io.*;
class c2w {
public static void main(String args[])throws IOException{

	BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

	System.out.println("enter the rows");
	int row = Integer.parseInt(obj.readLine());


	for(int i=1;i<=row;i++){
		int num=(row-i+1)*i;
		for(int j=row;j>=i;j--){

			System.out.print(num+"\t");
			num=num-i;
		}
		System.out.println();
	}
}
}

