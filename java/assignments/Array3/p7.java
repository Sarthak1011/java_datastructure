/*
 Program 7
WAP to find a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5
*/

import java.io.*;

class ArrayDemo {

	int StrongNum(int arr[]){

		for(int i=0;i<arr.length;i++){
			int num=arr[i];

			int sum =0;
			while(num != 0){
				int rem = num%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact = fact *j;
				}
				sum = sum + fact;
				num=num/10;
			}
			if(arr[i]==sum){
				return i;
			}
		}
		return -1;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the arra size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];
		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		int ret=obj2.StrongNum(arr);

		if(ret != -1){
			System.out.println("The index of strong number is "+ ret);
		}else{
			System.out.println("Threr is no strong number");
		}
	}
}


