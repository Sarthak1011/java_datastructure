/*
 Program 8
WAP to find an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4
 */ 


import java.io.*;

class ArrayDemo {

	int CountDigit(int num){
	
		int count=0;
		while(num != 0){
			count++;
			num=num/10;
		}
		return count;
	}

	int ArmstrongNum(int arr[]){

		for(int i=0;i<arr.length;i++){
			int num = arr[i];
		        int count = CountDigit(arr[i]);
			int sum=0;
			while(num != 0){
				int rem = num%10;
				int mul=1;
				for(int j=1;j<=count;j++){
					mul = mul * rem;
				}
				sum = sum + mul;
				num=num/10;
			}
			if(arr[i]==sum ){
				return i;
			}
		}
		return -1;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		int ret= obj2.ArmstrongNum(arr);

		if(ret != -1){
			System.out.println("The armstrong number present at "+ret);
		}else{
			System.out.println("There is no armstrong number");
		}
	}
}

