/*
 Program 6
WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
Output: Palindrome no 252 found at index: 2

 */

import java.io.*;

class ArrayDemo {
	
	int Palindrome(int arr[]){

		for(int i=0;i<arr.length;i++){

			int num=arr[i];

			int rem=0;
			while(num!=0){
				 rem = (rem * 10)+num%10;
				 num= num/10;
			}
			if(rem == arr[i]){
				return i;
			}
		}
		return -1;
	}
	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		
		int ret = obj2.Palindrome(arr);

		if(ret != -1){
			System.out.println("The index of palindrome number is "+ret);
		}else{
			System.out.println("There is no palindrome number in array");
		}
	}
}


