/*
 Program 5
WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3
 */ 


import java.io.*;

class ArrayDemo {


	int PerfectNum(int arr[]){

		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=1;j<=arr[i]/2;j++){
				
				if(arr[i]%j==0){
				sum=sum+j;
				}
			}
			if(sum==arr[i]){
				return i;
			}
		}
		return -1;
	}



	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		System.out.println("Enter the array elements");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2 = new ArrayDemo();

		int ret=obj2.PerfectNum(arr);

		if(ret!=-1){
			System.out.println("The perfect number in array of index "+ret);
		}else{
			System.out.println("There is no perfect number in the array");
		}
	}
}

