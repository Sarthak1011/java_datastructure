/*
 Program 10
Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15
 */ 


import java.io.*;

class ArrayDemo {


	
	int SecondMin(int arr[]){

		if(arr.length>1){
	
			for(int i=0;i<arr.length;i++){
				for(int j=0;j<arr.length-i-1;j++){
					if(arr[j]>arr[j+1]){
						int temp = arr[j];
						arr[j]=arr[j+1];
						arr[j+1]=temp;
					}
				}
			}
			return arr[1];
		}
		return -1;
	}

	

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}


		ArrayDemo obj2 = new ArrayDemo();

		int ret = obj2.SecondMin(arr);

		if(ret != -1){
			System.out.println("The second min element in array is "+ret);
		}else{
			System.out.println("wrong input");
		}
	}
}
