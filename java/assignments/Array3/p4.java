/*
 Program 4
WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5
 */ 

import java.io.*;
class ArrayDemo {
	
	int PrimeNum(int arr[]){


		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=1;j<=arr[i]/2;j++){
				if(arr[i]%j==0){
					count++;
				}
			}
			if(count==1){
				return i;
			}
		}
		return -1;
	}


	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];

		System.out.println("enter the array elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2= new ArrayDemo();

		int ret=obj2.PrimeNum(arr);

		if(ret !=-1){
			System.out.println("Prime number in the array there index is "+ret);
		}else{
			System.out.println("There is no prime number in the array");
		}

	}
}


