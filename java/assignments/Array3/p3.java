/*
 Program 3
WAP to find a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4

 */

import java.io.*;
class ArrayDemo{
	
	int CompositeNum(int arr[]){

	
		
		for(int i=0;i<arr.length;i++){
			int j=2;
			if(j<=arr[i]/2){
				if(arr[i]%j==0){
					return i;
				}else{
					j++;
			     }
		       }  
		   } 
		return -1;

	}


	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];
		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		ArrayDemo obj2= new ArrayDemo();

		int ret=obj2.CompositeNum(arr);

		if(ret != -1){
			System.out.println("composite number index is "+ret);
		}else{
			System.out.println("not composite number found");
		}

	}
}
		
