/*
 Program 2
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
 */ 

import java.io.*;

class ArrayDemo{

	void reverseEle(int arr[],int rev[]){

		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int reve=0;
			while(num!=0){
				 reve= (reve*10)+num%10;
				 num=num/10;
			}
			rev[i]=reve;
		}
	}
				



	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size ");
		int size = Integer.parseInt(obj.readLine());

		int arr[]= new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		int rev[]= new int[arr.length];

		ArrayDemo obj2 = new ArrayDemo();

		obj2.reverseEle(arr,rev);

		for(int i=0;i<arr.length;i++){
			System.out.println(rev[i]);
		}
	}
}
