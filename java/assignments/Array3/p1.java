/*
 Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4
 */ 

import java.io.*;

class ArrayDemo {


	void CountDigit(int arr[],int CountArr[]){

		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int count=0;
			while(num!=0){
				count++;
				num=num/10;
			}
			CountArr[i]=count;
		}
	}
			

	public static void main(String args[])throws IOException{

		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");

		int size = Integer.parseInt(obj.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the array elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(obj.readLine());
		}

		int CountArr[]=  new int[arr.length];

		ArrayDemo obj2= new ArrayDemo();

		 obj2.CountDigit(arr,CountArr);

		for(int i=0;i<arr.length;i++){
			System.out.println(CountArr[i]);
		}
	}
}
