/*
 Q5
write a program to print the following pattern
A B C D
B C D
C D
D
 */ 


class c2w {
       public static void main(String args[]){

	       int row=4;
	       char ch1='A';

	       for(int i=1;i<=row;i++){
		       char ch2=ch1;
		       for(int j=row;j>=i;j--){
			       System.out.print(ch2++ +" ");
		       }
		       System.out.println();
		       ch1++;
	       }
       }
}
