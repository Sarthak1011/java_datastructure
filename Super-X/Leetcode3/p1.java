/*
 1. To Lower Case (Leetcode-709)
Given a string s, return the string after replacing every uppercase
letter with the same lowercase letter.
Example 1:
Input: s = "Hello"
Output: "hello"
Example 2:
Input: s = "here"
Output: "here"
Example 3:
Input: s = "LOVELY"
Output: "lovely"

Constraints:
1 <= s.length <= 100
s consists of printable ASCII characters
 */ 


import java.io.*;

class StringDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


                System.out.println("Enter the String");
                String str = br.readLine();

                char arr[] = str.toCharArray();
                for(int i =0;i<arr.length;i++){
                        if(arr[i] >= 'A' && arr[i]<='Z'){
				arr[i]=(char)(arr[i]+32);
			}
                }
		System.out.println(arr.toString());
               System.out.println(arr);
        }
}
