/*
 1  2  3  4 
 4  5  6  7 
 6  7  8  9
 7  8  9  10

 */

import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(br.readLine());

		int num = 1;
		for(int i = 1;i<=row;i++){
			for(int j = 1;j<=row;j++){

				System.out.print(num++ +"\t");

			}
			System.out.println();
			num = num - i;
		}
		/*
		System.out.println("\n\n");

		int k = 0;
		int x = 1;
		for(int i = 1;i<=row*row;i++){

			if(i % row == 0){
				System.out.println(x);
				x = x - k;
				k++;
			}else{
				System.out.print(x++ +"\t");
			}
		}
		*/
	}
}
