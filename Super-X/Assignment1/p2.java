/*
 0 
 3   8
 15  24  35
 48  63  80  99
 */ 
import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());

		int num = 0;
		int k = 1;
		for(int i = 1;i<=row;i++){

			for(int j = 1;j<=i;j++){

				System.out.print(num+"\t");

				k = k + 2;
			       num = num + k;
			}
			System.out.println();	
		}
	}
}	


