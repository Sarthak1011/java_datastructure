/*
 5 
 6  8
 7  10  13
 8  12  16  20
 9  14  19  24  29
 */ 

import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());

		for(int i = 1;i<=row;i++){
			int num = row + i-1;
			for(int j = 1;j<=i;j++){
				System.out.print(num +"\t");
				num = num +i;
			}
			System.out.println();
		}
	}
}

