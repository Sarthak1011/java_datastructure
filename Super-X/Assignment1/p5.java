/*
 Prime Number
 */ 
import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the starting number");
                int start = Integer.parseInt(br.readLine());
                
		System.out.println("Enter the Ending number");
                int end = Integer.parseInt(br.readLine());
			
		
		for(int i = start; i<=end;i++){
			int flag = 1;
			for(int j = 2;j<=i/2;j++){
				if(i % j == 0){
					flag = 0;
					break;
				}
			}
			if(flag == 1 && i != 1){
				System.out.println(i);
			}
		}
	}
}

