/*
Que 3: WAP to check whether the given number is perfect or not.
 */ 
import java.io.*;

class NumberDemo {
        static boolean perfectNum(int num){
                int sum = 0;
          	for(int i = 1;i<=num/2;i++){
			if(num % i == 0){
				sum = sum + i;
			}
		}
		if(sum == num){
			return true;
		}
                return false;
        }
        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the number ");
                int num = Integer.parseInt(br.readLine());
                
		boolean ret = perfectNum(num);
		if(ret == true){
			System.out.println("Given number is Perfect Number");
		}else{
			System.out.println("Not the perfect number");
		}
        }
}
	
