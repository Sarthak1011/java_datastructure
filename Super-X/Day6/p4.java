/*
 Que 4: WAP to print strong numbers in a given range.
Input: 1 to 10
 */ 
import java.io.*;

class NumberDemo {
        static void StrongNum(int start , int end){
             int temp = num;
             int sum =0;

	     for(int i = start; i<=end;i++){

             	while(temp != 0){
                	int rem = temp % 10;
                	int fact = 1;
                	for(int i = 2;i<=rem;i++){
                        
				fact = fact * i;

                	}	
                sum = sum + fact;
                temp = temp/10;
             }
             if(sum == num){
                     return 0;
             }
            return -1;
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the start");
                int start = Integer.parseInt(br.readLine());

                System.out.println("Enter the end");
                int end = Integer.parseInt(br.readLine());
                
		StrongNum(start , end);
        }
}
