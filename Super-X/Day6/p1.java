/*
  Que 1: WAP to print the following pattern
 Take input from the user
 A  B  C  D
 1  3  5  7
 A  B  C  D
 9  11 13 15
 A B C D
 */ 

import java.io.*;

class Pattern {
        static void Pattern(int row, int col){

		int num = 1;
                for(int i = 1;i<=row;i++){
                        char ch = 65;
                        for(int j = 1;j<=col;j++){

                                if(i % 2 == 1){
                                        System.out.print(ch++ + "\t");
                                }else{
                                        System.out.print(num +"\t");
					num = num + 2;
                                }
                        }
                        System.out.println();
                }
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());

                System.out.println("Enter the col");
                int col = Integer.parseInt(br.readLine());
                Pattern(row,col);
        }
}
