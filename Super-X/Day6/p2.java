/*
 Que 2: WAP to print the following pattern
Take row input from the user
1
7 26
63 124 215
342 511 728 999

 */ 
import java.io.*;

class Pattern {
        static void Pattern(int row){

                int num = 1;
		int cube = 1;
                for(int i = 1;i<=row;i++){
			for(int j = 1;j<=i;j++){

                      		System.out.print(cube + "\t");
				num++;
		      		cube = num * num * num - 1;
         	       }
		       System.out.println();
		}
		
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());
                Pattern(row);
        }
}
