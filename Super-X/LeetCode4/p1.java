/*
 Day-4
 Multiply Strings (LeetCode- 43)
 Given two non-negative integers num1 and num2 represented as strings,
 return the product of num1 and num2, also represented as a string.
 Note: You must not use any built-in BigInteger library or convert the inputs
 to integers directly.
 Example 1:
 Input: num1 = "2", num2 = "3"
 Output: "6"
 Example 2:
 Input: num1 = "123", num2 = "456"
 Output: "56088"
 Constraints:
 1 <= num1.length, num2.length <= 200
 num1 and num2 consist of digits only.
 Both num1 and num2 do not contain any leading zero, except the number 0
 itself.
*/
  
import java.io.*;

class StringDemo{

	static String MultiplyStrings(String str1 , String str2){
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		//[1 , 2, 3]
		//[4 , 5, 6]
		int temp1 = 0;
		for(int i = 0;i<arr1.length;i++){
			int num1 = arr1[i] - 48;
			 temp1 = temp1 * 10 + num1;
		} 
		int temp2 = 0;
		for(int i = 0;i<arr2.length;i++){
			int num2 = arr2[i] - 48;
			 temp2 = temp2 * 10 + num2;
		}
	       	int mul = temp1 * temp2;
       		String str = Integer.toString(mul);
	    return str;	

	}
	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String that contains only non Negative Integer");
		String str1 = br.readLine();
		
		System.out.println("Enter the String that contains only non Negative Integer");
		String str2 = br.readLine();

		System.out.println( MultiplyStrings(str1 , str2));

	}
}
