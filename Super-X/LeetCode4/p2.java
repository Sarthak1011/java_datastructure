/*
 Reverse String (LeetCode- 344)
 Write a function that reverses a string. The input string is given as an array
 of characters s.
 You must do this by modifying the input array in-place with O(1) extra
 memory.
 Example 1:
 Input: s = ["h","e","l","l","o"]
 Output: ["o","l","l","e","h"]
 Example 2:
 Input: s = ["H","a","n","n","a","h"]
 Output: ["h","a","n","n","a","H"]
 Constraints:
 1 <= s.length <= 105
 s[i] is a printable ascii character
 */ 

import java.io.*;

class StringDemo {

	static String[] ReverseString(String[] str){

		int start = 0;
		int end = str.length-1;
		while(start < end){
			String temp = str[start];
		       	str[start] = str[end];
	       		str[end] = temp;
			start++;
			end--;
		}
		return str;
	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array size");
		int size = Integer.parseInt(br.readLine());

		String str[] = new String[size];
		System.out.println("Enter the Array Elements");
		for(int i = 0;i<size;i++){
			str[i]= br.readLine();
		}
		String str1[] = ReverseString(str);

		for(int i = 0;i<str1.length;i++){
			System.out.print(str[i]+"\t");
		}
		System.out.println();
	}
}



