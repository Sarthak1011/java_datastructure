/*
 2. Find the Index of the First Occurrence in a String(LeetCode 28)


Given two strings needle and haystack, return the index of the first occurrence of needle
in haystack, or -1 if needle is not part of haystack.
Example 1:
Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.
Example 2:
Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.

Constraints:
1 <= haystack.length, needle.length <= 104
haystack and needle consist of only lowercase English characters.
 */ 

import java.io.*;
class StringDemo {
	static int strStr(String haystack, String needle) {
        	char carr1[] = haystack.toCharArray();
        	char carr2[] = needle.toCharArray();
        	int j = 0;
        	int store = 1;
        	int flag = 0;
        	for(int i = 0;i<carr1.length;i++){
                        if(j < carr2.length){

                            if(carr1[i] == carr2[j]){
                                    flag = 1;
                                    store = i;
                                    j++;
                            }else{
                                    flag = 0;
                                    j = 0;
                            }
                        }
                        if( j == carr2.length){
                                return store - j + 1;
                 	}
        	 }
		return -1;
       }

	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String1");
		String str1 = br.readLine();

		System.out.println("Enter the Second String");
		String str2 = br.readLine();

		System.out.println(strStr(str1 , str2));
	}
}
