/*
Palindrome Number(LeetCode 9)
Given an integer x, return true if x is a palindrome, and false otherwise.
Example 1:
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
Example 2:
Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-.
Therefore it is not a palindrome.
Example 3:
Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Constraints:
-231 <= x <= 231 - 1
*/ 

import java.io.*;

class NumberDemo {

	static boolean palindromeNum(int num ){
		int temp1 = num,rev = 0;
		while(temp1 != 0){
			rev = rev * 10 + temp1%10;
			temp1 = temp1 /10;
		}

		if(rev == num){
			return true;
		}
		return false;
	}

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");
		int num = Integer.parseInt(br.readLine());

		System.out.println(palindromeNum(num));
	}
}
