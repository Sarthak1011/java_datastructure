/*
 Que 5: WAP to toggle the String to uppercase or lowercase
 Input: Java output: jAVA
 Input: data
 output: DATA
 */ 
import java.io.*;

class StringDemo {
	static char[] LowerCase(char []arr){
                for(int i =0;i<arr.length;i++){
                        if(arr[i] >= 'A' && arr[i]<='Z'){
                                arr[i]=(char)(arr[i]+32);
                        }else{
                              arr[i] =(char)(arr[i]-32);
                        }
                }
		return arr;


	}

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


                System.out.println("Enter the String");
                String str = br.readLine();


               char[] arr= str.toCharArray();
               System.out.println(LowerCase(arr));
        }
}
