/*
  Que 2: WAP to print the following pattern
 Take row input from the user
 A
 B A
 C B A
 D C B A
 */ 
import java.io.*;

class Pattern {
        static void Pattern(int row){

                for(int i = 1;i<=row;i++){
                        char ch = (char)(64+i);
                        for(int j = 1;j<=i;j++){

                            System.out.print(ch-- + "\t");
			}
			System.out.println();
		}
                   
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());

                Pattern(row);
	
        }
}
