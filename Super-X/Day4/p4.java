/*
  Que 4: WAP to print the sum of digits in a given range.
 Input: 1 to 10
 Input: 21 to 30
 */

import java.io.*;

class NumberDemo {
	static int SumDigit(int start , int end){
		int sum = 0;
		for(int i = start;i<=end;i++){
			sum = sum + i;
		}
		return sum;
	}
        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("How many times you sum ");
		int num = Integer.parseInt(br.readLine());
		for(int i = 1;i<=num;i++){
                	System.out.println("Enter the start");
                	int start = Integer.parseInt(br.readLine());

                	System.out.println("Enter the end");
                	int end = Integer.parseInt(br.readLine());
			int sum = SumDigit(start , end);
			System.out.println("Sum of Digits from"+start+" to "+end+" is "+sum );
		}
	}
}

		

