/*
 Que 3: WAP to find the factorial of a given number.
 */ 
import java.io.*;

class NumberDemo {
        static int Factorial(int num){
		int fact = 1;
                for(int i = 2;i<=num;i++){
			fact = fact * i;
                  
                }

		return fact;
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the num");
                int num = Integer.parseInt(br.readLine());

                System.out.println(Factorial(num));
        }
}
