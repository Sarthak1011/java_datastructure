/*
 Que 3 : WAP to check whether the given no is a palindrome number or not.
 */ 

import java.io.*;

class NumberDemo {

        static boolean palindromeNum(int num ){
                int temp1 = num,rev = 0;
                while(temp1 != 0){
                        rev = rev * 10 + temp1%10;
                        temp1 = temp1 /10;
                }

                if(rev == num){
                        return true;
                }
                return false;
        }

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the number");
                int num = Integer.parseInt(br.readLine());

                boolean x  = palindromeNum(num);
		if(x == true){
			System.out.println("Palindrom number");
		}else{
			System.out.println("Not Palindrome number");
		}
        }
}
