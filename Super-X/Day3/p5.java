/*
 Que 5 : WAP to check whether the string contains characters other than
letters.
 */ 

import java.io.*;

class StringDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


                System.out.println("Enter the String");
                String str = br.readLine();

                char arr[] = str.toCharArray();
		int j = 0;
                for(int i =0;i<arr.length;i++){
                        if(arr[i] >= 'A' && arr[i]<='Z' || arr[i] >= 'a' && arr[i] <= 'z'){
				arr[j++] = arr[i];
			}
                }
		for(int i =0;i<j;i++){
                	System.out.print(arr[i]);
		}
		System.out.println();
        }
}
