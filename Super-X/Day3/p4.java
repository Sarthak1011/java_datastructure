/*Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
 */

import java.io.*;

class NumberDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the start");
                int start = Integer.parseInt(br.readLine());

                System.out.println("Enter the end");
                int end = Integer.parseInt(br.readLine());


                for(int i = start;i<=end;i++){
			int rev = 0;
			int temp = i;
                        while(temp != 0){
		 		rev = rev* 10 + temp % 10;
				temp = temp/10;
			}
			System.out.println(rev);
                }
        }
}
