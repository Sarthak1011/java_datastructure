/*
 Two Sum (LeetCode 1)

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.
Example 1:
Input nums (2,7,11,15], target = 9 Output [0,1]
Explanation: Because nums[0]+ nums[1]== 9, we return [0, 1].
Example 2:
Input nums 13.2.4], target = 6
Output [1,2]
Example 3:
Input: nums[3.31 target=6 Output: [0,1]
Constraints:
2 < nums length << 104
-109< nums[] < 109 -109 target = 109 Only one valid answer exists.
 */ 
import java.io.*;

class Array {
	static int[] Sum(int arr[] , int target){

		int arr1[] = new int[2];
		for(int i =0;i<arr.length-1;i++){
			if(arr[i] + arr[i+1]==target){
				arr1[0] = i;
			       	arr1[1] = i+1;
				break;
			}
		}
		return arr1;
	}
	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter the array Elements");

		for(int i = 0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter the Target Element");
		int target = Integer.parseInt(br.readLine());

		int arr2[] = Sum(arr , target);
		System.out.println(arr2[0] + ""+ arr2[1]);
	}
}
