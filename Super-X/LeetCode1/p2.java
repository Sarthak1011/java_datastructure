/*
 Search Insert Position (LeetCode 35)

Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5 Output: 2
Example 2:
Input: nums[1,3,5,6). target = 2
Output: 1
Example 3:
Input: nums[1,3,5,6], target = 7
Output: 4
Constraints:
1< nums.length <= 104
-104< nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 target <= 104
 */ 
import java.io.*;
class Array {

	static int InsertPos(int arr[] , int target ){

		if(target > arr[arr.length -1]){
			return arr.length;
		}else{

			for(int i =0;i<arr.length;i++){
				if(arr[i] >= target){
					return i;
				}
			}
		}
	return -1;	
	}

  	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Array Size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter the array Elements");

                for(int i = 0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter the Target Element");
                int target = Integer.parseInt(br.readLine());

		System.out.println(InsertPos(arr , target));
  	}	
}


