/*
Day-5
 Missing Number (LeetCode- 268)
 Given an array nums containing n distinct numbers in the range [0, n],
 return the only number in the range that is missing from the array.
 Example 1:
 Input: nums = [3,0,1]
 Output: 2
 Explanation: n = 3 since there are 3 numbers, so all numbers are in the
 range [0,3]. 2 is the missing number in the range since it does not appear in
 nums.
 Example 2:
 Example 3:
 Input: nums = [0,1]
 Output: 2
 Explanation: n = 2 since there are 2 numbers, so all numbers are in the
 range [0,2]. 2 is the missing number in the range since it does not appear in
 nums.
 Input: nums = [9,6,4,2,3,5,7,0,1]
 Output: 8
 Explanation: n = 9 since there are 9 numbers, so all numbers are in the
 range [0,9]. 8 is the missing number in the range since it does not appear in
 nums.
 Constraints:
 n == nums.length
 1 <= n<=104
 0 <= nums[i] <= n
 All the numbers of nums are unique.
 
 */

import java.io.*;

class Array {
	static int MissingNum(int arr[]){

		for(int i = 0;i<arr.length;i++){
			for(int j = i+1;j<arr.length;j++){
				if(arr[i]>arr[j]){
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		for(int i = 0;i<arr.length-1;i++){
			if(arr[i] != arr[i+1]-1){
				return arr[i]+1;
			}
			
		}
		return arr[arr.length-1]+1;
	}

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter the Array elements");
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println(MissingNum(arr));
	}
}
