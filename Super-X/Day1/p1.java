/*Que 1 : WAPto print the following pattern
 Take input from user
 1 2 3 4 
 2 3 4 5 
 3 4 5 6
 4 5 6 7

 */
import java.io.*;

class Pattern {

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Rows");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1;i<=row; i++){
			int num = i;
			for(int j = 1;j<=row;j++){

				System.out.print(num++ +"\t");

			}
			System.out.println();
		}
	}
}
