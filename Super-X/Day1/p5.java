/*
  Que 5 : WAPto count the size of given string
 (without using inbuilt method)
 */ 

import java.io.*;

class StringDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


		System.out.println("Enter the String");
		String str = br.readLine();

		char arr[] = str.toCharArray();

		System.out.println(arr.length);
	}
}
