/*
 Que 3 : WAPto check whether the given no is odd or even
 */ 

import java.io.*;

class NumberDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Num");
                int num = Integer.parseInt(br.readLine());

		if(num %2 == 0){
			System.out.println("Given number is Even");
		}else{
			System.out.println("Given number is Odd");
		}
	}
}
