/*
 Que 4 : WAPto print the odd numbers in the given range
 Input: start:1
 end:10
 */

import java.io.*;

class NumberDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the start");
                int start = Integer.parseInt(br.readLine());
                
		System.out.println("Enter the end");
                int end = Integer.parseInt(br.readLine());


		for(int i = start;i<=end;i++){
			if(i % 2 == 1){
				System.out.println(i);
			}
		}
	}
}
