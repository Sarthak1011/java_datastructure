/*
WAP to find the Occurance of vowels in a given String

i/p - >adgtiosebi
o/p -> a = 1
       e = 1
       i = 2 
       o = 1
       u = 0
 */ 
import java.io.*;
class StringDemo {

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		char arr[] = str.toCharArray();
		int counta = 0, counte = 0,counti = 0, countO = 0,countu = 0 ; 

		for(int i = 0;i<arr.length;i++){
			if(arr[i]== 'a'){
				counta++;
			}else if(arr[i]=='e'){
				counte++;
			}else if(arr[i]=='i'){
				counti++;
			}else if(arr[i]=='o'){
				countO++;
			}else if(arr[i]=='u'){
				countu++;
			}
		}
		System.out.println(" a = "+counta +"\n" + " e = " +counte +"\n" + " i = "+counti +"\n" + " O = "+countO+ "\n" + " u = "+countu);
	}
}

