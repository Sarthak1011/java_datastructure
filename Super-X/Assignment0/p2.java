/*
 ArmStrong Number

 */ 


import java.io.*;

class Array {

	static int ArmStrongNum(int num){

		int temp = num,temp2 = num;
		int count = 0;
		while(num != 0){
			num = num /10;
			count++;
		}

		int sum = 0;
		while(temp != 0){

			int rem = temp % 10;
			int mul =1;
			for(int i = 1;i<=count;i++){
				mul = mul * rem;
			}
			sum = sum + mul;
			temp = temp / 10;
		}
		if(sum == temp2){
			return 0;
		}
		return -1;
	}
	public static void main(String args[])throws IOException{
             BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Number");
                int num = Integer.parseInt(br.readLine());

                int ret = ArmStrongNum(num);
		if(ret == 0){
			System.out.println("ArmStrong Number");
		}else{
			System.out.println("Not ArmStrong Number");
		}
        }
}
