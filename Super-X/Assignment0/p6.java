/*
 WAP to find no which has no its left is less than itself
i/p = 45675962
o/p = 9

 */ 

import java.io.*;

class Number{

	static int leftNum(int num){

		int temp = num;
		int count = 0;
		while(num != 0){
			count++;
			num = num /10;
		}
		int arr[] = new int[count];

		int j = 0;
		while(temp != 0){
			int rem = temp %10;
			arr[j++] = rem;
			temp = temp/10;
		}
		int max = Integer.MIN_VALUE;

		for(int i =0;i<arr.length;i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}
		return max;
	}

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the num");
                int num = Integer.parseInt(br.readLine());

		System.out.println(leftNum(num));
	}
}
