/*
 D  C  B  A
 e  f  g  h
 F  E  D  C
 g  h  i  j

 */

import java.io.*;

class Array {

	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Row");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1;i<=row;i++){
			char ch1 = (char)(63 + row + i);
			char ch2 = (char)(ch1 + 32 );

			for(int j = 1;j<=row;j++){

				if(i %2 == 1){
					System.out.print(ch1+"\t");
					ch1--;
				}else{
					System.out.print(ch2+"\t");
					ch2++;
				}
			}
			System.out.println();
		}
	}
}


