/*
 reverse a number and put succesive number sum into array and print
 i/p - > 45689
 0/p -> 17 14 11 9 4

 */

import java.io.*;
class Number {
	static int[] reverseNum(int num){

		int temp = num,count = 0 ;
		while(temp != 0){
			count++;
			temp = temp/10;
		}

		int arr[] = new int[count];
		int j = 0;
		while(num != 0){
			int rem = num % 10;
			arr[j++]= rem;
			num = num/10;
		}
		for(int i = 0 ;i<arr.length-1;i++){
			arr[i] = arr[i]+ arr[i+1];
		}
		return arr;
	}
        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Num");
                int num = Integer.parseInt(br.readLine());

		int arr[] = reverseNum(num);

		for(int i = 0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}




