/*
 factorial of even Number
 i/p = 256946
 o/p = 2 , 720 , 24 , 720
 */ 

import java.io.*;

class Array {

	static void FactNum(int num ){

		while(num != 0){

			int rem = num % 10;
			int fact = 1;
			if(rem % 2 == 0){

				for(int i = 1;i<=rem;i++){
			
					fact = fact * i;
				
				}
				System.out.println(fact);
			}
			num = num / 10;
		}

	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number");
		int num = Integer.parseInt(br.readLine());

		FactNum(num);
	}
}

