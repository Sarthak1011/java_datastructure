/*

   A  b  C  d  E
   e  D  c  B
   B  c  D
   d  C
   c

 */

import java.io.*;

class Array {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Row");
                int row = Integer.parseInt(br.readLine());

                char ch = 'A';
                for(int i = 1;i<=row;i++){

                        for(int j = 1;j <=row-i+1;j++){

                                if(i % 2 == 1){

                                        if((i+j) % 2 == 0){
                                                System.out.print(ch + "\t");
                                        }else{
                                                System.out.print((char)(ch+32)+"\t");
                                        }
                                        ch++;
                                }else{
                                        ch--;
                                        if((i+j) % 2 == 0){
                                                System.out.print(ch + "\t");
                                        }else{
                                                System.out.print((char)(ch+32)+"\t");
                                        }
                                }
                        }
                        System.out.println();
                }
        }
}

