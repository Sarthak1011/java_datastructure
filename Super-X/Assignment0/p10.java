/*
WAP to take String from the user and convert all even indexes of string to uppercase and odd indexes of string to lowercase
asCwSdcxS
 */ 
import java.io.*;
class StringDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the String");
                String str = br.readLine();

                char arr[] = str.toCharArray();

		for(int i = 0;i<arr.length;i++){

			if( i % 2 == 0){
				//System.out.println("even"+arr[i]);
				if(arr[i]>='A' && arr[i]<='Z'){
					System.out.print(arr[i]+" ");
				}else{
					System.out.print((char)(arr[i]-32)+" ");
				}
			}else{
				//System.out.println("else"+arr[i]);
				if(arr[i]>='a' && arr[i]<='z'){
					System.out.print(arr[i]+" ");
				}else{
					System.out.print((char)(arr[i]+32)+" ");
				}
			}
		}
		System.out.println();
	}
}

