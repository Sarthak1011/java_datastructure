/*
 Palindrome String 
 i/p -> malayalam
 */
import java.io.*;
class StringDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the String");
                String str = br.readLine();

                char arr[] = str.toCharArray();

		int start = 0;
		int end = arr.length-1;
		int flag = 1;
		while(start < end){
			if(arr[start] == arr[end]){
				start++;
				end--;
			}else{
				flag = 0;
				break;
			}
		}

		if(flag == 1){
			System.out.println("Palindrom String");
		}else{
			System.out.println("String is not Palindrome");
		}
	}
}
		


