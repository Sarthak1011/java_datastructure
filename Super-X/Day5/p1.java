/*
 Que 1: WAP to print the factorial of digits in a given range.
 Input: 1-10 
 */
import java.io.*;

class NumberDemo {
        static void Factorial(int start , int end){
             
                for(int i = start;i<=end;i++){
			int fact = 1;
			for(int j = 2;j<=i;j++){
                        	fact = fact * j;
			}
			System.out.print(fact+"\t");
                }
		System.out.println();
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the starting num");
                int start = Integer.parseInt(br.readLine());
                
		System.out.println("Enter the ending num");
                int end = Integer.parseInt(br.readLine());

                Factorial(start,end);
        }
}
