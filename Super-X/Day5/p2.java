/*
  Que 2: WAP to print the following pattern
 Take row input from the user
 a
 A B
 a b c
 A B C D
 
 */ 
import java.io.*;

class Pattern {
        static void Pattern(int row){

		
                for(int i = 1;i<=row;i++){
			char ch = 97;
                        for(int j = 1;j<=i;j++){
				if(i % 2 == 1){
                            		System.out.print(ch++ + "\t");
				}else{
					System.out.print((char)(ch -33+j)+"\t");
				}

                        }
                        System.out.println();
                }

        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());

                Pattern(row);
        }
}
