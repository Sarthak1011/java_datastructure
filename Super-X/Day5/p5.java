/*
 Que 5: WAP to print the occurrence of a letter in given String.
 Input String: “Know the code till the core”
 Alphabet : o
 Output: 3
 */ 

import java.io.*;

class StringDemo {
        static int LowerCase(char []arr,char ch){
		int count  = 0;
                for(int i =0;i<arr.length;i++){
                        if(arr[i] == ch){
                                count++;
			}
		}
                return count;
        }

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


                System.out.println("Enter the String");
                String str = br.readLine();

		System.out.println("Enter the Alphabate to check occurance");
		char ch =(char)( br.read());

               char[] arr= str.toCharArray();
               System.out.println(LowerCase(arr,ch));
        }
}
