/*
 Que 3: WAP to check whether the given number is a strong number or not.
 
 */ 
import java.io.*;

class NumberDemo {
        static int StrongNum(int num){
             int temp = num;
	     int sum =0;
	     while(temp != 0){
	     	int rem = temp % 10;
		int fact = 1;
             	for(int i = 2;i<=rem;i++){
                        fact = fact * i;

                }
		sum = sum + fact;
		temp = temp/10;
	     }
	     if(sum == num){
		     return 0;
	     }
            return -1;
        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the num");
                int num = Integer.parseInt(br.readLine());

                int strong = StrongNum(num);
		if(strong == 0){
			System.out.println("Strong number");
		}else{
			System.out.println("Not Strong number");
		}
        }
}
