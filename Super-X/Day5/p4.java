/*
 Que 3: WAP to check whether the given number is a strong number or not.
 Que 4: WAP to print the following pattern
 Take input from the user
 1   3   5  7
 2   4   6  8
 9  11  15 17
 10 12  14 16
 
 */ 

import java.io.*;

class Pattern {
        static void Pattern(int row){
		int num1 = 1;
		int num2 = 2;
                for(int i = 1;i<=row;i++){
                        for(int j = 1;j<=row;j++){
                                if(i % 2 == 1){
                                        System.out.print(num1 + "\t");
					num1 = num1 + 2;
                                }else{
                                        System.out.print(num2+"\t");
					num2 = num2 + 2;
                                }

                        }
                        System.out.println();
                }

        }

        public static void main(String args[])throws IOException{

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the rows");
                int row = Integer.parseInt(br.readLine());

                Pattern(row);
        }
}
