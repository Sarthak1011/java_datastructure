/*
 WAP to print the following Pattern
1 
2 4 
3 6 9
4 8 12 16
 */ 
import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Rows");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1;i<=row; i++){
                       int num = i;
                        for(int j = 1;j<=i;j++){

                                System.out.print(num +"\t");
				num  = num + i;

                        }
                        System.out.println();
                }
        }
}
