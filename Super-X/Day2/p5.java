
/*
 WAP to Check wheater the String contains vowels and return the count of vowels
 */ 
import java.io.*;

class StringDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


                System.out.println("Enter the String");
                String str = br.readLine();

                char arr[] = str.toCharArray();
		int count = 0;
                for(int i =0;i<arr.length;i++){
			if(arr[i] == 'a' || arr[i]=='A' ||arr[i] == 'e' || arr[i] =='E' || arr[i] == 'i'  || arr[i] == 'I'|| arr[i] == 'o' || arr[i]=='O' || arr[i] =='u'  || arr[i]=='U'){
					count++;			
			}
		}
		System.out.println(count++);
	}	
}
