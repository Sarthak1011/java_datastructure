/*
 WAP to print the given no is prime or composite
 */ 

import java.io.*;

class NumberDemo {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the start");
                int start = Integer.parseInt(br.readLine());

                System.out.println("Enter the end");
                int end = Integer.parseInt(br.readLine());


                for(int i = start;i<=end;i++){
			int flag =0;
			for(int j =2;j<=i/2;j++){
                        	if(i % j == 0){
                                	flag = 1;
					break;
				}
			}
			if(flag == 1){
				System.out.println(i);
			}
		}
	}
}
