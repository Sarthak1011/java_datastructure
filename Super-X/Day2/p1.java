/*Que 1 : WAPto print the following pattern
 Take input from user
 A B C D 
 B C D E
 C D E F
 D E F G

 */
import java.io.*;

class Pattern {

        public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Rows");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1;i<=row; i++){
                        char num = (char)(64 + i);
                        for(int j = 1;j<=row;j++){

                                System.out.print(num++ +"\t");

                        }
                        System.out.println();
                }
        }
}
