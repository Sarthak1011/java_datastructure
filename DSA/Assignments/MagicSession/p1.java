/*
 Problem 1:
Given an array containing n integers. The problem is to find the sum of the
elements of the contiguous subarray having the smallest(minimum) sum.
Examples:
Input : arr[] = {3, -4, 2, -3, -1, 7, -5}
Output : -6
Subarray is {-4, 2, -3, -1} = -6
Input : arr = {2, 6, 8, 1, 4}
Output : 1

 */
import java.io.*;

class Array {

	static int subArray(int arr[]){

		int min = Integer.MAX_VALUE;

		for(int i =0;i<arr.length;i++){
			int sum = 0;
			for(int j = i+1;j<arr.length;j++){
				sum = sum + arr[j];
				if(sum < min ){
					min = sum;
				}
			}
		}
		return min;
	}
	public static void main(String args[])throws IOException{

	/*	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Array Elements");
		int arr[] = new int[size];

		for(int i = 1;i<size;i++){
			arr[i] = Integer.parseInt(br.readLine);
		}*/
	      // 	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }

		System.out.println(subArray(arr));
	}
}
