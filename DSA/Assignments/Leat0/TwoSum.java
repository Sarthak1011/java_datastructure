/*
 2. Two Sum (Leetcode:- 1)
Given an array of integer numbers and an integer target, return indices of
the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you
may not use the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.

*/
import java.io.*;
class Array {

	int[] TwoSum(int arr[] , int target){


		int j =1;
		int newarr[] = new int[2];
		for(int i = 0;i<arr.length-1;j++){

			if(arr[i]+arr[j] == target){
				newarr[0] = i;
				newarr[1] = j;
				i++;
			}
			if(j<arr.length){
				i++;
				j = i;

			}
		}
		return newarr;
	}
	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the Array elements");
		for(int i = 0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());

		}

		System.out.println("Enter the Target element");
		int target = Integer.parseInt(br.readLine());

		Array obj = new Array();
		int arr1[] = obj.TwoSum(arr , target);

		for(int i = 0;i<arr1.length;i++){

			System.out.println(arr1[i]);
		
		}
	}
}
