/*
3. Search Insert Position (LeetCode-35)
Given a sorted array of distinct integers and a target value, return the index
if the target is found. If not, return the index where it would be if it were
inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4
Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
 */
import java.io.*;
class Array {

	int insertPos(int arr[] , int target){
		
		for(int i =0;i<arr.length;i++){
			if(arr[i]== target){
				return i;
			}else if(target < arr[i] ){
				return i;
			}
		}
		return arr.length;
	}
 public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }

                System.out.println("Enter the Target element");
                int target = Integer.parseInt(br.readLine());

                Array obj = new Array();
               	System.out.println(obj.insertPos(arr , target));
 }
}

