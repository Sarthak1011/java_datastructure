/*
 Q1. Max Min of an Array
Problem Description
- Given an array A of size N.
- You need to find the sum of the Maximum and Minimum
elements in the given array.
Problem Constraints
1 <= N <= 105
-109 <= A[i] <= 109
Example Input
Input 1:
A = [-2, 1, -4, 5, 3]
Input 2:
A = [1, 3, 4, 1]
Example Output
Output 1:
1
Output 2:
5
Example Explanation
Explanation 1:
Maximum Element is 5 and Minimum element is -4. (5 + (-4)) = 1.
Explanation 2:
Maximum Element is 4 and Minimum element is 1. (4 + 1) = 5.
=======================================================================
=========

 */
import java.io.*;

class Array {
	int maxMin(int arr[]){
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for(int i = 0;i<arr.length;i++){
			if(max < arr[i]){
				max = arr[i];
			}
			if(min > arr[i]){
				min = arr[i];
			}
		}
		return max + min;
	}
	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }

		Array obj = new Array();
		int sum = obj.maxMin(arr);
		System.out.println(sum);
	}
}

