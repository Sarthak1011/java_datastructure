/*
 Q4. Time to equality
Problem Description
- Given an integer array A of size N.
- In one second, you can increase the value of one element by 1.
- Find the minimum time in seconds to make all elements of the array
equal.
Problem Constraints
1 <= N <= 1000000
1 <= A[i] <= 1000
Example Input
A = [2, 4, 1, 3, 2]
Example Output
8
Example Explanation
We can change the array A = [4, 4, 4, 4, 4]. The time required will be 8
seconds.
=======================================================================
==================

 */

import java.io.*;
class Array {

	static int TimeEquality(int arr[]){
		int max = Integer.MIN_VALUE;
		for(int i = 0;i<arr.length;i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}
		int count =0;

		for(int i = 0;i<arr.length;){
			if(arr[i]==max){
				i++;
			}else{
				arr[i]=arr[i]+1;
				count++;
			}
		}
		return count;
	}
	public static void main(String args[])throws IOException{


		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }

		System.out.println(TimeEquality(arr));
	}
}




