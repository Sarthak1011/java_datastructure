/*
 Q2. Linear Search - Multiple Occurences
Problem Description
Given an array A and an integer B, find the number of occurrences
of B in A.
Problem Constraints
1 <= B, Ai <= 109
1 <= length(A) <= 105
Example Input
Input 1:
A = [1, 2, 2], B = 2
Input 2:
A = [1, 2, 1], B = 3
Example Output
Output 1:
2
Output 2:
0
Example Explanation
Explanation 1:
Element at index 2, 3 is equal to 2 hence count is 2.
Explanation 2:
There is no element equal to 3 in the array.
=======================================================================
==================
 */ 

import java.io.*;

class Array {

	int linearSearch(int arr[] , int B){

		int x = 0;
		for(int i = 0;i<arr.length;i++){
			if(arr[i]==B){
				x = i;
			}
		}
		return x;
	}
  	public static void main(String args[])throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }
		System.out.println("Enter the Searching Element");
		int Search = Integer.parseInt(br.readLine());

                Array obj = new Array();
                int ele = obj.linearSearch(arr,Search);
                System.out.println(ele);
	}
}
