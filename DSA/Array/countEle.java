/*
2. 
Given an integer array of size N count the no of elements having atleast 1 element greater than itself
arr: [2,5,1,4,8,0,8,1,3,8]
N :10
o/p:7 
brute force
t.c = o(n^2)
s.c = 0(1)
optimes 
t.c = o(n) 
s.c = o(1)
 
 */

class Array {

	static int bruteforce(int arr[]){

		int count = 0;
		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length;j++){
				if(arr[i]<arr[j]){
					count++;
					break;
				}
			}
		}
		return count;
	}
	static int optimise(int arr[]){
		int count = 0;
		int max = Integer.MIN_VALUE;

		for(int i = 0;i<arr.length;i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}

		for(int i = 0;i<arr.length;i++){
			if(max > arr[i]){
				count++;
			}
		}
		return count;
	}

	public static void main(String args[]){

		int N = 10;
		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};

		System.out.println(bruteforce(arr));
		System.out.println(optimise(arr));
		
	}
}
