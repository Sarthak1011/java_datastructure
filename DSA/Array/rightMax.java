/*
 right max array
 */ 
import java.util.*;
class Array {

	static int[] rightArray(int arr[]){

		int right[] = new int[arr.length];

		right[arr.length - 1]= arr[arr.length-1];
		for(int i = arr.length - 2;i>=0;i--){
			if(arr[i]>right[i+1]){
				right[i] = arr[i];
			}else{
				right[i] = right[i+1];
			}
		}
		return right;
	}
 	public static void main(String  args[]){


                Scanner sc = new Scanner(System.in);

                System.out.println("Enter the Array Size");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = sc.nextInt();

                }
                //int arr1[] = leftMax(arr);
                //int arr2[] = optimise(arr);
                int arr3[] = rightArray(arr);

                for(int i = 0;i<arr3.length;i++){

                        System.out.print(arr3[i]+"\t");

                }
                System.out.println();
        }

  }					
