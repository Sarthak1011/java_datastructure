/*
 1. 
given an array of size N print all the elements
arr: [5,6,2,3,1,9]
N = 6
*
*/

class Array {

	public static void main(String [] args){

		int N = 6;

		int arr[] = new int[]{5,6,2,3,1,9};

		for(int i = 0;i<arr.length;i++){

			System.out.println(arr[i]);
		}
	}
}
