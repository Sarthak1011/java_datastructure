/*
 4. 
Given an array size N reverse the array
arr:[1,2,3,4,5] 
brute force
t.c = o(N)
s.c = o(N)
optimise 
t.c = o(N)
s.c = o(1)
 */ 

class Array {

	static int[] bruteforce(int arr[]){

		int newarr[] = new int[arr.length];

		int j = arr.length -1;
		for(int i = 0;i<arr.length;i++){
			newarr[i] = arr[j--];
		}
		return newarr;
	}

	static int[] optimise(int arr[]){

		for(int i = 0;i<arr.length/2;i++){
			int temp = arr[i];
			arr[i] = arr[arr.length - i -1];
			arr[arr.length - i -1]= temp;
		}
		return arr;
	}
	public static void main(String args[]){

		int arr[] = new int[]{1,2,3,4,5,6};

		int arr1[] = bruteforce(arr);
		
		for(int i = 0;i<arr1.length;i++){
			System.out.println(arr1[i]);
		}
		
		int arr2[] = optimise(arr);
		
		for(int i = 0;i<arr2.length;i++){
			System.out.println(arr2[i]);
		}
	}
}



