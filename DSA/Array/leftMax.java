/*
 Given an integer array of size N build an array leftmax of size N leftmax of i contains maximum for the index to the index i
arr: [-3,6,2,4,5,2,8,-9,8]
 */ 

import java.util.*;
class Array {
/*
	static int[] leftMax(int arr[]){

		int left[] =new int[arr.length];
		for(int i = 0;i<arr.length;i++){
			int max = Integer.MIN_VALUE;
			for(int j = 0;j<=i;j++){

				if(max < arr[j]){
					
					max = arr[j];
				}
			}
			left[i] = max;
		}
		return left;
	}*/
	/*
	static int[] optimise(int[] arr){
		int left[] = new int[arr.length];

		int max = Integer.MIN_VALUE;

		for(int i = 0;i<arr.length;i++){
			if(max < arr[i]){
				max = arr[i];
			}
			left[i] = max;
		}
		return left;
	}*/
	static int[] carryForward(int arr[]){

		int left[] = new int[arr.length];
		left[0] = arr[0];


		for(int i = 1;i<arr.length;i++){
			if(left[i-1] < arr[i]){
				left[i] = arr[i];
			}else{
				left[i] =left[i-1];
			}
		}
		return left;
	}

	public static void main(String  args[]){


                Scanner sc = new Scanner(System.in);

                System.out.println("Enter the Array Size");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = sc.nextInt();

                }
		//int arr1[] = leftMax(arr);
		//int arr2[] = optimise(arr);
		int arr3[] = carryForward(arr);

		for(int i = 0;i<arr3.length;i++){

			System.out.print(arr3[i]+"\t");

		}
		System.out.println();
	}

  }
