/*
 6. 
sum of pair
i/p - 1 
[8,4,1,3,9,2,6,7] 
8 + 7 = 15 ;
4 + 6 = 10 ;
1 + 2 = 3 ;
3 + 9 = 12 ;
sum = 40;
i/p -2 
[1,2,3,4,5] 
1 + 5 = 6
2 + 4 = 6
3 = 3
sum = 15
T.C = o(N)
 */

class Array {

	static void sumPair(int arr[]){

		int j = arr.length - 1;
		for(int i =0;i<(arr.length+1)/2;i++){
			if(i == j){
				System.out.println(arr[i]);
			}else{
				System.out.println(arr[i] + arr[j]);
			}
			j--;
		}

	}
	public static void main(String args[]){

		int arr[] = new int[]{1,2,3,4,5,6};

		sumPair(arr);
	}
}
