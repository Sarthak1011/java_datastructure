/*
 5. 
ϐind second largest element
arr: [8,4,1,3,9,2,6,7]
t.c = o(N)
s.c = o(1)

 */

class Array {

	static int secondLarge(int arr[]){
		int max = Integer.MIN_VALUE;
		int secmax = Integer.MIN_VALUE;
		for(int i = 0;i<arr.length;i++){
			if(max<arr[i]){
				secmax = max;
				max = arr[i];
			}
			if(secmax<arr[i] && arr[i] != max){
				secmax = arr[i];
			}
		}
		/*
		for(int i = 0;i<arr.length;i++){
			if(secmax<arr[i] && arr[i] != max){
				secmax = arr[i];
			}
		}*/
		return secmax;
	}
	public static void main(String args[]){

		int arr[] = new int[]{8,16,1,3,9,2,6,7};

		System.out.println(secondLarge(arr));
	}
}

