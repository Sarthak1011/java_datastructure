/*Equlibrium array 

  i/p - [-7 , 1 , 5 , 2 , -4 , 3 , 0]
  o/p - 3
*/

import java.io.*;

class Array {

	static int EquilibriumArr(int arr[]){

		for(int i = 0;i<arr.length;i++){
			int left = 0;
			int right = 0;

			for(int j = 0;j <i;j++){
				left = left + arr[j];
			}
			for(int j = i+1;j<arr.length;j++){
				right = right + arr[j];
			}
			if(left == right){
				return i;
			}
		}
		return -1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("enter the Array Elements");

		for(int i =0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		System.out.println(EquilibriumArr(arr));
	}
}

