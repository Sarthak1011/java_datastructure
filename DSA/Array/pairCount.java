/*
 3. 
given an array of size N return th count of the pair(i,j) with arr[i]+arr[j]=K
arr:[3,5,2,1,-3,7,8,15,16,13] 
N = 10;
K = 10
Note i != j
o/p = 6
brute force
t.c = o(N^2)
s.c = o(1)
optimes
t.c = o(n^2) //kel tr o(n) pn hote hashing nshitr two pointer vaprun
s.c = o(1)


 */ 
class Array {

	static int bruteforce(int arr[] , int k){
		int count  = 0;	
		for(int i = 0;i<arr.length;i++){
			for(int j = 0;j<arr.length;j++){
				if(arr[i] + arr[j]== k && i!=j){
					count++;
				}
			}
		}
		return count;
	}			
	public static void main(String args[]){

		int arr[] = new int[]{3,5,2,1,-3,7,8,15,16,13};

		int k = 10;
		System.out.println(bruteforce(arr,k));
	}
}
