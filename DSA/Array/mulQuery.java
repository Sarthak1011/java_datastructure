/*
 Given an array of size N and Q no of queries query contains two parameters(s , e) s =>  start index and e => contains end index for all queries print the sum of all elements from index s to index e 

arr :[-3,6,2,4,5,2,8,-9,3,1]
N = 10 ;
Q = 3;

Queries   s   e   sum
q1        1    3   12
q2        2    7   12
q3        1    1   6


t.c = O(Q*N)
S.c = o(1)

 */ 
import java.util.*;

class Array {

	public static void main(String args[]){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the Array Size");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i = 0;i<arr.length;i++){
		
			arr[i] = sc.nextInt();
		
		}
		System.out.println("Enter the no of Queries");
		int Q = sc.nextInt();

		for(int i = 1;i<=Q;i++){
			System.out.println("Enter the start number");
			int start = sc.nextInt();
			System.out.println("Enter the end number");
			int end  = sc.nextInt();
			int sum = 0;
			for(int j = start;j <= end;j++){
				sum = sum + arr[j];
			}
			System.out.println(sum);
		}
	}
}



