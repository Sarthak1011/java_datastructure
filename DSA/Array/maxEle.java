/*
 find max element upto index

 */
import java.util.*;
class Array {

	static int maxEle(int arr[] , int k){

		int max = Integer.MIN_VALUE;
		for(int i = 0;i<=k;i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}
		return max;

	}
	public static void main(String  args[]){

	
                Scanner sc = new Scanner(System.in);

                System.out.println("Enter the Array Size");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter the Array elements");
                for(int i = 0;i<arr.length;i++){

                        arr[i] = sc.nextInt();

                }
		System.out.println("Enter the index number");
		int k = sc.nextInt();
		System.out.println(maxEle(arr,k));
	}
}




