/*
 given an charachter array (lowercase) return the count of pair(i , j) such that
a) i < j 
b) arr[i] = 'a'
c) arr[j] = 'g'

arr: [a,b,e,g,a,g]
o/p = 3
*/

import java.util.*;
class Array {

/*	static int countPair(char arr[]){

		int count = 0;
		for(int i = 0;i<arr.length;i++){

			if(arr[i]=='a'){

				for(int j = i+1;j<arr.length;j++){

					if(arr[j]=='g'){
						count++;
					}
				}
			}
		}
		return count;
	}*/

	static int countPair(char arr[]){
		int counta = 0;
		int countb = 0;
		int sum = 0;
		//[a , b , c,a , b , c]
		for(int i =0;i<arr.length;i++){
			if(arr[i]=='a'){
				counta++;
			}else if(arr[i]=='b'){
                                countb = counta+countb;
                        }else if(arr[i]=='c'){
				sum = sum + countb;
			}
		}
		return sum;
	}

	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Array size");
		int size = sc.nextInt();

		char arr[] = new char[size];

		for(int i =0;i<size;i++){
			arr[i] = sc.next().charAt(0);
		}

		System.out.println(countPair(arr));

	}
}

