import java.io.*;
class Search {

	static int binarySearch(int []arr, int target){
		
		//1 4 7 23 35 36 40

		int start = 0;
		int end = arr.length - 1;

		int count = 0;
		while(start <= end){
			count++;
			int mid = (end + start)/2;

			if(arr[mid]==target){

				System.out.println("Count :"+count);
				return mid;
			}
			if(arr[mid] > target){
				end = mid - 1;
			}
			if(arr[mid] < target){
				start = mid + 1;
			}
		}
		return -1;

	}
	public static void main(String args[])throws IOException{

		System.out.println("Enter the Array Size");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Array Elements");
		int arr[] = new int[size];

		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter the target Element");
		int target = Integer.parseInt(br.readLine());
		int ret = binarySearch(arr , target);

			if(ret == -1 ){
				System.out.println("target ELement not Found");
			}else{
				System.out.println("Target Element FOund at index"+ret);
			}
	}
}



