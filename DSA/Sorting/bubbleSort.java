import java.io.*;
class Sort {

	void bubbleSort(int arr[]){

		for(int i = 0;i<arr.length;i++){
			int flag = 0;
			for(int j = 0;j<arr.length-1-i;j++){

				if(arr[j] > arr[j+1]){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1]= temp;
					flag = 1;
				}
			}
			if(flag == 0){
				break;
			}
		}
	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter the Array Elements");

		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Sort obj = new Sort();
		obj.bubbleSort(arr);
		for(int i = 0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}	
