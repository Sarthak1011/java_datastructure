import java.io.*;
class Sort {

	void selectionSort(int arr[]){

		for(int i = 0;i<arr.length-1;i++){
			int minIndex = i;
			for(int j = i+1;j<arr.length;j++){
				if(arr[j] < arr[minIndex]){
					minIndex = j;
				}
			}
			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		}
	}
 	public static void main(String args[])throws IOException {


                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                Sort obj = new Sort();

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter the Array Elements");

                for(int i = 0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
                
                obj.selectionSort(arr);

                for(int i = 0;i<arr.length;i++){
                        System.out.println(arr[i]);
                }
	}
}	
