import java.io.*;

class Sort {

	void insertionSort(int arr[]){

		for(int i = 1;i<arr.length;i++){

			int ele = arr[i];
			int j = i - 1;
			while(j>=0 && arr[j]>ele){
			
				arr[j+1] = arr[j];
				j--;
		
			}
			arr[j+1] = ele;
		}
	}
	public static void main(String args[])throws IOException {


                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                Sort obj = new Sort();

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter the Array Elements");

                for(int i = 0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());
                
		}

                obj.insertionSort(arr);

                for(int i = 0;i<arr.length;i++){
                        System.out.println(arr[i]);
                }
        }
}	

