//quick Sort  ---> Lomoto Apporach
import java.io.*;
class Sort {
	int partition(int arr[] , int start , int end){
		int pivot = arr[end];
		int i = start - 1;

		for(int j = start; j<end;j++){
			if(arr[j]<=pivot){
				i++;
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
		i++;
		int temp = arr[end];
		arr[end] = arr[i];
		arr[i] = temp;
		return i;
	}
	void quickSort(int arr[] , int start , int end){

		if(start < end){
			int pivot = partition(arr , start , end);
			quickSort(arr , start , pivot - 1);
			quickSort(arr , pivot + 1 , end);
		}
	}

 	public static void main(String args[])throws IOException {


                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                Sort obj = new Sort();
         

                System.out.println("Enter the size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter the Array Elements");
                for(int i = 0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
                int start = 0;
                int end = arr.length -1;

                obj.quickSort(arr , start , end);

                for(int i = 0;i<arr.length;i++){
                        System.out.println(arr[i]);
                }
 	}
}
