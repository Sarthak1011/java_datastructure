import java.io.*;
class Sort {

	int count = 0;
	void merge(int arr[] , int start ,int mid , int end){

		count++;
		int n1 = mid - start + 1;
		int n2 = end - mid;

		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		//copy main array elements to two array

		for(int i = 0;i<arr1.length;i++){
			arr1[i] = arr[start + i];
		}
		for(int i = 0;i<arr2.length;i++){
			arr2[i] = arr[mid+1+i];
		}
		//compair two array data and sort 

		int i = 0,j = 0,k = start;

		while(i < arr1.length && j < arr2.length){
			if(arr1[i] < arr2[j]){
				arr[k] = arr1[i];
				i++;
			}else{
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}
		//if only first array elements are remainig then directly attached to the main array
		while(i<arr1.length){
			arr[k] = arr1[i];
			i++;
			k++;
		}
		//if only second array elements are remainig then directly attached to the main array
		while(j < arr2.length){
			arr[k] = arr2[j];
			j++;
			k++;
		}	
	}	
	void mergeSort(int arr[] , int start , int end){
		count++;

		if(start >= end){
			return;
		}
		int mid = start + (end - start)/2;

		mergeSort(arr , start , mid);
		mergeSort(arr , mid + 1,end);
		merge(arr , start , mid , end);
	}

	public static void main(String args[])throws IOException {
		

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Sort obj = new Sort();
		obj.count++;

		System.out.println("Enter the size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter the Array Elements");
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int start = 0;
		int end = arr.length -1;

		obj.mergeSort(arr , start , end);
		
		for(int i = 0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
		System.out.println("COunt"+obj.count);
	}
}

