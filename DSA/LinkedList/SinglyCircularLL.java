import java.io.*;
class Node {

	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}
class LinkedList {
	Node head = null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
			head.next = head;
	
		}else{
			Node temp = head;
			while(temp.next != head){
				temp = temp.next;
			}
			newNode.next = head;
			head = newNode;
			temp.next = head;
		}
	}
	void addLast(int data){

		Node newNode = new Node(data);
		if(head == null){
			head = newNode;
			head.next = head;
		}else{
			Node temp = head;
			while(temp.next!=head){
				temp = temp.next;
			}
			temp.next = newNode;
			newNode.next = head;
		}
	}
	int countNode(){
		int count = 0;
		Node temp = head;
		while(temp.next!= head){
			temp = temp.next;
			count++;
		}
		return count+1;
	}
	int addAtPos(int data , int pos){
		Node newNode = new Node(data);
		if(head == null){
			head = newNode;
			head.next = head;
		}else if(pos <=0 || pos>countNode()+1){
			return 1;
		}else if(pos == 1){
			addFirst(data);
		}else if(pos == countNode()+1){
			addLast(data);
		}else{
			Node temp = head;
			while(pos -2 != 0){
				temp = temp.next;
			}
			newNode.next = temp.next;
			temp.next = newNode;

		}
		return 0;
	}
	int deleteFirst(){
		if(head == null){
			return 1;
		}else if(head.next == head){
			head = null;
		}else{
			Node temp = head;
			while(temp.next != head){
				temp = temp.next;
			}
			head = head.next;
			temp.next = head;
		}
		return 0;
	}
	int deleteLast(){
		if(head == null){
			return 1;
		}else if(head.next == null){
			head = null;
		}else{
			Node temp = head;
			while(temp.next.next != head){
				temp = temp.next;
			}
			temp.next = head;
		}
		return 0;
	}
	int deleteAtPos(int pos){

		if(head == null){
			return 1;
		}else if(pos <=0 || pos >countNode()){
			return -1;
		}else if(pos == 1){
			deleteFirst();
		}else if(pos == countNode()){
			deleteLast();
		}else{
			Node temp = head;
			while(pos-2 != 0){
				temp = temp.next;
			}
			temp.next = temp.next.next;
		}
		return 0;
	}
	int printSLL(){

		if(head == null){
			return 1;
		}else{
			Node temp = head;
			while(temp.next!= head){
				System.out.print(temp.data+"->");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
		return 0;
	}
}
class Client {
	public static void main(String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		LinkedList ll = new LinkedList();
		
		char ch;
		do {

			System.out.println("Singly LinkedList");
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.deleteFirst");
			System.out.println("5.deleteLast");
			System.out.println("6.deleteAtPos");
			System.out.println("7.Count");
			System.out.println("8.printSLL");

			System.out.println("Enter your Choice:");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){

				case 1 :
					{
						System.out.println("Enter the data");
						int data = Integer.parseInt(br.readLine());

						ll.addFirst(data);
					}
					break;
				case 2 :
					{
						System.out.println("Enter the data");
						int data = Integer.parseInt(br.readLine());

						ll.addLast(data);
					}
					break;
				case 3 :
					{
						System.out.println("Enter the data");
						int data = Integer.parseInt(br.readLine());
						
						System.out.println("Enter the position");
						int pos = Integer.parseInt(br.readLine());

						int ret = ll.addAtPos(data,pos);
						if(ret == 1){
							System.out.println("Wrong Input");
						}
					}
					break;
				case 4 :
					{
						int ret = ll.deleteFirst();
						if(ret == 1){
							System.out.println("LinkedList is Empty");
						}
					}
					break;	
				case 5 :
					{
						int ret = ll.deleteLast();
						if(ret == 1){
							System.out.println("LinkedList is Empty");
						}
					}
					break;	
				case 6:
					{
						System.out.println("Enter the position");
						int pos = Integer.parseInt(br.readLine());
						
						int ret = ll.deleteAtPos(pos);
				      		if(ret == 1){
							System.out.println("LinkedList is Empty");
						}else if(ret == -1){
							System.out.println("Wrong Input");
						}
					}
					break;
				case 7: 
					{
						int ret = ll.countNode();	
						System.out.println("Count :"+ret);

					}
					break;
				case 8:
					{
						int ret = ll.printSLL();
						if(ret == 1){
							System.out.println("LinkedList is Empty");
						}
					}
					break;	
			}
			System.out.println("Do you want to Continue");
			ch = (char)(br.read());
			br.skip(1);
		}while(ch == 'Y' || ch == 'y');
	}
}
	

