//5. WAP to check whether the number is prime or not.


import java.io.*;
class Recursion{
	boolean PrimeNum(int num){

		for(int i = 2;i<=num /2;i++){
			if(num % i == 0){
				return false;
			}
		}
		if(num == 1){
			return false;
		}
		return true;
	}
	public static void main(String args[])throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                        System.out.println("Enter the number");
                        int num = Integer.parseInt(br.readLine());

                        Recursion obj = new Recursion();

                        boolean ret = obj.PrimeNum(num);
			if(ret == true ){
				System.out.println("Prime");
			}else{
				System.out.println("Not Prime");
			}
        }
}
