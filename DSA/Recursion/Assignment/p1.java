//1. WAP to print the numbers between 1 to 10.


import java.io.*;
class Recursion {

	void fun(int num){
		if(num == 0){
			return;
		}
		fun(num - 1);
		System.out.println(num);
	}
	public static void main(String args[])throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number");
		int num = Integer.parseInt(br.readLine());

		Recursion obj = new Recursion();

		obj.fun(num);
	}
}

			
