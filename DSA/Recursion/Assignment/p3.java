//3. WAP to print the sum of n natural numbers.
//
import java.io.*;
class Recursion{

	/*int sum = 0;
	int SumOfN(int num){

		if(num == 0){
			return sum;
		}
		sum = sum + num;
		return SumOfN(num - 1);
	}*/
	int SumOfN(int num){

		if(num == 1){
			return num;
		}
		return num + SumOfN(num - 1);
	}	
     	public static void main(String args[])throws IOException{
        	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                	System.out.println("Enter the number");
                	int num = Integer.parseInt(br.readLine());

                	Recursion obj = new Recursion();

                	System.out.println(obj.SumOfN(num));
        }
}	
