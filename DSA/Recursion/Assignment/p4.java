//4. WAP to print the length of digits in a number.
//
import java.io.*;
class Recursion{
	int count = 0;
	int LenOfDigit(int num){

		if(num == 0){
			return count;
		}
		count ++;
		return LenOfDigit(num / 10); 
	}
     	public static void main(String args[])throws IOException{
        	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                	System.out.println("Enter the number");
                	int num = Integer.parseInt(br.readLine());

                	Recursion obj = new Recursion();

                	System.out.println(obj.LenOfDigit(num));
        }
}	
