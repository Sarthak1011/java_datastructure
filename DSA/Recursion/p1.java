class Recursion {
	int i = 1;
	void fun(int num){
		if(num == 11){
			return;
		}
		System.out.println(num);
		fun(++num);
	}

	public static void main(String args[]){

		int num = 1;
		Recursion obj = new Recursion();
		obj.fun(num);
	}
}
