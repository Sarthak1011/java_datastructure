class Recursion {

	void fun(int num){

		if(num == 0){

			return;
		
		}
		System.out.println(num--);
		fun(num);
	}
	public static void main(String args[]){

		Recursion obj = new Recursion();
		obj.fun(10);
	
	}
}

