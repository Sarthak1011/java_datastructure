/*
       1
     A b
   1 2 3
 A b C d
 */ 


class c2w {
	public static void main(String args[]){

		char ch1='A';
		char ch2='a';
		int num=1;
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("_ ");
			}
			num=1;
			ch1='A';
			ch2='a';
			for(int k=1;k<=i;k++){
				if(i%2==1){
					System.out.print(num++ +" ");
				}else{
					if((i+k)%2==1){
						System.out.print(ch1+" ");
					}else{
						System.out.print(ch2 +" ");
					}
					ch1++;
					ch2++;
				}
			}
			System.out.println();
		}
	}
}

