/*
 10. Take no of rows from the user ROWS = 4

10
I H
7 6 5
D C B A
 */ 
class c2w {
	public static void main(String args[]){
		int row=4;
		int num=10;
		char ch=74;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(num + " ");
				}else{
					System.out.print(ch + " ");
				}
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
