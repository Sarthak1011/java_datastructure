/*
       D
     c D
   B c D
 a B c D
 */ 


class c2w {
	public static void main(String args[]){

		int row=4;
		char ch1='D';
		char ch2='d';
		for(int i=1;i<=row;i++){
			for(int j=row;j>i;j--){
				System.out.print("_ ");
			}

			char big=ch1;
			char small=ch2;

			for(int k=1;k<=i;k++){
				if((i+k)%2==0){

				System.out.print(big++ +" ");
				}else{
					System.out.print(small++ +" ");
				}
			}
			System.out.println();
			ch1--;
			ch2--;
		}
	}
}
