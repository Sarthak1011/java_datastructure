/*
           1
	1  2
     1  2  3
  1  2  3  4     
 */ 

class c2w {
	public static void main(String args[]){
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=row-1;j>=i;j--){
				System.out.print("_ ");
			}
			int num=1;
			for(int k=1;k<=i;k++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
