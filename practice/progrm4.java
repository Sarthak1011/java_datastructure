class C2W {
  public static void main(String[] args) {

    int n1 = 2, n2 = 3, lcm;

    // maximum number between n1 and n2 is stored in lcm
    if(n1 > n2){
	  lcm= n1;
    }else{
	    lcm=n2;
    }



    // Always true
    while(true) {
      if( lcm % n1 == 0 && lcm % n2 == 0 ) {
        System.out.printf("The LCM of %d and %d is %d\n", n1, n2, lcm);
        break;
      }
      ++lcm;
    }
  }
}
