import java.net.*;

public class SendDatagramPacket {

    public static void main(String[] args) throws Exception {
        // Create a datagram socket
        DatagramSocket socket = new DatagramSocket();

        // Create a datagram packet
        byte[] data = "Hello, world!".getBytes();
        DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName("127.0.0.1"), 8080);

        // Send the datagram packet
        socket.send(packet);

        // Close the datagram socket
        socket.close();
    }
}

