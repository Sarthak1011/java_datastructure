class Demo {

	static void fun(int num){

		if(num == 0){
			return;
		}
		System.out.println(num);
		fun(--num);
	}
	public static void main(String args[]){

		System.out.println("Start main");
		fun(10);
		System.out.println("End main");
	}
}
